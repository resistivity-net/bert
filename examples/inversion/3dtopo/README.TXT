3D Inversion with Topography
----------------------------
Here we have three examples

1. Burial Mound 
This is the synthetic model of the original Inversion paper (G�nther et al. 2006), Figs. 4-8. See description therein.

2. Slag dump
This is the field case examples of the original Inversion paper (G�nther et al. 2006), Figs. 10-12. See description therein.
The slag heap 

3. Acucar
The name of the hill is derived from the spanish word of the sugar loaf (of Rio) as presents the title page.
It represents two perpendicular profiles crossing the top and one ring profile around.