from pygimli.physics import ert

res = ert.ERTManager('gallery.dat')
res.data["k"] = ert.geometricFactors(res.data)
res.estimateError()
res.invert()
res.showResult()
# res.fw.blockyModel = True
res.invert(stopAtChi1=False, blockyModel=True, verbose=True)
# res.invert(blockyModel=True)
res.showResult()
res.showMisfit()
res.showMisfit(errorWeighted=True)
