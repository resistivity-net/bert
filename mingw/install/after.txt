Make the install directories available for calling by adding the bin
folder to the PATH variable, either by System Control or by the line
export PATH=$PATH:/c/Software/BERT
(assuming BERT is installed in c:\Software\BERT)
This line could be placed in the .bashrc file to be called at startup.

For using pygimli, the Python bindings of gimli, set your python path
variable PYTHONPATH to the installation directory, either by System
Control or by
export PYTHONPATH=/c/Software/BERT

Please note that distribution is compiled with Python 3.4 64bit
and will only work with such a version. 
We recommend using the WinPython distribution but Anaconda works as well.
