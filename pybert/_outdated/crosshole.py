import os.path
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import pygimli as pg
import pygimli.meshtools as mt
from pygimli.physics import ert
from datetime import datetime


def timeIndex(times, t):  #
    """Return index of closest timestep in times to t.

    Parameters
    ----------
    times : np.array(dtype=datetime)
        array of datetimes
    t : str|datetime
        datetime object or string
    """
    if isinstance(t, str):  # convert into datetime
        t = datetime.fromisoformat(t) # check others
    if isinstance(t, datetime): # detect closest point
        return np.argmin(np.abs(times-t))
    elif hasattr(t, "__iter__"):
        return np.array([timeIndex(times, ti) for ti in t], dtype=int)
    else:
        raise TypeError("Unknown type", type(t))


class CrossholeERT():
    """Class for crosshole ERT data manipulation.

    Note that this class is to be split into a hierarchy of classes for general
    timelapse data management, timelapse ERT and crosshole ERT.
    You can load data, filter them data in the temporal or measuring axis, plot
    data, run inversion and export data and result files.

    """

    def __init__(self, filename=None, **kwargs):
        """Initialize class and possibly load data.

        Parameters
        ----------
        filename : str
            filename to load data, times, RHOA and ERR from
        data : DataContainerERT
            The data with quadrupoles for all
        times : np.array of datetime objects
            measuring times
        RHOA : 2d np.array (data.size(), len(times))
            all apparent resistivities
        ERR : 2d np.array (data.size(), len(times))
            all apparent relative errors
        bhmap : array
            map electrode numbers to borehole numbers
        mesh : array
            mesh for inversion
        """
        self.data = kwargs.pop("data", None)
        self.RHOA = kwargs.pop("RHOA", [])
        self.ERR = kwargs.pop("ERR", [])
        self.times = kwargs.pop("times", [])
        self.bhmap = kwargs.pop("bhmap", [])
        self.mesh = kwargs.pop("mesh", None)
        self.models = []
        self.chi2s = []
        self.model = None
        self.mgr = ert.ERTManager()
        if filename is not None:
            self.load(filename, **kwargs)
            self.name = filename[:-4]
        else:
            self.name = kwargs.pop("name", "new")

        if self.data is not None and not np.any(self.bhmap):
            self.determineBHmap()
        if np.any(self.RHOA):
            self.mask()

    def __repr__(self):  # for print function
        """Return string representation of the class."""
        out = ['Crosshole data:', self.data.__str__()]
        if np.any(self.RHOA):
            out.append("{} time steps".format(self.RHOA.shape[1]))
            if np.any(self.times):
                out[-1] += " from " + self.times[0].isoformat(" ", "minutes")
                out[-1] += " to " + self.times[-1].isoformat(" ", "minutes")

        return "\n".join(out)

    def determineBHmap(self):  # XH-specific
        """Auto-determine borehole map from xy positions."""
        xy = pg.x(self.data)*999+pg.y(self.data)*999  # maybe needs a data.sortX() first
        d0 = np.diff(np.hstack([0, np.nonzero(np.diff(xy))[0]+1, len(xy)]))
        self.bhmap = np.concatenate([np.ones(dd, dtype=int)*i for i, dd in enumerate(d0)])

    def load(self, filename, **kwargs):
        """Load or import data."""  # TL-ERT
        if os.path.isfile(filename):
            self.data = ert.load(filename)
            if os.path.isfile(filename[:-4]+".rhoa"):
                self.RHOA = np.loadtxt(filename[:-4]+".rhoa")
            if os.path.isfile(filename[:-4]+".err"):
                self.ERR = np.loadtxt(filename[:-4]+".err")
            if os.path.isfile(filename[:-4]+".times"):
                timestr = np.loadtxt(filename[:-4]+".times", dtype=str)
                self.times = np.array([datetime.fromisoformat(s) for s in timestr])

    def saveData(self, filename=None):
        """Save all data as datacontainer, times, rhoa and error arrays."""
        filename = filename or self.name
        self.data.save(filename + ".shm")
        np.savetxt(filename+".rhoa", self.RHOA, fmt="%6.2f")
        if np.any(self.ERR):
            np.savetxt(filename+".err", self.ERR, fmt="%6.2f")
        with open(filename+".times", "w", encoding="utf-8") as fid:
            for d in self.times:
                fid.write(d.isoformat()+"\n")
        self.name = filename

    def timeIndex(self, t):  #
        """Return index of closest timestep in times to t.

        Parameters
        ----------
        t : str|datetime
            datetime object or string
        """
        if isinstance(t, str):  # convert into datetime
            t = datetime.fromisoformat(t) # check others
        if isinstance(t, datetime): # detect closest point
            return np.argmin(np.abs(self.times-t))
        elif isinstance(t, (int, np.int32)):
            return t
        elif hasattr(t, "__iter__"):
            return np.array([self.timeIndex(ti) for ti in t], dtype=int)
        else:
            raise TypeError("Unknown type", type(t))

    def filter(self, tmin=0, tmax=None, t=None, select=None, kmax=None):
        """Filter data set temporally or data-wise.

        Parameters
        ----------
        tmin, tmax : int|str|datetime
            minimum and maximum times to keep
        t : int|str|datetime
            time to remove
        kmax : float
            maximum geometric factor to allow
        """
        if np.any(self.RHOA):
            if select is not None:
                ind = self.timeIndex(select)
            else:
                tmin = self.timeIndex(tmin)  # converts dt/str to int
                if tmax is None:
                    tmax = self.RHOA.shape[1]
                else:
                    tmax = self.timeIndex(tmax)

                ind = np.arange(tmin, tmax)
                if t is not None:
                    ind = np.setxor1d(ind, t)

            self.RHOA = self.RHOA[:, ind]
            if np.any(self.ERR):
                self.ERR = self.ERR[:, ind]
            if np.any(self.times):
                self.times = self.times[ind]
        if kmax is not None:
            ind = np.nonzero(np.abs(self.data["k"]) < kmax)[0]
            self.data["valid"] = 0
            self.data.markValid(ind)
            self.data.removeInvalid()
            if np.any(self.RHOA):
                self.RHOA = self.RHOA[ind, :]
            if np.any(self.ERR):
                self.ERR = self.ERR[ind, :]

    def mask(self, rmin=0.1, rmax=1e6, emax=None):
        """Mask data.

        Parameters
        ----------
        rmin, rmax : float
            minimum and maximum apparent resistivity
        emax : float
            maximum error
        """
        self.RHOA = np.ma.masked_invalid(self.RHOA)
        self.RHOA = np.ma.masked_outside(self.RHOA, rmin, rmax)
        if emax is not None:
            self.RHOA.mask = np.bitwise_or(self.RHOA.mask, self.ERR > emax)

    def showData(self, v="rhoa", x="a", y="m", t=None, **kwargs):
        """Show data.

        Show data as pseudosections (single-hole) or cross-plot (crosshole)

        Parameters
        ----------
        v : str|array ["rhoa]
            array or field to plot
        x, y : str|array ["a", "m"]
            values to use for x and y axes
        t : int|str|datetime
            time to choose
        kwargs : dict
            forwarded to ert.show or showDataContainerAsMatrix
        """
        kwargs.setdefault("cMap", "Spectral_r")
        if t is not None:
            t = self.timeIndex(t)
            rhoa = self.RHOA[:, t]
            v = rhoa.data
            v[rhoa.mask] = np.nan
        if len(np.unique(self.data["na"])) > 1:
            ax, cb = pg.viewer.mpl.showDataContainerAsMatrix(
                self.data, x, y, v, **kwargs)
            xx = np.nonzero(np.diff(self.bhmap))[0] + 1
            ax.set_xticks(xx)
            ax.set_yticks(xx)
            return ax, cb
        else:
            return self.data.show(v, **kwargs)

    def showTimeline(self, ax=None, **kwargs):
        """Show data timeline.

        Parameters
        ----------
        ax : mpl.Axes|None
            matplotlib axes to plot (otherwise new)
        a, b, m, n : int
            tokens to extract data from
        """
        if ax is None:
            _, ax = plt.subplots(figsize=[8, 5])
        good = np.ones(self.data.size(), dtype=bool)
        lab = kwargs.pop("label", "ABMN") + ": "
        for k, v in kwargs.items():
            good = np.bitwise_and(good, self.data[k] == v)

        abmn = [self.data[tok] for tok in "abmn"]
        for i in np.nonzero(good)[0]:
            lab1 = lab + " ".join([str(tt[i]) for tt in abmn])
            ax.semilogy(self.times, self.RHOA[i, :], "x-", label=lab1)

        ax.grid(True)
        ax.legend()
        ax.set_xlabel("time")
        ax.set_ylabel("resistivity (Ohmm)")
        return ax

    def generateDataPDF(self, **kwargs):
        """Generate a pdf with all data as timesteps in individual pages.

        Iterates through times and calls showData into multi-page pdf
        """
        kwargs.setdefault("verbose", False)
        with PdfPages(self.name+'-data.pdf') as pdf:
            fig = plt.figure(figsize=kwargs.pop("figsize", [5, 5]))
            for i in range(self.RHOA.shape[1]):
                ax = fig.subplots()
                self.showData(t=i, ax=ax, **kwargs)
                ax.set_title(str(i)+": "+ self.times[i].isoformat(" ", "minutes"))
                fig.savefig(pdf, format='pdf')
                fig.clf()

    def extractSubset(self, nbh, slice=None, name=None):
        """Extract a subset (slice) by borehole number.

        Returns a CrossholeERT instance with reduced boreholes

        Parameters
        ----------
        nbh : int|array
            borehole(s) to extract data from
        name : str
            name to give the new instance
        slice : bool [None]
            reduce to 2D (automatic if max. 2 boreholes are used)
        """
        xh2 = pg.DataContainerERT(self.data)
        good = pg.Vector(xh2.size(), 1)
        good = np.ones(xh2.size(), dtype=bool)
        if isinstance(nbh, int):
            nbh = [nbh]
        for tok in ["a", "b", "m", "n"]:
            bla = np.zeros(xh2.size(), dtype=bool)
            for nn in nbh:
                bla = bla | (self.data["n"+tok] == nn)

            good = good & bla

        xh2["valid"] *= 0  # all invalid
        xh2.markValid(np.nonzero(good)[0])
        xh2.removeInvalid()
        xh2.removeUnusedSensors()
        if slice is None:  # decide upon number
            slice = len(nbh) < 3
        if slice:
            p0 = self.data.sensor(np.nonzero(self.bhmap==nbh[0]-1)[0][0])
            dists = [p0.dist(self.data.sensor(np.nonzero(
                self.bhmap==nn-1)[0][0])) for nn in nbh]
            for i in range(xh2.sensorCount()):
                xh2.setSensor(i, [dists[self.bhmap[i]], 0,
                                  xh2.sensorPosition(i).z()])
        name = name or self.name + "xh" + "".join([str(i) for i in nbh])
        return CrossholeERT(data=xh2, RHOA=self.RHOA[good], times=self.times,
                            name=name)

    def createMesh(self, ibound=2, obound=10, quality=None, show=False,
                   threeD=None, ref=0.25):
        """Create a 2D mesh around boreholes.

        Parameters
        ----------
        ibound : float
            inner boundary in m
        ibound : float
            outer boundary in m
        quality : float
            triangle or tetgen quality
        threeD : bool|None
            create 3D model (None-automatic)
        ref : float
            electrode refinement in m
        """
        data = self.data
        xmin, xmax = min(pg.x(data)), max(pg.x(data))
        ymin, ymax = min(pg.y(data)), max(pg.y(data))
        zmin, zmax = min(pg.z(data)), max(pg.z(data))
        if threeD is None:
            threeD = (ymin != ymax) and (zmin != zmax)
        if threeD:
            world = mt.createWorld(start=[xmin-obound, ymin-obound, zmin-obound],
                                end=[xmax+obound, ymax+obound, 0], marker=1)
            box = mt.createCube(start=[xmin-ibound, ymin-ibound, zmin-ibound],
                                end=[xmax+ibound, ymax+ibound, zmax+ibound],
                                marker=2, area=1)
            quality = quality or 1.3
        else:
            world = mt.createWorld(start=[xmin-obound, zmin-obound],
                                   end=[xmax+obound, 0.], marker=1)
            box = mt.createRectangle(start=[xmin-ibound, zmin-ibound],
                                    end=[xmax+ibound, zmax+ibound], marker=2)
            quality = quality or 34.4

        geo = world + box
        for pos in data.sensors():
            if threeD:
                geo.createNode(pos, marker=-99)
                geo.createNode(ProcessLookupError - pg.Pos(0, 0, ref))  # refinement
            else:
                geo.createNode([pos.x(), pos.z()], marker=-99)
                geo.createNode([pos.x(), pos.z()-ref])

        self.mesh = mt.createMesh(geo, quality=quality)
        if show:
            pg.show(self.mesh, markers=True)

    def chooseTime(self, t=None, **kwargs):
        """Return data for specific time.

        Parameters
        ----------
        t : int|str|datetime
        """
        if not isinstance(t, int):
            t = self.timeIndex(t)
        rhoa = self.RHOA[:, t].copy()
        arhoa = np.abs(rhoa.data)
        arhoa[rhoa.mask] = np.nanmedian(arhoa)
        data = self.data.copy()
        data["rhoa"] = arhoa
        data.estimateError()
        data["err"][rhoa.mask] = 1e8
        self.data = data
        return data

    def invert(self, t=None, reg={}, **kwargs):
        """Run inversion for a specific timestep or all subsequently."""
        if t is not None:
            t = self.timeIndex(t)

        if self.mesh is None:
            self.createMesh()
        self.mgr.setMesh(mesh=self.mesh)
        self.mgr.fop.setVerbose(False)
        if isinstance(reg, dict):
            self.mgr.inv.setRegularization(**reg)
        if t is None:  # all
            t = np.arange(len(self.times))

        t = np.atleast_1d(t)
        self.models = []
        self.chi2s = []
        startModel = kwargs.pop("startModel", 100)
        creep = kwargs.pop("creep", False)
        for i, ti in enumerate(np.atleast_1d(t)):
            self.mgr.setData(self.chooseTime(ti))
            self.model = self.mgr.invert(startModel=startModel, **kwargs)
            if i == 0 or creep:
                startModel = self.model.copy()

            self.models.append(self.model)
            self.chi2s.append(self.mgr.inv.chi2)

        if len(t) == 1:
            self.mgr.showResult()

    def fullInversion(self, scalef=1.0, **kwargs):
        """Full (4D) inversion."""
        DATA = [self.chooseTime(ti) for ti in range(len(self.times))]
        fop = pg.frameworks.MultiFrameModelling(ert.ERTModelling, scalef=scalef)
        fop.setData(DATA)
        fop.setMesh(self.mesh)
        print(fop.mesh())  # important to call mesh() function once!
        dataVec = np.concatenate([data["rhoa"] for data in DATA])
        errorVec = np.concatenate([data["err"] for data in DATA])
        startModel = fop.createStartModel(dataVec)
        inv = pg.Inversion(fop=fop, startModel=startModel, verbose=True)
        fop.createConstraints()
        kwargs.setdefault("maxIter", 10)
        kwargs.setdefault("verbose", True)
        kwargs.setdefault("startModel", startModel)
        model = inv.run(dataVec, errorVec, **kwargs)
        self.models = np.reshape(model, [len(DATA), -1])
        self.pd = fop.paraDomain
        self.fop = fop
        return model

    def showFit(self, **kwargs):
        """Show data, model response and misfit."""
        _, ax = plt.subplots(ncols=3, figsize=(10, 6), sharex=True, sharey=True)
        _, cb = self.showData(ax=ax[0], verbose=False)
        self.showData(self.mgr.inv.response, ax=ax[1],
                      cMin=cb.vmin, cMax=cb.vmax, verbose=False)
        misfit = self.mgr.inv.response / self.data["rhoa"] * 100 - 100
        self.showData(misfit, ax=ax[2], cMin=-10, cMax=10, cMap="bwr", verbose=0)
        return ax

    def generateModelPDF(self, **kwargs):
        """Generate a multi-page pdf with the model results."""
        with PdfPages(self.name+'-model.pdf') as pdf:
            fig = plt.figure(figsize=kwargs.pop("figsize", [5, 5]))
            for i, model in enumerate(self.models):
                ax = fig.subplots()
                self.mgr.showResult(model, ax=ax, **kwargs)
                ax.set_title(str(i)+": " + self.times[i].isoformat(" ", "minutes"))
                fig.savefig(pdf, format='pdf')
                fig.clf()



if __name__ == "__main__":
    pass
