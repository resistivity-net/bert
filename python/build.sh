#!/bin/bash
MAKEOPTS=-j7
mkdir -p generated

[ -f _pybert_.cache ] && rm _pybert_.cache

if [ $OSTYPE = "msys" ]; then
	MAKEFILE=Makefile.msys
	MAKEOPTS=-j2
	echo "build for mingw"
else
	MAKEFILE=Makefile.linux
	echo "build for linux gcc"
fi

if [ $# -lt 1 ]; then 
    python generate_pybert_code.py
    make -f $MAKEFILE $MAKEOPTS 
elif [ $# -gt 0 ]; then
    if [ "$1" = "test" ]; then
        echo "build testsuite"
        python generate_pybert_code.py test
        DEFINES='-D PYTEST' make -f $MAKEFILE $MAKEOPTS
        exit
    elif [ "$1" = "clean" ]; then
        make -f $MAKEFILE clean
    else
        echo "unknown command" $1	
    fi
fi

