# -*- coding: utf-8 -*-
"""
Created on Thu Feb 02 15:39:13 2012

@author: Guenther.T
"""

import pygimli as pg
import pybert as pb
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import Delaunay
import matplotlib.tri as mtri

import os # prepare directories for 2d files and positions
if not os.path.isdir( '2d' ): os.mkdir( '2d' )
if not os.path.isdir( '3d' ): os.mkdir( '3d' )
if not os.path.isdir( 'pos' ): os.mkdir( 'pos' )

#%% load profile (*.pro) file
profile = 'alldata.pro'
XL, YL, FILES = [], [], []
fid = open(profile)
for line in fid:
    FILES.append(line.split()[0])
    XL.append(np.double(line.split()[1::2]))
    YL.append(np.double(line.split()[2::2]))

fid.close()

#%% Load topography, triangulate and prepare interpolator using MPL.tri
x, y, z = np.loadtxt('topo.xyz', unpack=True)
tri = mtri.Triangulation(x, y)
interp_lin = mtri.LinearTriInterpolator(tri, z)
#interp_lin = mtri.CubicTriInterpolator(tri, z)  # alternative
#%%
fig, ax = plt.subplots()
ax.tricontourf(tri, z, 50)
ax.plot(x, y, 'kx')
for tt in tri.triangles:
    ttt = np.hstack((tt, tt[0]))
    ax.plot(x[ttt], y[ttt], 'w-')

if False:  # from old HowTo
    mesh = pg.Mesh(3)
    for x1, y1, z1 in zip(x, y, z):
        mesh.createNode(x1, y1, z1)

    for v in tri.triangles:
        mesh.createCell(pg.Triangle(mesh.node(int(v[0])),
                                    mesh.node(int(v[1])),
                                    mesh.node(int(v[2]))))

    mesh.exportVTK('topo.vtk')

nump, acc = 200, 0.001  # number of interpolation points and position accuracy
data3d = pb.DataContainerERT()  # empty 3d data to be filled with 2d data
for xl, yl, datfile in zip(XL, YL, FILES):
    data = pb.DataContainerERT(datfile)
    te = [pos.x() for pos in data.sensorPositions()]

    le = np.hstack((0., np.cumsum(np.sqrt(np.diff(xl)**2 + np.diff(yl)**2))))
    xi = np.interp(np.linspace(le[0], le[-1], nump), le, xl)
    yi = np.interp(np.linspace(le[0], le[-1], nump), le, yl)
    zi = interp_lin(xi, yi)
    if False:  # in old tutorial
        qmesh = pg.Mesh(2)
        for x1, y1 in zip(xi, yi):
            n1 = qmesh.createNode(x1, y1, 0.0)

        pg.interpolateSurface(mesh, qmesh)
        zi = pg.z(qmesh.positions())

    dt = np.sqrt(np.diff(xi)**2 + np.diff(yi)**2 + np.diff(zi)**2)
    ti = np.hstack((0., np.cumsum(dt)))
    ze = np.interp(te, ti, zi)
    xe = np.interp(te, ti, xi)
    ye = np.interp(te, ti, yi)
    ax.plot(xe, ye, '.')

    x2d = np.hstack((0., np.cumsum(np.sqrt(np.diff(xe)**2 + np.diff(ye)**2))))
    ALL = np.vstack((te, xe, ye, ze, x2d))
    posfile = 'pos/' + datfile.replace('.dat', '.txyz')
    np.savetxt('pos/' + datfile.replace('.dat', '.txyz'), ALL.T)

    for i in range(data.sensorCount()):
        po = pg.RVector3(round(x2d[i]/acc)*acc, 0., round(ze[i]/acc)*acc)
        data.setSensorPosition(i, po)

    data.save('2d/'+datfile, data.inputFormatString())

    for i in range(data.sensorCount()):
        po = pg.RVector3(round(xe[i]/acc)*acc, round(ye[i]/acc)*acc,
                         round(ze[i]/acc)*acc)
        data.setSensorPosition(i, po)

    data3d.add(data)

data3d.save('3d/'+profile.replace('.pro', '.dat'), data.inputFormatString())

ax.set_aspect('equal')
ax.grid(True)
plt.show()
