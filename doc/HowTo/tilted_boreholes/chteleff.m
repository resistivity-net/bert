Data=readunifile('eleff.dat');
%%
kana=Data.k; %point
knum=1./Data.rho; %ring
eleff=Data.r; % electrode effect = kana/knum
%%
subplot(211);loglog(abs(knum),abs(kana),'.');% crossplot
grid on; xlabel('k-num in  m');ylabel('k_ana in m');
subplot(212);loghist(abs(eleff));grid on; % histogram
xlabel('electrode effect (k-ana/k-num)');ylabel('frequency');
% epsprint(gcf,'elec-effect-alldata',1)
%%
%%
Data2=readunifile('18sep-original.ohm')
Data2.x=[Data.x(57:132);Data.x(1:56)];
Data2.y=[Data.y(57:132);Data.y(1:56)];
Data2.z=[Data.z(57:132);Data.z(1:56)];
map=[77:132 1:76]';
Data2.a=map(Data.a);
Data2.b=map(Data.b);
Data2.m=map(Data.m);
Data2.n=map(Data.n);
Data2.rho=Data2.rho.*knum./kana;
saveunifile('18sep-corrected.ohm',Data2);

