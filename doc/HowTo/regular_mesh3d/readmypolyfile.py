# -*- coding: utf-8 -*-
"""
Created on Tue Jan 18 18:20:52 2011

@author: Guenther.T
"""

import pygimli as pg

Poly = pg.Mesh()
with open('allBoundary.poly', 'r') as f:
    line1 = f.readline()
    nnodes = int(line1.split()[0])
    nodes = []
    for i in range(nnodes):
        pos = f.readline().split()
        p = pg.RVector3(float(pos[1]), float(pos[2]), float(pos[3]))
        n = Poly.createNodeWithCheck(p)
        n.setMarker(int(pos[4]))
        nodes.append(n)

    line2 = f.readline()
    nfaces = int(line2.split()[0])
    for i in range(nfaces):
        bla = f.readline()
        ind = f.readline().split()
        fa = Poly.createTriangleFace(nodes[int(ind[1])], nodes[int(ind[2])],
                                     nodes[int(ind[3])], 0)
        fa.setMarker(-2)  # for outer boundary (mixed boundary conditions)

print(Poly)
Poly.exportAsTetgenPolyFile('test.poly')
