mkdir -p mesh
ALTITUDE=164.5
TOLERANCE=0.1
QUAL2D=25
# create dam footprint and dam lines
matlab -wait -nojvm -r "mklines;exit"

# make meshes for inversion
createSurface -v -a topo.xyz -P poly.xyz -b 30 -p 7 -q 30 -m 0.0 -o mesh/mesh thomas-z0.dat
closeSurface -v -z 500 -a 0 -o mesh/mesh mesh/meshFine.poly # -> mesh/mesh.poly

# make a circular 2d world out of the x/y shore positions and mesh it
polyCreateWorld -d2 -C -t outline.xyz outline # ->outline.poly
dctriangle -q $QUAL2D outline # ->outline.bms
polyCreateFacet -v -o outline3d outline.bms # ->outline3d.poly
# bring to actual altitude
polyTranslate -z $ALTITUDE outline3d
polyConvert -V outline3d

polyCreateWorld -d2 -C -t foot2d.xz foot2d # ->fuzz2d.poly
# bring first point to (0,0) for later rotation
polyTranslate -y -$ALTITUDE foot2d
# mesh 2d poly
dctriangle -q $QUAL2D foot2d # ->foot2d.bms
# create 3d mesh out of it
polyCreateFacet -v -o foot3d foot2d.bms # ->foot3d.poly
# rotate into x-z plane
polyRotate -x 90 foot3d
# rotate in the x-y plane to the right position
X1=-320.9 # better from file
Y1=529.9
ANGLE=-40.586821694830192
X1=`head -1 foot.xyz|awk '{print $1}'`
Y1=`head -1 foot.xyz|awk '{print $2}'`
X2=`tail -1 foot.xyz|awk '{print $1}'`
Y2=`tail -1 foot.xyz|awk '{print $2}'`
ANGLE=`echo $X1 $X2 $Y1 $Y2|awk '{printf("%.12f",atan2($4-$3,$2-$1)*90/atan2(1,0))}'`
echo $X1 $X2 $Y1 $Y2 $ANGLE
polyRotate -z $ANGLE foot3d
# translate to actual first dam point position
polyTranslate -x $X1 -y $Y1 -z $ALTITUDE foot3d
# convert into vtk for paraview visualization
polyConvert -V foot3d # ->foot3d.poly

# polyMerge -t TOLERANCE mesh/mesh outline3d alles
# polyMerge -t TOLERANCE alles foot3d wirklichalles
# tetgen -pazVAC -q1.3 wirklichalles
# meshconvert -v -iT -BDVM -o wirklichalles wirklichalles.1
