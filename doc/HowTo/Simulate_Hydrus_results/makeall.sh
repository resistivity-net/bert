################################################################
# Part 1: Importing a Hydrus mesh and export its boundary as PLC
python << END
import pygimli as g

mesh=g.Mesh(3)
f=open('MESHTRIA.TXT','r')
for i in range(6):
    line1=f.readline()
    
nnodes=int(line1.split()[0])
ncells=int(line1.split()[1])
print nnodes, ncells
line1=f.readline()
nodes=[]
dx=0.01
for ni in range(nnodes):
    pos=f.readline().split()
    p=g.RVector3(float(pos[1])*dx,float(pos[2])*dx,float(pos[3])*dx*(-1.))
    n=mesh.createNode(p)
    nodes.append(n)

line1=f.readline()
line1=f.readline()
cells=[]
for ci in range(ncells):
    pos=f.readline().split()
    i,j,k,l=int(pos[1]),int(pos[2]),int(pos[3]),int(pos[4]),
    c=mesh.createTetrahedron(nodes[i-1],nodes[j-1],nodes[k-1],nodes[l-1])
    cells.append(c)

f.close()

print mesh
mesh.save('hydrus')
mesh.exportVTK('hydrus')

mesh.createNeighbourInfos()
for b in mesh.boundaries():
    if not ( b.leftCell() and b.rightCell() ):
        b.setMarker(1)

poly = g.Mesh()
poly.createMeshByBoundaries( mesh, mesh.findBoundaryByMarker( 1 ) );
poly.exportAsTetgenPolyFile('paraBoundary')
END
polyConvert -V paraBoundary
echo finished part 1
################################################################
# Part 2a: Create world, merge it with the parameter boundary & mesh
SIZE=100
polyCreateWorld -x $SIZE -y $SIZE -z $SIZE world
polyScale -z 0.5 world
echo merging
polyMerge world paraBoundary worldSurface
polyConvert -V worldSurface
polyAddVIP -x 0.1 -y 0.1 -z -0.1 -H worldSurface
tetgen -pazVAC worldSurface
meshconvert -vBDM -it -o worldBoundary worldSurface.1
echo finished part 2a
# Part 2b: Export world boundary as PLC and merge it dirty with parameters
python << END
import pygimli as g
worldBoundary = g.Mesh('worldBoundary.bms')
worldPoly = g.Mesh()
worldPoly.createMeshByBoundaries( worldBoundary, worldBoundary.findBoundaryByMarker( -2, 0 ) );
worldPoly.exportAsTetgenPolyFile( "worldBoundary.poly" )
END
polyConvert -V worldBoundary
# merge world and para without check
polyMerge -N worldBoundary paraBoundary allBoundary
polyConvert -V allBoundary
echo finished part 2b
# Part 2c: remove doubled Nodes by recounting
python << END
import pygimli as g

Poly=g.Mesh()
f=open('allBoundary.poly','r')
line1=f.readline()
nnodes=int(line1.split()[0])
nodes=[]
for i in range(nnodes):
    pos=f.readline().split()
    p=g.RVector3(float(pos[1]),float(pos[2]),float(pos[3]))
    n=Poly.createNodeWithCheck(p)
    n.setMarker(int(pos[4]))
    nodes.append(n)
    
line2=f.readline()
nfaces=int(line2.split()[0])
for i in range(nfaces):
    bla=f.readline()
    ind=f.readline().split()
    fa=Poly.createTriangleFace(nodes[int(ind[1])],nodes[int(ind[2])],nodes[int(ind[3])],0)
    fa.setMarker( -2 ) # for outer boundary (mixed boundary conditions)
f.close()
print Poly
Poly.exportAsTetgenPolyFile('test.poly')
END
polyConvert -V test
polyAddVIP -x 0.1 -y 0.1 -z -0.1 -H test
echo finished part 2c

########################################################################
# Part 3a: Check consistency, mesh outer box and merge with parameters
rm -f wrong.vtk right.vtk test.1.*
tetgen -d test
if [ -f test.1.face ]
then
    echo "Detected intersecting subfaces! See wrong.vtk"
    meshconvert -V -it -o wrong test.1
    return 1
else
    tetgen -pazVACY -q 1.2 test
    meshconvert -VBDM -it -o right test.1
fi
echo finished part 3a
# Part 3b: merge the inner and the outer mesh
python << END
import pygimli as g
# read inner and outer mesh
Outer=g.Mesh('right.bms')
Inner=g.Mesh('hydrus.bms')
print Outer
print Inner
# merge inner mesh into outer
for m,c in enumerate(Inner.cells()): 
        nodes = g.stdVectorNodes( )
        for i, n in enumerate( c.nodes() ):
            nodes.append( Outer.createNodeWithCheck( n.pos() ) )
                
        Outer.createCell( nodes, m+1 );
        
print Outer
Outer.save('mesh')
Outer.exportVTK('mesh')
END
echo finished part 3b