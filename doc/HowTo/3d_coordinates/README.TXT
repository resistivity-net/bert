To be included into standard bert (DIMENSION=2.5?)

File: doc/Howto/3d_coordinates
Task: do 2d inversion of profile with 3d coordinates and create appropriate vtk file
Problem: fit an appropriate (non-exact) line

1. Read data file and fit a profile line through electrodes
Input could be:
1a) 2d data file with 3d coordinates (x,y,z)
1b) 2d flat earth data file and additional GPS positions file

I suppose the latter case with gps positions for some (not necessarily all) electrodes
in a separate file gps.txt (profile distance, height, northing and easting).

A=g.RMatrix()
g.loadMatrixCol(A,'gps.txt')

After reading and plotting the coordinates we see typical errors of up to a few meters
due to GPS inaccuracies, which we want to eliminate. Therefore we fit a smooth curve by
using harmonic functions implemented in the pygimli function harmfit. From the resulting
x and y we compute the distance t along the tape, which is used for inversion.

tt, hh, yy, xx = A # tape distance, height, northing and easting
x = harmfit( xx, tt, nCoefficients=10 )[0]
y = harmfit( yy, tt, nCoefficients=10 )[0]
z = harmfit( hh, tt, nCoefficients=15 )[0]
t = N.hstack( ( 0., N.cumsum( N.sqrt( N.diff(x)**2 + N.diff(y)**2 ) ) ) ) # 2d distance

The number of coefficients nCoefficients defines the complexity of the fitted curve and
should be large enough to describe the course, but not too large to avoid small-scaled
oscillation. Experience with different numbers with easily give good values for any data.

P.plot(A[0],A[1],'bx-',A[0],z,'r-')
P.plot(A[3],A[2],'bx-',x,y,'r-')

2a. project positions onto profile:
We now load the data container and interpolate the (tape) coordinates xe onto the 2d
positions t and z. We then run through all electrodes and change their 3d position to
the 2d position before saving the data to a 2d file.

data = b.DataContainer('profile.dat')
xe   = [ pos[ 0 ] for pos in data.sensorPositions() ]    
t    = P.hstack( ( 0., P.cumsum( P.sqrt( P.diff( x )**2 + P.diff( y )**2 ) ) ) )
x2d  = P.interp( xe, tt, t )
z2d  = P.interp( xe, tt, z )
for i in range( data.sensorCount() ):
    pp = g.RVector3( x2d[ i ], 0.0, z2d[ i ] )
    data.setSensorPosition( i, pp )

data.save('profile2d.ohm','a b m n u i','x z')
    
Furthermore we create a map from the 2d to the 3d coordinates for result projection.

POS = N.vstack( ( x2d, x, y ) )
N.savetxt( 'pos.map', POS.T )

2b. calculate 2d and 3d geometric factors and correct u or R (not rhoa) by G_3d/G_2d
We could actually take into account that the profile is not strictly straight.
(G_2d>G_3d: u_2d<u_3d for same rhoa -> correct u_2d=u_3d*G_3d/G_2d)
Create standard meshes, refine and run dcmod with them.

NOT DONE YET

3. carry out 2d inversion and change positions in vtk file
A 2d inversion of the 2d data set is carried out using standard tools:

bertNew2DTopo profile2d.ohm > bert.cfg
# add some other useful options as value bounds or whatever
bert bert.cfg all

4. Projection of the result back to 3d domain
As a result we have a vtk file with 2d coordinates that need to be brought to 3d.
We read the mesh and save the node positions in the variable tn and zn:

mesh=g.Mesh()
mesh.importVTK('dcinv.result.vtk')
tn=[n.pos()[0] for n in mesh.nodes()]
zn=[n.pos()[1] for n in mesh.nodes()]

Then we load the previously saved map and interpolate the tn to real x and y:

A=N.loadtxt('pos.map').T
xn=N.interp(tn,A[0],A[1])
yn=N.interp(tn,A[0],A[2])

Finally we set the positions of the individual nodes and export the file.

for i,n in enumerate(mesh.nodes()):
    n.setPos(g.RVector3(xn[i],yn[i],zn[i]))
    
mesh.exportVTK('result3d.vtk')

