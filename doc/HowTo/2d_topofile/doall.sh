#!/bin/bash
infile='profile-topo.dat'
datfile='profile.dat'
outfile='profile.ohm'
topofile='topo.xz'

read line < $infile
nel=${line%#*}
line=`head -n $[nel + 3] $infile|tail -n 1`
ndata=${line%#*}
head -n $[nel + $ndata + 4] $infile > $datfile
line=`head -n $[nel + $ndata + 5] $infile|tail -n 1`
ntopo=${line%#*}
echo $nel $ndata $ntopo
tail -n $ntopo $infile > $topofile

python << END
import numpy as np
import pygimli as pg
import pybert as pb

data = pb.DataContainerERT('$infile')
print(data)
A  = np.loadtxt('$topofile').T
tt = np.hstack((0., np.cumsum(np.sqrt(np.diff(A[0])**2 + np.diff(A[1])**2))))
xe = [pos[0] for pos in data.sensorPositions()]
z  = np.interp(xe, A[0], A[1])
x  = xe[0] + np.hstack((0., np.cumsum(np.sqrt(np.diff(xe)**2 - np.diff(z)**2))))
de = np.sqrt(np.diff(x)**2 + np.diff(z)**2)

for i in range(data.sensorCount()):
    data.setSensorPosition(i, [x[i], 0.0, z[i]])

data.save('$outfile','a b m n u i r','x z')
END
