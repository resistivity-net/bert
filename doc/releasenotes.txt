BERT release notes

ToDo List

#UNREADY
enable reading AGI SuperSting data (*.stg)

2.2.3 (21.09.2017) based on pyGIMLi 1.0.1 (start real versioning)
reenable using PARADX (case DIMENSION=3, TOPOGRAPHY=3) for createSurface instead of prepareMeshRefinement
improve tutorial: wrong bertNew* names, CEM and replacement points
improve examples (e.g. 2dxh)
fix problem when python is not in PATH but given by PYTHON
reenable Resecs again (downgrade priority of Geosys)
enable importing non-integer electrode positions from Resecs

2.2.2 (21.08.2017)
mainly pyGIMLi changes to 1.0 standard
improved complex values handling
minor fixes

2.2.1 (21.07.2017) 
first Python 3.6 version
stable version available for Py 3.4, 3.5 and 3.6
improve tutorial (3D mesh generatoin, show misfit, typos, versions)
enable show misfit by bert target showmisfit
fix "bad substitution error" for updated MinGW/MSYS versions

2.2.0 (16.06.2017)
first version without C++ core library (all in GIMLi from now on)
(prior versions are not compatible with the git version anymore)
disambiguated geometric factor for 2D/3D cases


2.1.4 (02.06.2017) last version with C++ BERT core library
tutorial update: licence, version and opt targets, STARTMODEL, NOREFERENCE
add showMarker target showing the mesh with markers
enable picking timelapse frame number after show
bug fixes

2.1.3 (03.03.2017) 2nd Leipzig Workshop version
fix bug displaying small-scale data
enable KMIN independent of KMAX
fix LAMBDAIP bug
add reading res2dinv files (*.res2dinv) by bertNew*
improve tutorial: raw input files
enable ABEM *.amp files
improving BERT workshop talk document

2.1.2 (19.01.2017) 
first Python 3.5 build (from here default)
improve reading IRIS (Syscal or Elrec) pro files
improve SIP manager
improve ERT data view
new key LAMBDAIP for IP regularization strength
improve tutorial regarding IP inversion, add oak examples

2.1.1 (28.09.2016) stable post-workshop version 
only Python 3.4, last Python 3.4 version for a while
include modelling examples into binary installer
add showing topographic effect by target show topoeff
add demonstrate topographic correction (3 images) by target showtopocorr
 lots of bug fixes

2.1.0 (21.09.2016) Workshop Leipzig edition (not fully stable)
heavily updated tutorial regarding
    timelapse section
    
improving examples (mainly Python-based ones)
improve SIPmanager
several bug fixes and cleanups

2.0.19 (17.08.2016)
enable multi-phase technology (MPT) SIP data
add multiple gradient data scheme
improve SIP manager

2.0.15 (24.03.2016) Easter Egg

2.0.14 (26.11.2015) GELMON edition (quite stable)

2.0.5-2.0.14 (2015)

2.0.5-2.0.7 (2014)

2.0.1-2.0.4 (2013)