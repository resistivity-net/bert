Changes from BERT 1 to BERT 2

commands
primpot/default-
interpolate only for KEEPPRIMPOT
filter instead of correct
calcSensM entf�llt

New variables for CFG file (migration GUIDE)
RMIN/MAX, KMIN/MAX, ERRMAX - filtering
DOWNWEIGHT - filter method
RECALCJACOBIAN - full recalc
TOTALPOTENTIAL - prevents singularity removal (dcinv -S)
REGIONFILE 
ZWEIGHT <-ZPOWER
SAVEALOT - debug
RESOLUTIONFILE - file with resolution indices (dcinv -r)
CHI1OPT - chi^2=1 optimization

New plotting variables (for using pytripatch)
PYTRIPATCH - path to pytripatch.py
cMin/cMax - minimum/maximum color axis
USECOVERAGE - incorporate coverage using alpha shading
PLOTELECTRODES - plot electrodes as small dots

No more existing variables
SPACECONFIG - auto
FILTERVALUES - MAX/MIN switches
RHOSTART - done by REGIONFILE
CONSTRAINT - done by REGIONFILE
LINSOLVER - auto
CYLINDER/CIRCLE
ZPOWER -> ZWEIGHT

Noch zu �berpr�fen/einzubauen
dcinv -M und -T options (-T default, -M per switch?)
LAMBDADECREASE
SENSMATDROPTOL/MAXMEM
ICDROPTOL
SINGVALUE
DIPOLE
