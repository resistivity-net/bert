function Data2=deldeadelecs( Data )

% DELDEADELECS - Delete dead (not used) electrodes
% DataNew = deldeadelecs( Data )
% Data, DataNew - data structure

map = zeros( size(Data.elec,1), 1 );
used = unique( [Data.a;Data.b;Data.m;Data.n] );
if used(1)==0, used(1)=[]; end
% notused=setxor(used,1:size(Data.elec,1));
% Data.elec(notused,:)=[];
Data2 = Data;
Data2.elec = Data.elec(used,:);
if isfield( Data, 'x' ), Data2.x = Data.x(used); end
if isfield( Data, 'y' ), Data2.y = Data.y(used); end
if isfield( Data, 'z' ), Data2.z = Data.z(used); end    
map(used) = 1:length(used);
map = [ 0; map(:) ];
Data2.a = map( Data.a + 1 );
Data2.b = map( Data.b + 1 );
Data2.m = map( Data.m + 1 );
Data2.n = map( Data.n + 1 );
if isfield(Data,'r'), Data2.r = Data.r; end
if isfield(Data,'k'), Data2.k = Data.k; end
if isfield(Data,'err'), Data2.err = Data.err; end
if isfield(Data,'ip'), Data2.ip = Data.ip; end
if isfield(Data,'i'), Data2.i = Data.i; end
if isfield(Data,'u'), Data2.u = Data.u; end
if isfield(Data,'rez'), Data2.rez = Data.rez; end
if isfield(Data,'rho'), Data2.rho = Data.rho; end
if isfield(Data,'zweid'), Data2.zweid = Data.zweid; end
if isfield(Data,'nr'), Data2.nr = Data.nr; end
if isfield(Data,'names'), Data2.names = Data.names; end