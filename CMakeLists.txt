# main cmake configuration file

cmake_minimum_required(VERSION 2.8.7)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake")

if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release CACHE STRING "Sets the configuration to build (Release, Debug, etc...)")
endif()

project(bert)
add_custom_target(pybert)

set(BERT_VERSION_MAJOR 2)
set(BERT_VERSION_MINOR 2)
set(BERT_VERSION_PATCH 10)
set(BERT_VERSION ${BERT_VERSION_MAJOR}.${BERT_VERSION_MINOR}.${BERT_VERSION_PATCH})

set(PACKAGE_NAME \"${PROJECT_NAME}\")
set(PACKAGE_VERSION \"${BERT_VERSION}\")
set(PACKAGE_BUGREPORT \"carsten@resistivity.net\")
set(PACKAGE_AUTHORS \"carsten@resistivity.net thomas@resistivity.net\")

include(CheckIncludeFileCXX)
find_package(Threads REQUIRED)

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")

    set(CMAKE_CXX_FLAGS_RELEASE "-O2 -pipe -ansi -Wall -Wno-long-long -Wno-unused-result -Wno-unused-variable")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wno-unused-value -Wno-strict-aliasing -Wno-unused-local-typedefs")

    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -p -O2 -Wall -ansi -pedantic -fno-omit-frame-pointer")

    if(NOT WIN32 AND ASAN)
        set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fsanitize=address")
    endif()

    set(CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS "-Os")

    #add_definitions(-std=c++11)
    #add_definitions(-std=gnu++0x)

    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
        set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wno-overloaded-virtual")
        set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Qunused-arguments")
    else()
        # not for mingw set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fno-omit-frame-pointer -g -fsanitize=address" )
    endif()

    if(WIN32)
    endif(WIN32)

elseif(MSVC)
    # MSVC complain a lot of possible unsecure std function
    add_definitions(-D_SCL_SECURE_NO_WARNINGS)
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
endif()

find_package(GIMLi REQUIRED)

if(NOT THIRDPARTY_DIR)
    if (NOT ADDRESSMODEL)
        if("${CMAKE_SIZEOF_VOID_P}" EQUAL "8")
            message(STATUS "Target is 64 bits")
            set (ADDRESSMODEL "64")
        else()
            message(STATUS "Target is 32 bits")
            set (ADDRESSMODEL "32")
        endif()
    endif()

    set(TARGETNAME "-${CMAKE_CXX_COMPILER_ID}-${CMAKE_CXX_COMPILER_VERSION}-${ADDRESSMODEL}")
    set(ENV{TARGETNAME} ${TARGETNAME})
    set(THIRDPARTY_DIR ${GIMLI_INCLUDE_DIR}/../../../thirdParty/)
    set(EXTERNAL_DIR ${THIRDPARTY_DIR}/dist${TARGETNAME})
    message(STATUS "ThirdParty set to: ${THIRDPARTY_DIR}")
    message(STATUS "External set to: ${EXTERNAL_DIR}")
endif()

# if (BOOST_ROOT)
#     get_filename_component(BOOST_ROOT "${BOOST_ROOT}" ABSOLUTE)
# 	set(Boost_INCLUDE_DIR ${BOOST_ROOT}/include)
# else()
# 	IF(WIN32)
# 		set(Boost_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/../../boost/include)
# 	endif(WIN32)
# endif()

# find_package(Boost 1.46.0 COMPONENTS system thread REQUIRED)

# if (Boost_INCLUDE_DIR)
#     message(STATUS "boost include: ${Boost_INCLUDE_DIR}")
#     include_directories(${Boost_INCLUDE_DIR})
# 	set (CMAKE_REQUIRED_INCLUDES ${Boost_INCLUDE_DIR})
# 		check_include_file_cxx(boost/interprocess/managed_shared_memory.hpp HAVE_BOOST_INTERPROCESS_MANAGED_SHARED_MEMORY_HPP
# 						   HINT ${Boost_INCLUDE_DIR})
# 		check_include_file_cxx(boost/bind.hpp BOOST_BIND_FOUND
# 						   HINT ${Boost_INCLUDE_DIR})
# 		set(BOOST_BIND_FOUND ON)
# 	if (HAVE_BOOST_INTERPROCESS_MANAGED_SHARED_MEMORY_HPP)
# 		message(STATUS "HAVE_BOOST_INTERPROCESS_MANAGED_SHARED_MEMORY_HPP ${HAVE_BOOST_INTERPROCESS_MANAGED_SHARED_MEMORY_HPP} ${Boost_INCLUDE_DIR}")
# 		option (USE_IPC "Use support for interprocess communication" ON)
# 	else()
# 		message(STATUS "HAVE_BOOST_INTERPROCESS_MANAGED_SHARED_MEMORY_HPP 0 ${HAVE_BOOST_INTERPROCESS_MANAGED_SHARED_MEMORY_HPP} ${Boost_INCLUDE_DIR}")
# 		set(USE_IPC  OFF)
# 		set(HAVE_BOOST_INTERPROCESS_MANAGED_SHARED_MEMORY_HPP 0 )
# 	endif()
# 	if (Boost_THREAD_FOUND)
#         add_definitions( -DHAVE_BOOST_THREAD_HPP )
#     endif (Boost_THREAD_FOUND)
# endif (Boost_INCLUDE_DIR)

if (PYVERSION)
    set(Python_ADDITIONAL_VERSIONS ${PYVERSION})
endif()
find_package(PythonInterp)

if (WIN32)
    get_filename_component(PYTHONPATH "${PYTHON_EXECUTABLE}" PATH )
    #set (PYTHON_LIBRARY ${PYTHONPATH}/libs/python${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}.lib)
	set (PYTHON_LIBRARY ${PYTHONPATH}/libs/libpython${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}.a)
    set (PYTHON_INCLUDE_DIR ${PYTHONPATH}/include)
else()
    set(Python_ADDITIONAL_VERSIONS ${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR})
    set(PythonLibs_FIND_VERSION ${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR})
endif()
find_package(PythonLibs)

if (PYTHON_EXECUTABLE)
    set (PYTHON_FOUND TRUE)
endif()

message(STATUS "**********************************************************************")
message(STATUS "************************* Dependencies found *************************")
message(STATUS "**********************************************************************")
message(STATUS "GIMLI_FOUND        :${GIMLI_FOUND}    GIMLI_LIBRARIES: ${GIMLI_LIBRARIES} GIMLI_INCLUDE_DIR: ${GIMLI_INCLUDE_DIR}")
message(STATUS "PYTHON_FOUND       :${PYTHON_FOUND}   PYTHON_EXECUTABLE: ${PYTHON_EXECUTABLE}" )
message(STATUS "**********************************************************************")

if (PYTHON_EXECUTABLE
    AND PYTHON_FOUND
)
    set (PYBERT 1)
    message(STATUS "Everything looks good. BERT can be build.")
    message(STATUS "Run: make all (for dcmod, dcedit and dcinv)")
    message(STATUS "And: make bert1 (for the polytools / mesh utilities)")
    message(STATUS "**********************************************************************")
    message(STATUS "")
    message(STATUS "")
else()
    message (FATAL_ERROR "bert cannot be build due to some missing packages.
Check above for python and gimli")
endif ()

################################################################################
set (LIBRARY_INSTALL_DIR lib)
set (INCLUDE_INSTALL_DIR include/bert/)

set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})
set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})
set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY_MINSIZEREL ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})
set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELWITHDEBINFO ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY})
if (WIN32)
    set (CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
else()
    set (CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
endif()
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY_MINSIZEREL ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELWITHDEBINFO ${CMAKE_LIBRARY_OUTPUT_DIRECTORY})

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY_MINSIZEREL ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

# set up install sub-directories
if (CMAKE_SIZEOF_VOID_P EQUAL 8 AND EXISTS "${CMAKE_INSTALL_PREFIX}/lib64")
    set( BERT_LIB_INSTALL_DIR lib64 )
elseif (CMAKE_SIZEOF_VOID_P EQUAL 4 AND EXISTS "${CMAKE_INSTALL_PREFIX}/lib32")
    set( BERT_LIB_INSTALL_DIR lib32 )
else()
    set( BERT_LIB_INSTALL_DIR lib )
endif()

set( BERT_VER_INSTALL_SUBDIR "/${CMAKE_PROJECT_NAME}-${BERT_VERSION_MAJOR}" )
set( BERT_DATA_INSTALL_DIR "share${BERT_VER_INSTALL_SUBDIR}" )
set( BERT_DOC_INSTALL_DIR "share/doc${BERT_VER_INSTALL_SUBDIR}" )

set( BERT_PKGCONFIG_INSTALL_DIR "${BERT_LIB_INSTALL_DIR}/pkgconfig" )
set( BERT_MODULE_INSTALL_DIR "${BERT_LIB_INSTALL_DIR}/${CMAKE_PROJECT_NAME}-${BERT_VERSION_MAJOR}.${BERT_VERSION_MINOR}" )
set( BERT_SAMPLE_INSTALL_DIR "${BERT_MODULE_INSTALL_DIR}" ) # TODO: put into /samples subdir!
set( BERT_INCLUDE_INSTALL_DIR "include${BERT_VER_INSTALL_SUBDIR}" )

################################################################################
# descend into subdirs
################################################################################

add_subdirectory(apps)

add_custom_target(bert1)
add_subdirectory(dcfemlib EXCLUDE_FROM_ALL)
