#!/bin/bash

mkdir -p pot
mkdir -p mesh
MESH=mesh/world
#set -x

MESHGEN=tetgen
VERBOSE='-v'
DX=-0.1
QUALITY=1.2
BOUNDARY=100

createMesh(){
 
    if [ $style -eq 1 ]; then 
        polyCreateWorld -x $BOUNDARY -y $BOUNDARY -z $BOUNDARY -m1 $MESH 
        polyCreateCube -m2 $MESH'_layer'
        polyScale -x 50 -y 50 -z 0.5 $MESH'_layer'
        polyTranslate -z -0.25 $MESH'_layer'
    else
        polyCreateWorld -x $BOUNDARY -y $BOUNDARY -z $BOUNDARY -m2 $MESH 
        polyCreateCube -m1 $MESH'_layer'
        polyScale -x 50 -y 50 -z 50 $MESH'_layer'
        polyTranslate -z -25.5 $MESH'_layer'
    fi

    polyMerge $MESH $MESH'_layer' $MESH

    polyAddProfile -x 0.0 -y 0.0 -z -0.0 -X 6.0 -Y 0.0 -Z -0.0 -n7 $MESH
    polyRefineVIPS -z $DX $MESH 

    polyConvert -V $MESH
    mv $MESH.vtk $MESH'-'$style'-poly.vtk'

    [ "$VERBOSE" = "-v" ] && TETVERBOSE='V' || TETVERBOSE='Q'   
    $MESHGEN -pazAC'q'$QUALITY $MESH

    meshconvert -d3 $VERBOSE -o $MESH-$style -DBVEM -iT $MESH.1 
}

echo "1 1" > rho.map
echo "2 10" >> rho.map

echo "-1 -1" > bnd.map
echo "-2 -2" >> bnd.map

echo "0.5 10" > 2Layer.model
echo "100 1" >> 2Layer.model
dc1dinv -m 2Layer.model 7pp.shm

style=1; createMesh
dcmod -v -V -p pot/num-$style -s 7pp.shm -a rho.map -b bnd.map $MESH-$style.bms
dcedit 7pp.ohm -f 'a b m n rhoa' -o 7pp-$style.dat

style=2; createMesh
dcmod -v -V -o pot/num-$style -p pot/num-$style -s 7pp.shm -a rho.map -b bnd.map $MESH-$style.bms
dcedit 7pp.ohm -f 'a b m n rhoa' -o 7pp-$style.dat

echo " analyt"
tail -n6 7pp.shm.sim | cut -f 5 | tr -s '\n' ' '
echo -e "\n style 1"
tail -n6 7pp-1.dat | cut -f 5 | tr -s '\n' ' '
echo -e "\n style 2"
tail -n6 7pp-2.dat | cut -f 5 | tr -s '\n' ' '
echo -e "\n"
