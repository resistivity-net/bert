#!/bin/bash
# simple test for direct current four source points, test analytical as well es numerical solution
# Sources are on surface and within the domain, so we can also test mirror sources.
# dont call it directly
[ "$#" -eq "0" ] && echo "Don't call this script directly." && exit

[ "$#" -eq "1" ] && TOLERANCE=$1

source ./commonForTesting.sh

testSolution(){

    if [ "$DIPOLE" == "-D" ]; then
        [ -f test.ohm ] && testPolData test.ohm $1
    elif [ ! $withReference  == "1" ]; then
        [ -f num.collect ] && testPotential num.collect $1
        [ "$?" == "0" ] && return 0

        [ -f test.ohm ] && testPolData test.ohm $1
    fi
    [ -f test.ohm ] && testDipolData test.ohm $1
}

[ $SRTEST ] && RHOMAP=srrho.map || RHOMAP=rho.map

MESH=mesh/world
mkdir -p pot
mkdir -p mesh
rm -rf pot/*
rm -rf mesh/*
rm -rf num.collect test.ohm

source polyScripts.sh

# start with creating a PLC
if [ "$dimension" == "3" ]; then
    polyCreateWorld -d $dimension -x1000 -y1000 -z500 -m1 $MESH
    depth='-z'; DEPTH='-Z';  
else
    polyCreateWorld -d $dimension -x1000 -z500 -m1 $MESH
    depth='-z'; DEPTH='-Z';  
fi
    
case $electrodes in
"node" )
    polyAddProfile -x 0.0 -X 3.0 $depth  0.0 $DEPTH  0.0 -n 4 $MESH
    polyAddProfile -x 0.0 -X 3.0 $depth -1.0 $DEPTH -1.0 -n 4 $MESH
    polyRefineVIPS $depth -0.1 $MESH;;
"cem" )
    for x in 0 1 2 3; do        
        polyScriptAddCEM id=$[ x ] x=$x
    done
    polyAddProfile -x 0.0 -X 3.0 $depth -1.0 $DEPTH -1.0 -n 4 $MESH
    polyRefineVIPS $depth -0.1 $MESH;;
"free" )
    polyAddProfile -x0.0 -X 3.0 $depth -0.1 $DEPTH -0.1 -n 4 -m 0 $MESH
    polyAddProfile -x0.0 -X 3.0 $depth -1.1 $DEPTH -1.1 -n 4 -m 0 $MESH
esac

if [ "$withReference" == "1" ]; then REFMARKER='-m -999'; fi
polyAddVIP -x0 $depth -2 $REFMARKER $MESH
polyAddVIP -x0 $depth -2.01 $MESH

if [ "$withCalibration" == "1" ]; then CALMARKER='-m -1000'; fi
polyAddVIP -x-1.0 $depth 0.0 $CALMARKER $MESH
   
if [ "$dimension" == "3" ]; then
    polyCreateCube -m2 $MESH'_cube'
    polyTranslate -z -2 $MESH'_cube'
    polyMerge $MESH $MESH'_cube' $MESH
    polyConvert -o $MESH'-poly' -V $MESH 
    
    tetgen -QpazACq$QUALITY3D $MESH
    meshconvert $p2mesh $verbose -d $dimension -M -V -iT -BD -o $MESH $MESH'.1'
else
    echo "0 -100" > .tmp
    echo "0 -101" >> .tmp
    echo "1 -101" >> .tmp
    echo "1 -100" >> .tmp
    echo "0 -100" >> .tmp
    
    polyAddProfile -i .tmp $MESH
    polyAddVIP -x 0.5 -y -100.5 -R -m2 $MESH
    rm -f .tmp

    dctriangle $verbose -q34.4 $MESH
    meshconvert $p2mesh $verbose -d $dimension -V -BD -o $MESH $MESH.bms
fi

echo "8"    > test.shm
[ $dimension == 3 ] && echo "# x z" >> test.shm || echo "# x y" >> test.shm
echo "0  0" >> test.shm
echo "1  0" >> test.shm
echo "2  0" >> test.shm
echo "3  0" >> test.shm
echo "0 -1" >> test.shm
echo "1 -1" >> test.shm
echo "2 -1" >> test.shm
echo "3 -1" >> test.shm
echo "10"    >> test.shm
echo "# a b m n"    >> test.shm
echo "1 2 3 4"    >> test.shm
echo "3 4 1 2"    >> test.shm
echo "1 0 3 4"    >> test.shm
echo "3 4 1 0"    >> test.shm
echo "5 6 7 8"    >> test.shm
echo "7 8 5 6"    >> test.shm
echo "0 5 8 7"    >> test.shm
echo "8 7 0 5"    >> test.shm
echo "1 0 2 0"    >> test.shm
echo "0 1 0 2"    >> test.shm

echo "dimension=$dimension", "analytical=$analytical, electrodes=$electrodes, p2mesh=$p2mesh, singremoval=$SRTEST", \
     "Dipol-current pattern=$DIPOLE" TOLERANCE=$TOLERANCE
cmd="dcmod $SRTEST $DIPOLE $withData $verbose $analytical $neumann -a $RHOMAP -o num $MESH"
echo $cmd

if !($cmd); then
    echo "FAIL running: " $cmd
    exit
fi

testSolution $TOLERANCE
#echo "dimension=$dimension", "analytical=$analytical, electrodes=$electrodes, p2mesh=$p2mesh, singremoval=$SRTEST", TOLERANCE=$TOLERANCE
#dcmod $SRTEST $withData $verbose $analytical $neumann -a $RHOMAP -o num $MESH && testSolution $TOLERANCE
if [ "$?" == "0" ]; then
    echo "Fail tolerance check"
    echo "for $cmd"
    exit
else
    echo "OK"
fi

return
