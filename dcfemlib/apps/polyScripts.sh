#!/bin/bash

if [ $# -gt 0 ]; then
    if [ $1 == "help" ]; then
        echo "Available scripts are:"
        echo -e "\t polyScriptAddCEM"
        echo "use: source polyScripts.sh; 'Scriptname'"
    fi
fi

meshTetgen(){

if [ $# -lt 2 ]; then
    echo "usage: meshTetgen $MESH quality"
    echo "except: a $MESH.poly and a desired mesh quality."
    echo "delivers a resulting $MESH.bms $MESH.medit $MESH.vtk"
    echo "or a $MESH-intersecting.vtk if something goes wrong."
fi

MESH=$1
TETQUALITY=$2

if ! (tetgen -pazVACq$TETQUALITY $MESH); then
    echo "Tetgen fails, try to test for intersecting facets."
    tetgen -pzVAd $MESH
    meshconvert -v -it -V -o $MESH-intersecting $MESH.1
    exit
fi
    meshconvert -v -it -BDV -o $MESH $MESH.1
    rm -rf $MESH.1.*
}

polyScriptAddCEM(){
    ID__=0; X__=0; Y__=0; Z__=0; nX__=0; nY__=0; nZ__=-1; dX__=0.01; dY__=0.01; dZ__=0.1; cyl__=0;
    poly__=$MESH shape__=""

    if [ $# -lt 1 ]; then
        echo "usage addCEMElectrode id=$ID [x/y/z=positon nx/ny/nz=direction dx/dy/dz=size cyl=cylindersegments shape=e-shape poly=$poly]"
        echo "position -  x-y-z positon of center electrode-foot"
        echo "direction - nx-ny-nz normalized direction from center electrode-foot, [0.0, 0.0, -1.0]"
        echo "size - dx-dy-dz diameter in x/y/z direction for the default cube or cylinder [0.01, 0.01, 0.1] "
        echo "cylindersegments - amount of cylindersegments instead of a cube like shape [0 = cube]"
        echo "shape - a predefined poly for the electrode shape dx/dy/dz will be ignored"
        echo "poly is the target PLC where the the CEM will be add. (default is variable "\$"MESH but this can be changed be the set of poly)"
        echo "example:"
        echo "polyScriptAddCEM id=1 x=1 y=1 z=0 poly=mesh/mesh"
        echo -e "\t add a cube with dimensions 0.01m x 0.01m x 0.1m at position x=1 y=1 to the PLC mesh/mesh.poly as CEM-electrode 1"
        return
    fi

    ARGS=${@}

    for ARG in $ARGS; do
        VAR=`echo -e ${ARG/=/'\t'} | cut -f1`
        VAL=`echo -e ${ARG/=/'\t'} | cut -f2`

        case $VAR in
            "id" | "ID" )  ID__=$VAL ;;
            "x"  | "X" )    X__=$VAL ;;
            "y"  | "Y" )    Y__=$VAL ;;
            "z"  | "Z" )    Z__=$VAL ;;
            "nx" | "nX" )  nX__=$VAL ;;
            "ny" | "nY" )  nY__=$VAL ;;
            "nz" | "nZ" )  nZ__=$VAL ;;
            "dx" | "dX" )  dX__=$VAL dY__=$VAL;;
            "dy" | "dY" )  dY__=$VAL ;;
            "dz" | "dZ" )  dZ__=$VAL ;;
            "cyl" | "Cyl" )  cyl__=$VAL ;;
            "poly" | "Poly" )  poly__=$VAL ;;
            "shape" | "Shape" )  shape__=$VAL ;;
        esac
    done
    echo "ID=$ID__ x=$X__ y=$Y__ z=$Z__"
    echo "  shape=$shape__ nx=$nX__ ny=$nY__ nz=$nZ__ dx=$dX__ dy=$dY__ dz=$dZ__ cyl=$cyl__ poly=$poly__"

    ePoly__=$poly__'_el'

    if [ "$shape__" != "" ]; then
        cp $shape__ $ePoly__.poly
    else
        if [ $cyl__ -gt 2 ]; then
            polyCreateCube -Z -s $cyl__ -H $ePoly__
        else
            polyCreateCube -H $ePoly__
        fi
        polyScale -x $dX__ -y $dY__ -z $dZ__ $ePoly__
        polyTranslate -z `echo " - $dZ__ / 2.0" | bc -l` $ePoly__
    fi

    polyRotate    -T -x $nX__ -y $nY__ -z $nZ__ $ePoly__
    polyTranslate -x $X__ -y $Y__ -z $Z__ $ePoly__
    polyAddVIP    -B -m $[ -10000 - $ID__ ] $ePoly__
    polyMerge $poly__ $ePoly__ $poly__
}