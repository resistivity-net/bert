// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "fem.h"
#include "datamap.h"
#include "elementmatrix.h"
#include "taucswrapper.h"
#include "ldlwrapper.h"
#include "cholmodWrapper.h"

using namespace MyVec;

#include <sstream>
using namespace std;

int applyHomogDirichletBC( RDirectMatrix & S, RVector & rhs, const set < long > & nodesSet, bool verbose ){
  int node = 0;
  int nodesSetSize = nodesSet.size();
  int verboseStep = (int)rint( nodesSetSize / 100. );
 
  for ( set< long >::const_iterator it = nodesSet.begin(); it != nodesSet.end(); it++){

    if ( verbose ){
      //&& nodesSetSize%verboseStep == 0 ) cout << "."; 
    }

    node = (*it);
    if ( verbose ) cout << "\rNode " << node << " u = 0 ";
    S.clearRow( node + 1 );
    S.clearColumn( node + 1 );
    S.SetVal( node + 1, node + 1, 1.0 ); // S noch alt nummeriert von 1 -> n

    rhs[ node ] = 0.0;
  }

  if ( verbose ) cout << "U_dir done" << endl;
  return 1;
}

int applyInHomogDirichletBC( RDirectMatrix & S, RVector & rhs, const map < long, double > & nodesMap, bool verbose ){
  //  cout << WHERE_AM_I << endl;
  long node = 0;

  RVector u_0( rhs.size(), 0.0 );

  int count = 0;
  for ( map < long, double >::const_iterator it = nodesMap.begin(); it != nodesMap.end(); it++){
    u_0[ (*it).first ] = (*it).second;
  }

  rhs -=  S * u_0 ;

  //  ( *rhs_ ).showSparse();
  if ( verbose ) cout << "U_dir maped ..." << endl;
  int verboseStep = (int)rint( nodesMap.size() / 10. );
  if ( verbose ) cout << "Apply U_dir "; 
  for ( map < long, double >::const_iterator it = nodesMap.begin(); it != nodesMap.end(); it++){
    node = (*it).first;
    
    if ( verbose && nodesMap.size()%verboseStep == 0 ) cout << "."; 

    if ( verbose ) cout << "U_dir at node \t\t" << node << "\t\t" << 1.0 * (*it).second << "\r";

    S.clearRow( node + 1 );
    S.clearColumn( node + 1 );
    S.SetVal( node + 1, node + 1, 1.0 ); // S noch alt nummeriert von 1 -> n
    rhs[ node ] = (*it).second;
  }
  if ( verbose ) cout << " done." << endl; 

  //  S.show();
  //   ( *rhs_ ).save( "rhs.test" );
  return 1;
}

void initKWaveList( const BaseMesh & mesh, vector < double > & kValues, vector < double > & weights, bool verbose ){
  vector < RealPos > sources;
  initKWaveList( mesh, kValues, weights, sources, verbose );
}

void initKWaveList( const BaseMesh & mesh, vector < double > & kValues, 
		    vector < double > & weights, const vector < RealPos > & sources, bool verbose ){
   // verbose = true;
  vector < RealPos >  sourcesPos;

  if ( sources.size() == 0 ){
    sourcesPos = mesh.positions( mesh.getAllMarkedNodes( -99 ) );
  } else sourcesPos = sources;

  int nElecs = sourcesPos.size();

  if ( nElecs == 0 ) {
    cerr << WHERE_AM_I << " No sources found, nothing to do." << endl;
    exit( 1 );
  }

  //  double xmin = 9e99, xmax = -9e99, ymin = 9e99, ymax = -9e99, tmp = 0.0;
  //  double rMax = sqrt( pow( xmax - xmin, 2.0 ) + pow( ymax - ymin, 2.0) );
  double rMin = 9e99, rMax = -9e99, dist = 0;

  for ( int i = 0; i < nElecs; i ++ ){
    for ( int j = i + 1; j < nElecs; j ++ ){
      dist = sourcesPos[ i ].distance( sourcesPos[ j ] );
      rMin = min( dist, rMin );
      rMax = max( dist, rMax );
    }
  }

  //  cout << xmin << "\n" << xmax << "\n" << ymin << "\n" << ymax  << "\n" << endl;

//   RealPos startPos = sourcesPos[ 0 ];
//   for ( int i = 1; i < nElecs; i ++ ){
//     tmp = startPos.distance( sourcesPos[ i ] );
//     if ( tmp < rMin ) rMin = tmp;
//   }

  rMin /= 2.0;
  rMax *= 2.0;
  if ( verbose ) cout << "rMin = " << rMin << ", rMax = "<< rMax << endl;

  vector < double > k, w; 
  int nGauLeg = -1;

  if ( nGauLeg < 0) nGauLeg = static_cast< int >( floor( 6 * log10( rMax / rMin ) ) );
  
  if ( nGauLeg < 4 ){
    cerr << "*** Warning! Ng " << nGauLeg << " < 4 nicht empfohlen --> Ng = 4 erzwungen.\n";
    nGauLeg = 4;
  }

  int nGauLag = 4;
  //nGauLag = 10; nGauLeg = 10;

  GaussLegendre( 0.0, 1.0, nGauLeg, k, w );

  double k0 = 1.0 / ( 2.0 * rMin );
  
  for ( int i = 0; i < nGauLeg; i++ ){
    kValues.push_back( k0 * k[ i ] * k[ i ] );              // RueckSubstitution der Gauss Integration
    weights.push_back( 2.0 * k0 * k[ i ] * w[ i ] / PI_ );  // RueckSubst. GewichtsTERM Gauss Integration
  }

  GaussLaguerre( nGauLag, k, w );

  for ( int i = 0; i < nGauLag; i++){
    kValues.push_back( k0 * ( k[ i ] + 1 ) );           // RueckSubstitution der Gauss Integration
    weights.push_back( k0 * exp( k[ i ] ) * w[ i ] / PI_ ); // RueckSubst. GewichtsTERM Gauss Integration
  }

  if ( verbose ) cout << "NGauLeg + NGauLag, St�tzstellen zur inversen Fouriertransformation: " << nGauLeg << " + " << nGauLag << endl;
}

double boundaryAlphaDC3D( const RealPos & sourcePos, BaseElement & boundary, SpaceConfigEnum config ){
  double mirrorPlaneZ = 0.0;

  RealPos sourcePosMirror( sourcePos );
  sourcePosMirror.setZ( 2.0 * mirrorPlaneZ - sourcePos.z() );

  RealPos facetPos( boundary.averagePos() );
  RealPos norm( boundary.normVector() );
  RealPos radius( sourcePos - facetPos );
  RealPos radiusMirror( sourcePosMirror - facetPos );
  double rAbs = radius.abs();
  double rMirAbs = radiusMirror.abs();
  double alpha = 0.0;
  switch( config ){
  case HALFSPACE:
    alpha = fabs( radius.scalar( norm ) ) / ( rAbs * rAbs );  
    break;
  case FULLSPACE:
    TO_IMPL
    break;
  case MIRRORSOURCE:
    // nach Bing & Greenhalgh
    alpha =( ( rMirAbs * rMirAbs ) * fabs( radius.scalar( norm ) ) /  rAbs +
	     ( rAbs * rAbs) * fabs( radiusMirror.scalar( norm ) ) / rMirAbs ) / 
      ( rMirAbs * rAbs * ( rAbs + rMirAbs ) );
    //    cout << alpha << endl;
    break;
  default:
    cerr << WHERE_AM_I << " Warning SpaceConfigEnum = " << config << endl;
    break;
  }

  return alpha;
}

//double boundaryBetaDC3D( const RealPos & sourcePos, BaseElement & boundary, SpaceConfigEnum config ){
//   //** tests can be found in test/mesh/angletest.cpp

//   RealPos sourcePosMirror( sourcePos );
//   double mirrorPlaneZ = 0.0;
//   sourcePosMirror.setZ( 2 * mirrorPlaneZ - sourcePos.z() );

//   RealPos facetPos( boundary.averagePos() );
//   RealPos norm( boundary.normVector() );

//   double radius = sourcePos.distance( facetPos );
//   double radiusMirror = sourcePosMirror.distance( facetPos );

//   double theta = 0.0;
//   double thetaMirror = 0.0;
//   //** check ob der Normvektor ins innere des Gebietes oder nach aussen zeigt
//   if ( ( sourcePos - facetPos + norm ).abs() >  ( sourcePos - facetPos - norm ).abs() ){
//     theta = facetPos.angle( sourcePos, facetPos + norm );
//     thetaMirror = facetPos.angle( sourcePosMirror, facetPos + norm );
//   } else {
//     theta = facetPos.angle( sourcePos, facetPos - norm );
//     thetaMirror = facetPos.angle( sourcePosMirror, facetPos - norm );
//   }

//   double beta = 0.0;

//   switch( config ){
//   case HALFSPACE:
//     beta =  cos( theta ) / radius;
//   case FULLSPACE:
//     beta =  cos( theta ) / radius;
//     //    cerr << WHERE_AM_I << " Please insert function." << endl;
//     break;
//   case MIRRORSOURCE:
//     // gefunden in Bing & Greenhalgh
//     beta = ( radiusMirror * radiusMirror * cos( theta ) + 
// 	     radius * radius * cos( thetaMirror ) ) / 
//       ( radiusMirror * radius * ( radius + radiusMirror ) );
//     break;
//   default:
//     cerr << WHERE_AM_I << " Warning SpaceConfigEnum = " << config << endl;
//     break;
//   }

//   if ( isnan( beta ) || isinf( beta )  || theta < 0  || theta > PI_ / 2.0 ){
//     cout << endl << boundary.id() << " marker = " << boundary.marker() << endl;
//     cout << boundary.node(0).pos() << boundary.node(1).pos()<<boundary.node(2).pos()<< endl;
//     cout << "Source = " << sourcePos 
// 	 << "\t boundary = " << facetPos 
// 	 << "\tNorm(Boundary)" << norm << endl;
//     cout << "\t Radius = " << radius << "\t RadiusMirror = " << radiusMirror 
// 	 << "\t Theta(rad) = " << theta << "\t ThetaMirror = " << thetaMirror 
// 	 << "\t Theta(grad) = " << theta * 360.0 / ( 2.0 * PI_ ) 
// 	 << "\t beta = " << beta << endl;

//     cerr << " Beta forced to 0.0 " << endl;
//     beta = 0.0;
//   }
//   return beta;

// DO_NOT_USE
//   return -1;
// }

double boundaryBetaDC2D( const RealPos & source1, const RealPos & source2, BaseElement & boundary, double rho ){
//   double radius = sourcePos.distance( boundary.averagePos() );
//   double theta = boundary.averagePos().angle( boundary.normVector(), sourcePos );
//   double beta = cos( theta ) / ( rho * radius  );

	 /* die Herleitung werde ich noch aufschreiben sobald alles klappt
	    das potential nach parasnis f�r eine Linienelektrode U=(I*rho)/PI *ln(r2/r1)
	    nur definiert f�r zwei elektroden; */
  
  double r1 = source1.distance( boundary.averagePos() );
  double r2 = source2.distance( boundary.averagePos() );
  
  double x = boundary.averagePos().x();
  double y = boundary.averagePos().y();
  
  double dudx = ( ( x - source1.x() ) * r2 * r2 - ( x - source2.x() ) * r1 * r1 ) / ( r1 * r1 * r2 * r2 );
  double dudy = ( ( y - source1.y() ) * r2 * r2 - ( y - source2.y() ) * r1 * r1 ) / ( r1 * r1 * r2 * r2 );
  
  double dx = ( boundary.node( 0 ).x() - boundary.node( 1 ).x() );
  double dy = ( boundary.node( 0 ).y() - boundary.node( 1 ).y() );
  
  double nx = dy / ( sqrt( dx * dx + dy * dy ) ); 
  double ny = -dx / ( sqrt( dx * dx + dy * dy ) ); 
  
  double dudn = dudx * nx + dudy * ny;
  
  double beta = 0.0;
  if ( fabs( dudn ) < 1E-20) beta = 0; else beta = - dudn / ( rho * log( r2 / r1 ) );

  return beta;
}

double boundaryBetaDC2D5( const RealPos & source, BaseElement & boundary,  SpaceConfigEnum config, double k ){
  double beta = 0.0;
  RealPos facetPos( boundary.averagePos() );
  RealPos norm( boundary.normVector() );

  RealPos r( source - facetPos );
  RealPos rMir( source - RealPos( 0.0, 2.0 * source.y() ) - facetPos );
  double rAbs = r.abs(), rMirAbs = rMir.abs();

  switch( config ){
  case HALFSPACE:
    if ( fabs( besselK0( rAbs * k ) ) < 1e-40 ) beta = 0.0;
    else
      beta = k * fabs( r.scalar( norm ) ) / rAbs * besselK1( rAbs * k ) / besselK0( rAbs * k );
    break;
  case FULLSPACE:
    if ( fabs( besselK0( rAbs * k ) ) < 1e-40 ) beta = 0.0;
    else 
      beta = k * fabs( r.scalar( norm ) ) / rAbs * besselK1( rAbs * k ) / besselK0( rAbs * k );
    break;
    case MIRRORSOURCE:
        if ( ( besselK0( rAbs * k ) + besselK0( rMirAbs * k ) ) < TOLERANCE ) beta = 0.0;
        else
            beta = k * ( fabs( r.scalar( norm ) ) / rAbs * besselK1( rAbs * k ) + 
 		     fabs( rMir.scalar( norm ) ) / rMirAbs  * besselK1( rMirAbs * k ) ) /
                    ( besselK0( rAbs * k ) + besselK0( rMirAbs * k ) );
    break;
    default:
        TO_IMPL
  }

//   RealPos facetPos( boundary.averagePos() );
//   RealPos norm( boundary.normVector() );

//   double radius = sourcePos.distance( facetPos ), radiusMirror = 0.0;
//   double theta = 0.0, thetaMirror = 0.0;
//   //** check ob der Normvektor ins innere des Gebietes oder nach aussen zeigt
//   if ( ( sourcePos - facetPos + norm ).abs() >  ( sourcePos - facetPos - norm ).abs() ){
//     theta = facetPos.angle( sourcePos, facetPos + norm );
//   } else {
//     theta = facetPos.angle( sourcePos, facetPos - norm );
//   }
//   double beta = 0.0;
//   beta = k * cos( theta ) * besselK1( radius * k ) / besselK0( radius * k );

  if ( isnan( beta ) || isinf( beta ) ){
    cout << endl << boundary.id() << " marker = " << boundary.marker() << endl;
    cout << boundary.node(0).pos() << boundary.node(1).pos() << endl;
    cout << "Source = " << source
         << "\t boundary = " << facetPos
         << "\tNorm(Boundary)" << norm << endl;
    cout << "\t Radius = " << r << " = " << rAbs 
         << "\t RadiusMirror = " << rMir << " = " << rMirAbs
         << "\t beta = " << beta << endl;
    cout << "\t K = " << k  << endl;
    cout << "\t BesselK1(r*K) = " << besselK1( rAbs * k )  << endl;
    cout << "\t BesselK0(r*K) = " << besselK0( rAbs * k )  << endl;
    cerr << " Beta forced to 0.0 " << endl;
    
    
    beta = 0.0;
//exit(1);
  }
  return beta;
}

vector <int> findSourcesFromFile( const BaseMesh & mesh, const string & fname ){
  vector <int> sources;

  fstream file; 
  if (openInFile( fname, & file ) ){ 
    
    int intdummy;
    string str;
    int sourceCount = 0;

    //** searching in .geo file
    file >> intdummy >> sourceCount >> str;
    double x, y, z;
    for ( int i = 0; i < sourceCount; i++ ){
      file >> intdummy >> intdummy >> x >> y >> z;
      
      for ( int j = 0, jmax = mesh.nodeCount(); j < jmax; j++ ){
	if ( (fabs( mesh.node(j).x() - x) < 1E-3) && (fabs( mesh.node(j).y() - y) < 1E-3) ){
	  if ( mesh.node(j).marker() == -99 ) sources.push_back(j);  // node for elektrode i
	}
      }
    }
    if ( sources.size() != (unsigned int)sourceCount ){
      cerr << WHERE << " source.size() " << sources.size() 
	   << " != sourceCount " <<  sourceCount << endl;
    }
    file.close();
  }
  return sources;
}

double solveAnalytical( const RealPos & query, const RealPos & source, double k, SpaceConfigEnum config ){
    RealPos sourceMir( source - RealPos( 0.0, 2.0 * source.y() ) );
  //  RealPos sourceMir( source );

//   cerr << WHERE_AM_I << " Source mirrored at y = 0 " << endl;
//   cerr << source << sourceMir << endl;

  double konst = 0.0, r = 0.0, rMir = 0.0, current = 1.0, rho = 1.0, solution = 0.0;
  konst = rho * current / ( 2.0 * PI_ );

  if ( k == 0.0 ){ 
    solution = standardisedPotential( source, query, config );
  } else {
    //      cout << sourcePos << mesh.node( i ).pos() << endl;
    r = query.distance( source );
    rMir = query.distance( sourceMir );
    
    if ( r != 0 ){
      solution = konst * ( besselK0( r * k ) + besselK0( rMir * k ) );
    } else {
      solution = 0;
    }
  }
  return solution;
}

int solveAnalytical( const BaseMesh & mesh, const RealPos & source, RVector & solution, double k, SpaceConfigEnum config ){
  int dof = mesh.nodeCount();
  if ( solution.size() != dof ) solution.resize( dof );
  for ( int i = 0; i < dof; i++ ){
    solution[ i ] = solveAnalytical( mesh.node( i ).pos(), source, k, config );
  }
  return 1;
}
RVector solveAnalytical( const vector < RealPos > & queryPos, const RealPos & sourcePos, double k, SpaceConfigEnum config ){
  RVector u( queryPos.size(), 0.0 );
  for ( uint i = 0; i < queryPos.size(); i ++ ) u[ i ] = solveAnalytical( queryPos[ i ], sourcePos, k,  config );
  return u;
}
RVector solveAnalytical( const BaseMesh & mesh, const RealPos & sourcePos, double k, SpaceConfigEnum config ){
  return solveAnalytical( mesh.nodePositions(), sourcePos, k, config ); 
}

RealPos solveAnalyticalGrad( const RealPos & query, const RealPos & source, double k ){
  RealPos solution( 0.0, 0.0, 0.0) ;

  RealPos sourceMir( source - RealPos( 0.0, 2.0 * source.y() ) );
  RealPos radius( source - query );
  RealPos radiusMir( sourceMir - query );
  
//   cerr << WHERE_AM_I << " Source mirrored at y = 0 " << endl;
//   cerr << source << sourceMir << endl;

  double konst = 0.0, current = 1.0, rho = 1.0;
  konst = rho * current / ( 2.0 * PI_ );

  double r = radius.abs(),  rMir = radiusMir.abs();
  
  if ( r != 0 ){
    if ( k == 0.0 ){ 
      //** du/dx = -1/2pi * x/( x^2 + y^2 +z^2 )^(3/2);
//       solution[ 0 ] = - radius[ 0 ] * konst / 
//   radius / ( - konst * pow( r, 3.0 ) );
      solution =  ( radius / pow( r, 3.0 ) + radiusMir / pow( rMir, 3.0 ) ) * -konst/2.0;
//       cout << source << query << endl;
//       cout << radius << r << " " << pow( r, 3.0 ) << endl;
//       cout << solution << endl;
    } else {
      //      solution = 2.0* ( radius * k * konst * ( BesselK1( radius.abs() * k ) ) / radius.abs() );
      solution = ( radius * ( BesselK1( r * k ) ) / r  + radiusMir * ( BesselK1( rMir * k ) ) / rMir ) * konst * k;
    }
  } else {
    solution = RealPos( 0.0, 0.0, 0.0 );
  }
  return solution;
}

vector < RealPos > solveAnalyticalGrad( const vector < RealPos > & query, const RealPos & source, double k ){
  vector < RealPos > uGrad( query.size(), RealPos( 0.0, 0.0, 0.0 ) );
  for ( uint i = 0; i < query.size(); i ++ ) uGrad[ i ] = solveAnalyticalGrad( query[ i ], source, k );
  return uGrad;
}
vector < RealPos > solveAnalyticalGrad( const BaseMesh & mesh, const RealPos & source, double k ){
  return solveAnalyticalGrad( mesh.nodePositions(), source, k ); 
}


BaseFEM::BaseFEM( BaseMesh & mesh ) 
  : mesh_( &mesh ){
  rhs_ = new RVector( mesh.nodeCount() );
  _solver = PCG;
  _solver_verbose = false;
  _solver_epsilon = 1E-6;
  dropTol_ = 1e-4;
  rhs_given = false;
  S_ = new RDirectMatrix( mesh.nodeCount() );
  verbose_ = false;
  //  rtSolverInfo_ = new RTISolver();
  matrixUpdate_ = true;
}

BaseFEM::~BaseFEM(){
  delete rhs_;
  delete S_; 
}

int BaseFEM::applyHomDirichletBC( RDirectMatrix & S, const set < long > & nodesSet ){
  return applyHomogDirichletBC( S, *rhs_, nodesSet, verbose_ );
}

int BaseFEM::applyDirichletBC( RDirectMatrix & S, const map < long, double > & nodesMap ){
  return applyInHomogDirichletBC( S, *rhs_, nodesMap, verbose_ );
}


RVector BaseFEM::calculate(){
  int succses = 1;
  RVector solution( mesh_->nodeCount() );

  //  RDirectMatrix S( mesh_->nodeCount() ); 
  if ( rhs_->size() != solution.size() ) rhs_->resize( solution.size() );
  if ( !rhs_given ) succses *= generateRHSVector();
  //  rhs_->save( "righthandside.vector" );  

  if ( matrixUpdate_ ) {
    if (_solver != ANALYT) {
      //cout << "start assemble matrix." << endl;
      succses *= compileMatrix( *S_ );
      //      cout << "finish assemble matrix" << endl;
      //S_->save( "stiffness.matrix" );
      //** check holes in Matrix from TetGen-bug, and force to 1.0;
      for ( int i = 0, imax = S_->size(); i < imax; i++ ) {
	if ( S_->GetVal( i + 1, i + 1 ) == 0.0 ) {
	  cerr << WHERE_AM_I << "Error! S-diag-element = 0.0 at Value forced to 1.0: " << i << endl; 
	  S_->SetVal( i + 1, i + 1, 1.0);
	}
      }
    }
  } else { 
    cerr << "Warning Matrix unchanged." << endl;
  }

//    S_->save("S.mat");
//    rhs_->save( "rhs.vec" );

  if ( succses == 1 ){
    switch( _solver ){
//     case CG: 
//       solveCG( *S_, solution, *rhs_, _solver_epsilon, solution.size() * 10, _solver_verbose ); 
//       break;
//     case PCG: 
//       solvePCG( *S_, solution, *rhs_, _solver_epsilon, solution.size() * 10, _solver_verbose ); 
//       break;
//     case PETSCJCG: 
//       solveWithPetscJCG( *S_, solution, *rhs_, _solver_epsilon, solution.size() * 10, _solver_verbose ); 
//       break;
//     case PETSCSSORCG: 
//       solveWithPetscSSORCG( *S_, solution, *rhs_, _solver_epsilon, solution.size() * 10, _solver_verbose ); 
//       break;
//     case PETSCICCG: 
//       solveWithPetscICCG( *S_, solution, *rhs_, _solver_epsilon, solution.size() * 10, _solver_verbose ); 
//       break;
//     case TAUCSDIRECT: 
//       solveWithTaucsDirect( *S_, solution, *rhs_, _solver_epsilon, solution.size() * 10, false ); 
//       break;    
//     case ANALYT: 
//       solveAnalytical( solution ); 
//       break;
//     case TAUCSTEST: 
//       solveWithTaucsTest( *S_, solution, *rhs_, _solver_epsilon, solution.size() * 10, true ); 
//       break;
    default : 
      cerr << WHERE << " Solver " << _solver << " ungueltig" << endl; 
      return 0; 
      break;
    }
  } else cerr << WHERE_AM_I << " something goes wrong :( " << endl;

  return solution; 
}


RVector DCGeoElectricFEM::calculate2D5( const string & dataFileBody, int elektrode, bool verbose ){ 
DO_NOT_USE
//   RVector tmp( mesh_->nodeCount(), 0.0 );
//   vector <double > kValues, weights;
//   initKWaveList( *mesh_, kValues, weights, *new vector < RealPos >, verbose );

//   vector < RVector > solutionMatrix;

//   for ( size_t i = 0, imax = kValues.size(); i < imax; i++){
//     if ( verbose ) cout << "Calculating: " << i + 1 <<"/"<< imax << "\tk = "<< kValues[ i ]<< endl;
//     setK( kValues[ i ] );
//     if ( _solver == ANALYT ){
//       solveAnalytical( tmp );
//       solutionMatrix.push_back( tmp );
//     } else {
//       solutionMatrix.push_back( calculate() );
//     }
//     if ( dataFileBody != NOT_DEFINED ){
//       sprintf( strdummy, "%s.P.%d_%d.pot", dataFileBody.c_str(), elektrode, i );
//       solutionMatrix.back().save( strdummy );
//     }
//   }
  
 RVector solution(0);
//   for ( size_t i = 0, imax = kValues.size(); i < imax; i++){
//     solution += weights[ i ] * solutionMatrix[ i ];
//   }
    return solution;
}


double BaseFEM::energyNorm( RVector & e ){
  TO_IMPL
    return -1;
}

int BaseFEM::compileMatrix( RDirectMatrix & S ){;
  compileBasisMatrix( S );
  compileBasisMatrixBoundary( S );
  return 1;
}


int DCGeoElectricFEM::compileBasisMatrix( RDirectMatrix & S ){
  S.clean();
  ElementMatrix S_i, Stmp;

  int dim = mesh_->dim();
  double rho = 0.0;
  for ( int i = 0, imax = mesh_->cellCount(); i < imax; i++ ){
    rho = mesh_->cell( i ).attribute();

    //cout << "Element " << i << "\tAttribute: " << rho << endl;
    if ( fabs( rho ) > 1E-40) {
      switch ( dim ){
      case 2:
	Stmp = S_i.u2( mesh_->cell( i ) );
	Stmp *= k_ * k_;
	Stmp += S_i.ux2uy2uz2( mesh_->cell( i ) );
	break;
      case 3:
	Stmp = S_i.ux2uy2uz2( mesh_->cell( i ) );
	break;
      }
      Stmp *= 1./rho;
      S += Stmp;
      //      Stmp.show();
    } //else cerr << WHERE_AM_I << "\aWARNING !!!!    Resistivity < 1E-40 " << endl;
  }
  return 1;
}

int DCGeoElectricFEM::compileBasisMatrixBoundary( RDirectMatrix & S, const RealPos & sourcePos,
						  double rhoBoundary ){
  ElementMatrix S_i, Stmp;
  
  double beta = 0.0;
  int marker = 0, nodeID = 0;
  bool neumannOnly = true;
  dirichletMap_.clear();
  set < long > setHomDirichlet;

  RealPos averagePosSource( 0.0, 0.0, 0.0 );
  if ( !sourcePos.valid() ){
    for ( unsigned int i = 0, imax = _sources.size(); i < imax; i++ ) {
      averagePosSource += mesh_->node( _sources[ i ] ).pos();
    }
    averagePosSource /= _sources.size();
  } else averagePosSource = sourcePos;

//   cout << WHERE_AM_I << averagePosSource << endl;
//   cout << WHERE_AM_I << " spaceConfig_ " << spaceConfig_<< endl;

  for ( int i = 0, imax = mesh_->boundaryCount(); i < imax; i++ ){
    marker = mesh_->boundary( i ).marker();
    //    cout << "Face: " << i << " = " << marker << endl;
    if ( marker < 0 ){
      switch ( marker ){
      case -1: // homogen Neumann
	//	cout << "Face:" << i << " = -1" << endl;
	break;
      case -2: //** Couchy boundary condition (Mixed bc)
	neumannOnly = false;
	switch ( mesh_->dim() ){
	case 2: 
	  beta = boundaryBetaDC2D5( averagePosSource, mesh_->boundary( i ), spaceConfig_, k_ );
	  break;
	case 3:
	  //	  beta = boundaryAlphaDC3D( averagePosSource, mesh_->boundary( i ), MIRRORSOURCE_ );
	  beta = boundaryAlphaDC3D( averagePosSource, mesh_->boundary( i ), spaceConfig_ );
	  break;
	default:
	  cerr << WHERE_AM_I << " dimension wrong " << mesh_->dim() << endl;
	} 

	if ( rhoBoundary == 0.0 ){
	  rhoBoundary = mesh_->boundary( i ).findBoundaryCell()->attribute();
	  if ( rhoBoundary == 0.0 ){
	    cerr << WHERE_AM_I << "rho = "  << rhoBoundary  << endl;
	  }
	}

	beta /= rhoBoundary;

	Stmp = S_i.u2( mesh_->boundary( i ) );

	Stmp *= beta;
	//	cout << beta << endl;
	//	Stmp.show();
	S += Stmp;
	break;
      case -3: // homogen Dirichlet
	neumannOnly = false;
	for ( int j = 0; j < mesh_->boundary( i ).nodeCount(); j ++ ) {
	  nodeID = mesh_->boundary( i ).node( j ).id();
	  setHomDirichlet.insert( nodeID );
	}
	break; 
      case -4: // Dirichlet  ana3d dc mit Spiegelquelle analytiche RB
	neumannOnly = false;
	for ( int j = 0; j < mesh_->boundary( i ).nodeCount(); j ++ ){
	  nodeID = mesh_->boundary( i ).node( j ).id();
	  dirichletMap_[ nodeID ] = 0.0;
	}
	break; 
      default: cerr << WHERE_AM_I << " Marker " << marker << " not defined." << endl;
      }
    }
  }

  //** apply homogeneous neumann matrix constraints
  if ( neumannOnly ){
    
    //**   Fix one node of the boundary to zero
    vector < int > calibrationNode( mesh_->getAllMarkedNodes( CALIBRATION_MARKER ) );
    if ( calibrationNode.size() > 0 ){
      cout << "Fix homogeneous neumann domain (one boundary node to zero)" << endl;

      for ( uint i = 0 ; i < calibrationNode.size(); i ++ ) setHomDirichlet.insert( calibrationNode[ i ] );
    } else { 
      //      cout << "Fix homogeneous neumann domain (mean value over the boundary to zero)" << endl;
      //** Fix the mean value over the boundary to zero

      for ( uint i = 0, imax = mesh_->boundaryCount(); i < imax; i++ ){
	if ( marker < 0 ){
	  //** machen 
	  

	}
      }

    }
  }

  //** apply homogeneous dirichlet boundary conditions
  if ( setHomDirichlet.size() > 0 ) { 
    applyHomDirichletBC( S, setHomDirichlet );
  }

  //** apply dirichlet boundary conditions
  if ( dirichletMap_.size() > 0 ) {
    RVector analytisch( mesh_->nodeCount() );
    solveAnalytical( *mesh_, averagePosSource, analytisch, 0.0 );

    for ( map < long, double >::iterator it = dirichletMap_.begin(); it != dirichletMap_.end(); it++){
      dirichletMap_[ (*it).first ] = analytisch[ (*it).first ];
    }
    applyDirichletBC( S, dirichletMap_ );
  }

  return 1;
}

//int DCGeoElectricFEM::solveAnalytical( RVector & solution, int source_, double rho, bool verbose ){
// DO_NOT_USE 
//   if ( fabs( rho ) < 1E-40 ) { cerr << WHERE_AM_I << " Warning! Rho_0 = " << rho << endl; }  

//   //cerr << WHERE_AM_I<< " Ganz wichtig testen !!!!! " << endl;
//   RealPos source = mesh_->node( source_ ).pos();

//   RealPos sourcemirror( source.x(), source.y(), -1.0 * source.z() );
//   if ( verbose ) cerr << WHERE_AM_I << " Source mirrored at z = 0 " << endl;
  
//   if ( verbose ) {
//     cout << "Source: " << mesh_->node( source_ ).pos().x() <<  "\t" 
// 	 << mesh_->node( source_ ).pos().y() <<  "\t" 
// 	 << mesh_->node( source_ ).pos().z() << endl;
//     cout << "Sourcemirror: " <<  sourcemirror.x() <<  "\t" 
// 	 << sourcemirror.y() <<  "\t" 
// 	 << sourcemirror.z() << endl;
//   }
  
//   double konst = rho * current_ / ( 4 * PI_ );
//   double radius = 0.0, minradius = 1E100, radiusmirror = 0.0;

//   for ( int i = 0; i < solution.size(); i++ ){
//     radius = mesh_->node(i).pos().distance( source );
//     radiusmirror = mesh_->node(i).pos().distance( sourcemirror );
//     //    cout << WHERE_AM_I << "\t" << radius <<endl;
//     if ( radius > 1E-20 ){
//       if ( radius < minradius ) minradius = radius;
//       solution[ i ] = konst * ( 1 / radius + 1 / radiusmirror );
//       //cout << solution[ i ] << endl;
//     } else solution[ source_ ] =  0.0; 
//   }
//   //  cout << minradius << endl;
//   solution[ source_ ] =  konst * ( 1 / ( minradius / 5. ) );
  
//   return 0; 
// }

// int DCGeoElectricFEM::solveAnalytical( RVector & solution ){
//   DO_NOT_USE
//   RVector sol1( solution ), sol2( solution );
//   switch ( _sources.size() ){
//   case 0: 
//     cerr << WHERE << " sources.size() == 0 not defined." << endl; 
//     break;
//   case 1: 
//     switch ( mesh_->dim() ){
//     case 2:
//       solveAnalytical2D5( mesh_->node( _sources[ 0 ] ).pos(), solution ); break;
//     case 3:
//       solution = standardisedPotential( mesh_->node( _sources[ 0 ] ).pos(), *mesh_, spaceConfig_ ); break;
//     } break;
//   case 2:
//     switch ( mesh_->dim() ){
//     case 2:
//       cerr << WHERE_AM_I << " not in use, atm. " << endl;
//      //      solveAnalytical2D( solution, rhoBG_ );
//       break;
//     case 3:
//       solution = standardisedPotential( mesh_->node( _sources[ 0 ] ).pos(), *mesh_, spaceConfig_ );
//       solution -= standardisedPotential( mesh_->node( _sources[ 1 ] ).pos(), *mesh_, spaceConfig_ );
//       //       solveAnalytical( sol1, _sources[ 0 ], rhoBG_ );
//       //       solveAnalytical( sol2, _sources[ 1 ], rhoBG_ );
//       //       solution = sol1 - sol2;
//       break;
//     } break;
//   default:
//     cerr << WHERE << " sources.size() > 2, only first pair where used." << endl; 
//     solution = standardisedPotential( mesh_->node( _sources[ 0 ] ).pos(), *mesh_, spaceConfig_ );
//     solution -= standardisedPotential( mesh_->node( _sources[ 1 ] ).pos(), *mesh_, spaceConfig_ );
//     break;
//   }
  
//   if ( rhoBG_ != -1 ) solution *= rhoBG_;
//   return 1;
// }

// int DCGeoElectricFEM::solveAnalytical2D( RVector & solution, double rho ){;
//  DO_NOT_USE 
//  if ( _sources[0] == _sources[1] ) { 
//    cerr << WHERE << " There are less than 2 sources, or sources undefined." << endl;  return 0;
//  }
//  if ( fabs( rho ) < 1e-40 ) { cerr << WHERE_AM_I << " Warning! Rhos == 0 !!!! " << endl; } 
 
//  RealPos source1 = mesh_->node( _sources[ 0 ] ).pos();
//  RealPos source2 = mesh_->node( _sources[ 1 ] ).pos();
//  RealPos nodePos;
 
//  //  dbg << "Source 1 at Node " << mesh_->node( _sources[ 0 ] ).id() 
//  //      << "( " << mesh_->node( _sources[ 0 ] ).x()
//  //      << ", " << mesh_->node( _sources[ 0 ] ).z() << ")" << endl;
//  //  dbg << "Source 2 at Node " << mesh_->node( _sources[ 1 ] ).id() 
//  //      << "( " << mesh_->node( _sources[ 1 ] ).x()
//  //      << ", " << mesh_->node( _sources[ 1 ] ).z() << ")" << endl;
//  double r1 = 0, r2 = 0;
//  double _current = 1.0;
 
//  for (int i = 0, imax = mesh_->nodeCount(); i < imax; i++){
//    nodePos = mesh_->node(i).pos();
//    if (nodePos != source1 && nodePos != source2){
//      r1 = nodePos.distance( source1 );
//      r2 = nodePos.distance( source2 );
//      solution[i] = ( (_current * rho ) / PI_ ) * log( r2 / r1 );
//    } else solution[i] = 0.0; // singularity -- the potential forced to _current
//  }
// return 1;
// }

// int DCGeoElectricFEM::solveAnalytical2D5( const RealPos & sourcePos, RVector & solution ){
//   DO_NOT_USE;  
//   return 1;
// }

int DCGeoElectricFEM::generateRHSVector(){
  rhs_->clear(); 
  switch ( _sources.size() ){
  case 0: 
    cerr << WHERE << " sources.size() == 0 not defined." << endl; 
    break;
  case 1: 
    insertCurrent( _sources[0],  current_ );
    break;
  case 2:
    insertCurrent( _sources[0],  current_ );
    insertCurrent( _sources[1], -1.0 * current_ );
    break;
  default:
    cerr << WHERE << " sources.size() > 2, only first pair where used." << endl; 
    insertCurrent( _sources[0],  current_ );
    insertCurrent( _sources[1], -1.0 * current_ );
    break;
  } 
  return 1;
}

RVector DCGeoElectricSRFEM::calculate(){
 DO_NOT_USE 
//   map < long, int > mapTmpBC;
//   int marker = 0;
//   for ( int i = 0, imax = mesh_->boundaryCount(); i < imax; i ++ ) { //** for all faces with BC do
//     marker =mesh_->boundary( i ).marker();
//     if ( marker < -1 ){
//       //  cout << marker << endl;
//       mapTmpBC[ i ] = marker;                 //** if BC not hom. neumann, save BC in temp Map;
//       mesh_->boundary( i ).setMarker( -3 );        //** set all BC to hom. Dirichlet;
//     }
//   }

//   RVector potPrimaer( mesh_->nodeCount() ); solveAnalytical( potPrimaer );
//   generateRHSVector( potPrimaer );
//   rhs_given = true;
//   RVector potSekundaer = BaseFEM::calculate();

//   for ( map< long, int >::iterator it = mapTmpBC.begin(); it != mapTmpBC.end(); it++){
//     mesh_->boundary( (*it).first ).setMarker( (*it).second );
//   }

//   potSekundaer.save("CALsekundaer.pot");
//   potPrimaer.save("CALprimae.pot");
//  cout << WHERE_AM_I << "\t" <<(potPrimaer - potSekundaer).normL2() << endl;
//  return potSekundaer;;
  return RVector();//potPrimaer + potSekundaer;
}

int DCGeoElectricSRFEM::setPrimaerPotential( const RVector & potPrim ){;
 DO_NOT_USE 
  return generateRHSVector( potPrim );
}

int DCGeoElectricSRFEM::generateRHSVector( const RVector & potPrim ){;
 DO_NOT_USE 
   return 0;
}

int MultiElectrodeModelling::calculate( vector < RVector > & solution ){ 
  solution.clear();
  if ( haveSubSolution_ ) subSolution_->clear();
  Stopwatch swatch; swatch.start();

  vector < double > kValues, weights;
  
  switch ( fem_->mesh().dim() ){
  case 2:
    initKWaveList( fem_->mesh(), kValues, weights, vecSourcePos_ );
    break;
  case 3:
    kValues.push_back( 0.0 );
    weights.push_back( 1.0 );
    break;
  }
  kValues_ = kValues;

  vector < RVector > solutionK;

  for ( size_t k = 0; k < kValues.size(); k ++ ){
  
    if ( verbose_ && kValues.size() > 1 ) cout << "\r" << k + 1 << " / " << kValues.size(); 
    //    if ( verbose_ ) cout << k + 1 << " / " << kValues.size() << endl; 
    
    if ( fromElectrode_ == -1 ) fromElectrode_ = 0;
    if ( toElectrode_ == -1 ) toElectrode_ = vecSourcePos_.size();

    calculateK( solutionK, kValues[ k ], k );

    for ( size_t i = 0, imax = solutionK.size(); i < imax; i++ ){
      if ( haveSubSolution_ ) {
	subSolution_->push_back( solutionK[ i ] );
      }
      if ( hold_ || fem_->mesh().dim() == 2 ){
	if ( k == 0 ){ 
	  if ( i == 0 ) solution.resize( solutionK.size(), solutionK[ i ] );
	  solution[ i ] = weights[ k ] * solutionK[ i ];
	}
	else solution[ i ] += weights[ k ] * solutionK[ i ];
      }
    }
  }

  if ( dataFileBody_ != NOT_DEFINED && kValues.size() > 1 ){
    for ( size_t i = 0, imax = solution.size(); i < imax; i++ ){
      if ( !hold_ ) solution[ i ].save( dataFileBody_ + "." + toStr( i ) + ".pot" );
    }
  }
  if ( verbose_ ) cout << "\rForward calculation takes " << swatch.duration() << " seconds." << endl; 

  return 1;
}

int MultiElectrodeModelling::calculateK( vector < RVector > & solution, double kVal, int k ){ 
  solution.clear();
  Stopwatch swatch;
  BaseMesh * mesh = & fem_->mesh();
  int dof = mesh->nodeCount();
  RDirectMatrix S( dof );

  RVector uDirichlet;
  map < long, double > dirichletMap;

  if ( !analytical_ ){
    //    cout << endl << kVal << endl;
    fem_->setK( kVal );
    fem_->compileBasisMatrix( S );
    fem_->compileBasisMatrixBoundary( S, findAverageSource() );
//    S.save("Sdcfem.matrix");
    dirichletMap = fem_->dirichletMap();

    if ( dirichletMap.size() > 0 ){
      uDirichlet.resize( dof );
      RVector u_0( dof );
      
      for ( map < long, double >::iterator it = dirichletMap.begin(); it != dirichletMap.end(); it++){
	u_0[ (*it).first ] = (*it).second;
      }
      RDirectMatrix Sin( dof );
      fem_->compileBasisMatrix( Sin );
      uDirichlet = Sin * u_0;
    }
  }

  SolverWrapper * solver = NULL;
  swatch.start();
  if ( !analytical_ ){
    if ( verbose_ && k == 0 ){
      //cout << "start factorize ..." << endl;
    }
    switch( solverType_ ){
    case PCG:               solver = new PCGWrapper( S, epsilon_, true ); break;
    case TAUCSWRAPPER:      solver = new TaucsWrapper( S, "metis", dropTol_, epsilon_, verbose_ ); break;
    case LDLWRAPPER:        
        //std::cout << "LDLWRAPPER" << std::endl;
        solver = new LDLWrapper( S ); break;
    case CHOLMODWRAPPER:   
        //std::cout << "CHOLMODWRAPPER" << std::endl;
        solver = new CHOLMODWrapper( S ); break;
    default: cerr << WHERE_AM_I << " solverType undefined " << solverType_ << endl;
      solver = new TaucsWrapper();
    }
    if ( verbose_ && kVal == 0){
      cout << "... finish factorize. ( " << swatch.duration() << "s )" << endl;
    }
  }
  swatch.stop();

  RVector rhs( dof ), sol( dof );

  int eA = 0, eB = 0, nElecs = vecSourcePos_.size();
  if ( nElecs == 0 ) {
    cerr << WHERE_AM_I << " No sources positions given, nothing to do." << endl; exit( 1 );
  }

  swatch.start();

  RVector reNewPotVec; 
  bool  reNewPot = false;
  if ( reNewPot ){
    reNewPotVec.load("renewpot.vec", Ascii, 1 );
    cout << "Found renewpot.vec, starting renew some potentials. " << endl;
    fromElectrode_ = 0; 
    toElectrode_ = reNewPotVec.size();	
    hold_ = false;
  }
  
  for ( int i = fromElectrode_; i < toElectrode_; i ++ ){
    if ( reNewPot ) eA = (int)reNewPotVec[ i ]; else eA = i;
   
    if ( verbose_ && kVal == 0 ) cout << "\r[" << eA+1 << "/" << nElecs  << "]";

    if ( dipole_ && eA == nElecs - 1  && !circle_  ) break;
    
    rhs.clear();

    if ( dipole_ || circle_ ) {
      eB = eA + 1;
      if ( eB > (int)nElecs - 1 ) eB -= nElecs;
    }

    if ( analytical_ ){
      solveAnalytical( *mesh, vecSourcePos_[ eA ], sol, kVal, fem_->spaceConfig() );
      if ( dipole_ ){
	RVector solTmp( sol );
	solveAnalytical( *mesh, vecSourcePos_[ eB ], sol, kVal, fem_->spaceConfig() );
	sol = solTmp - sol;
      }
    } else {
      if ( electrodeNodeIndex_.size() == 0 ){
	cerr << WHERE_AM_I << " no informations about electrode nodes" << endl;
	exit(0);
      }
      rhs[ electrodeNodeIndex_[ eA ] ] = 1.0;

      if ( referenceElectrode_ ) {
	rhs[ referenceNodeIdx_ ] = -1.0;
      } else if ( dipole_ ) rhs[ electrodeNodeIndex_[ eB ] ] = -1.0;

      if ( dirichletMap.size() > 0 ){
	rhs -= uDirichlet;
	for ( map < long, double >::iterator it = dirichletMap.begin(); it != dirichletMap.end(); it++){
	  ( rhs )[ (*it).first ] = (*it).second;
	}
      }

//       S.save("S.mat");
//       rhs.save("b.vec");
//       exit(0);

      solver->solve( rhs, sol );
      //      cout << "rms: " <<  rms( S*sol-rhs ) << endl;
      
      if ( verbose_ && kVal == 0){
 	swatch.stop(); cout << "\t\tswatch = " << swatch.duration() << " s" << endl; swatch.start();
      }
    } //** else if !analytical;

    if ( dataFileBody_ != NOT_DEFINED ){
      if ( kVal == 0.0 ){
	if ( !hold_ ) sol.save( dataFileBody_+ "." + toStr( eA ) + ".pot");
      } else { 
	if ( !hold_ ) sol.save( dataFileBody_+ "." + toStr( eA ) + "_" + toStr( k ) + ".pot");
      }
    }

    if ( hold_ || fem_->mesh().dim() == 2 ){
      if ( eA == 0 ){
	if ( dipole_ && !referenceElectrode_ ){
	  solution.resize( nElecs-1, sol );
	} else {
	  solution.resize( nElecs, sol );
	}
      }
      solution[ eA ] = sol;
    }
  }
  
  if ( verbose_ && kVal == 0 ) cout << endl;
  if ( solver ) delete solver;

  if ( reNewPot ) exit( 0 );
  return 1;
}

int MultiElectrodeModellingSingularityRemoval::calculateK( vector < RVector > & solution, double kVal, int k ){ 
  Stopwatch swatch;
  //  cout << WHERE_AM_I << kVal << " " << k << endl;
  if ( analytical_ ){
    cerr << WHERE_AM_I << "Analytical solution not defined" << endl;
    return 0;
  } 

  solution.clear();
  vector < double > individualRho( findIndividualRhoBackground() );
  BaseMesh * mesh = &fem_->mesh();

  int dof = mesh->nodeCount();
  RDirectMatrix S( dof ), S1( dof );

  fem_->setK( kVal );
  fem_->compileBasisMatrix( S );
  fem_->compileBasisMatrixBoundary( S, findAverageSource() );

  vector < double > tmpAttr( mesh->cellCount() );
  for ( size_t i = 0; i < tmpAttr.size(); i ++ ) {
    tmpAttr[ i ] = mesh->cell( i ).attribute();
    mesh->cell( i ).setAttribute( 1.0 );
  }
  fem_->compileBasisMatrix( S1 );
  fem_->compileBasisMatrixBoundary( S1, findAverageSource() );
  for ( size_t i = 0; i < tmpAttr.size(); i ++ ){
    mesh->cell( i ).setAttribute( tmpAttr[ i ] );
  }

  vector < int > calibrationNode( mesh->getAllMarkedNodes( CALIBRATION_MARKER ) );

  ///   S1.save( "S1.mat" );
//   S.save( "S.mat" );
  //  exit(0);

    SolverWrapper * solver = NULL;
  //  solverType_ = TAUCSWRAPPER;
    switch( solverType_ ){
    case PCG:               solver = new PCGWrapper( S, epsilon_, true ); break;
    case TAUCSWRAPPER:      solver = new TaucsWrapper( S, "metis", dropTol_, epsilon_, verbose_ ); break;
    case LDLWRAPPER:        solver = new LDLWrapper( S ); break;
    case CHOLMODWRAPPER:    solver = new CHOLMODWrapper( S ); break;
    default: cerr << WHERE_AM_I << " solverType undefined " << solverType_ << endl;
    }

  RVector rhs( dof ), sol( dof ), prim( dof );

  double rhoSource = 0.0;
  int nElecs = vecSourcePos_.size() ;

  swatch.start();
  int eA = 0, eB = 0;

  for ( int i = fromElectrode_; i < toElectrode_; i ++ ){
    if ( verbose_ && kVal == 0 ) cout << "\r[" << i+1 << "/" << nElecs  << "]";
    if ( i == nElecs -1 && dipole_ && !circle_  ) {
      delete solver;
      break;
    }

    rhs.clear();
    eA = i;
    if ( dipole_ || circle_ ) {
      eB = i + 1;
      if ( eB > (int)nElecs - 1 ) eB -= nElecs;
      
//       if ( referenceElectrode_ ){ // ** if ( circle geometry with a reference electrode )
// 	eB = referenceNodeIdx_;
//  	if ( eA == eB ) {
// 	  cerr << WHERE_AM_I << " reference node == electrode " << endl; 
// 	  delete solver; 
// 	  return 1;
// 	}
//       }
    }
    
    if ( dipole_ ) {
      rhoSource = ( individualRho[ eA ] + individualRho[ eB ] ) / 2.0;
    } else rhoSource = individualRho[ eA ];

    if ( primaryPotFileBody_.find( NOT_DEFINED ) != string::npos ){
      if ( havePrimPot_ ){
	prim = (*primPot_)[ k * nElecs + i ];
      } else {
	if ( !circle_ ){
	  solveAnalytical( *mesh, vecSourcePos_[ eA ], prim, kVal, fem_->spaceConfig() );
	  if ( dipole_){
	    RVector primTmp( prim );
	    
	    if ( referenceElectrode_ ) {
	      solveAnalytical( *mesh, mesh->node( referenceNodeIdx_ ).pos(), prim, kVal, fem_->spaceConfig() );
	    } else solveAnalytical( *mesh, vecSourcePos_[ eB ], prim, kVal, fem_->spaceConfig() );
	    
	    prim = primTmp - prim;
	    //	prim.save("analyt.vec");
	  }
	} else { // if circle && analytical
	  cerr <<WHERE_AM_I<< " cannot determine primary potentials." << endl; exit(0);
	}
      }
    } else {
      if ( kVal == 0.0 ){
	prim.load( primaryPotFileBody_ + "." + toStr( i ) + ".pot", verbose_);
      } else { 
	prim.load( primaryPotFileBody_ + "." + toStr( i ) + "_" + toStr( k ) + ".pot", verbose_);
      }
    }

    prim *= rhoSource; 
    rhs = ( S1 * prim / rhoSource - S * prim );

    if ( calibrationNode.size() == 1 ) rhs[ calibrationNode[ 0 ] ] = 0.0;


//    sol.save("sec.vec");
//    rhs.save("rhsDcfemlib.vec", Ascii);
    
//    prim.save("prim.vec");

    solver->solve( rhs, sol );
    sol += prim;
//     sol.save("sol.vec", Ascii);

//    exit(0);



    if ( collectOnly_ && kVal == 0.0 ) {
      dataMap_->importSolution( i, sol );
    } else {
      if ( dataFileBody_ != NOT_DEFINED ){
	if ( kVal == 0.0 ){
	  if ( !hold_ ) sol.save( dataFileBody_+ "." + toStr( i ) + ".pot");
	} else { 
	  if ( !hold_ ) sol.save( dataFileBody_+ "." + toStr( i ) + "_" + toStr( k ) + ".pot");
	}
      }

      if ( hold_ || fem_->mesh().dim() == 2 ){
	if ( i == 0 ){
	  if ( dipole_ && !referenceElectrode_ ){
	    solution.resize( nElecs-1, sol );
	  } else {
	    solution.resize( nElecs, sol );
	  }
	}
	solution[ i ] = sol;
      }
    }
    if ( verbose_ && kVal == 0){
      swatch.stop(); cout << "\t\tswatch = " << swatch.duration() << " s"; swatch.start();
    }
  }
  if ( verbose_ && kVal == 0) cout << endl;
  if ( solver ) delete solver;
  return 1;
}

void MultiElectrodeModellingSingularityRemoval::subResistivity_( double rho ){
  for ( int i = 0, imax = fem_->mesh().cellCount(); i < imax; i ++ ) {
    if ( fabs( rho - fem_->mesh().cell( i ).attribute() ) < 1e-12 ){
      fem_->mesh().cell( i ).setAttribute( 0.0 );
    } else {
      fem_->mesh().cell( i ).setAttribute( 1.0 / ( 1.0 / fem_->mesh().cell( i ).attribute() - 1.0 / rho ) );
    }
  }
}
void MultiElectrodeModellingSingularityRemoval::addResistivity_( double rho ){
  for ( int i = 0, imax = fem_->mesh().cellCount(); i < imax; i ++ ) {
    if ( fem_->mesh().cell( i ).attribute() == 0.0 ){
      fem_->mesh().cell( i ).setAttribute( rho );
    } else {
      fem_->mesh().cell( i ).setAttribute( 1.0 / ( 1.0 / fem_->mesh().cell( i ).attribute() + 1.0 / rho ) );
    }
  }
}


double MultiElectrodeModellingSingularityRemoval::findRhoBackground(){
  return geometricMean( findIndividualRhoBackground() );
}

vector< double > MultiElectrodeModellingSingularityRemoval::findIndividualRhoBackground(){
  vector < double > individualRhos, tmpRhos;
  SetpCells cellsPerNode;

  if ( electrodeNodeIndex_.size() > 0 ){
    for ( int i = 0, imax = electrodeNodeIndex_.size(); i < imax; i ++ ){
      cellsPerNode = fem_->mesh().node( electrodeNodeIndex_[ i ] ).cellSet();
      tmpRhos.clear();
      for ( SetpCells::iterator it = cellsPerNode.begin(); it != cellsPerNode.end(); it++){
	//cout << (*it)->attribute() << "\t";
	tmpRhos.push_back( (*it)->attribute() );
      }
      //    cout << i << "\t" << geometricMean( tmpRhos ) << endl;
      individualRhos.push_back( geometricMean( tmpRhos ) );
      if (individualRhos.back() < TOLERANCE ){
	cerr << WHERE_AM_I << ": Not a valid rhoBackground found " << individualRhos.back() << endl;
      }
    }
  } else {
    for ( int i = 0, imax = vecSourcePos_.size(); i < imax; i ++ ){
      individualRhos.push_back( fem_->mesh().findNearestCell( vecSourcePos_[ i ] ).attribute() );
    }
  }
  //  save( individualRhos, "individ.rhos");
  return individualRhos;
}


/*
$Log: fem.cpp,v $
Revision 1.70  2008/12/11 15:14:13  carsten
*** empty log message ***

Revision 1.69  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.68  2008/05/19 14:52:28  carsten
Add cholmodwrapper, interpolate V.2 in createSurface

Revision 1.67  2007/12/13 18:42:16  carsten
*** empty log message ***

Revision 1.66  2007/11/20 19:00:22  thomas
removed bug with mirror sources

Revision 1.65  2007/11/07 18:31:15  carsten
*** empty log message ***

Revision 1.64  2007/10/04 13:09:54  carsten
*** empty log message ***

Revision 1.63  2007/09/17 16:36:14  carsten
*** empty log message ***

Revision 1.62  2007/09/16 21:07:24  carsten
*** empty log message ***

Revision 1.61  2007/08/06 13:05:49  carsten
*** empty log message ***

Revision 1.60  2007/08/02 15:22:15  carsten
*** empty log message ***

Revision 1.59  2007/08/01 12:30:06  carsten
*** empty log message ***

Revision 1.58  2007/06/12 12:51:35  carsten
*** empty log message ***

Revision 1.57  2006/11/21 19:26:29  carsten
*** empty log message ***

Revision 1.56  2006/10/26 18:28:00  carsten
*** empty log message ***

Revision 1.55  2006/09/01 15:20:15  carsten
*** empty log message ***

Revision 1.54  2006/08/29 16:21:37  carsten
*** empty log message ***

Revision 1.52  2006/08/15 14:04:38  carsten
*** empty log message ***

Revision 1.51  2006/08/14 11:11:32  carsten
*** empty log message ***

Revision 1.50  2006/08/02 18:24:19  carsten
*** empty log message ***

Revision 1.49  2006/07/27 15:49:50  carsten
*** empty log message ***

Revision 1.48  2006/07/27 14:52:08  carsten
*** empty log message ***

Revision 1.47  2006/07/16 20:00:03  carsten
*** empty log message ***

Revision 1.46  2006/07/11 12:33:40  carsten
*** empty log message ***

Revision 1.44  2006/03/30 14:44:48  carsten
*** empty log message ***

Revision 1.43  2005/12/06 13:57:26  carsten
*** empty log message ***


Revision 1.42  2005/11/09 12:38:26  carsten
*** empty log message ***

Revision 1.41  2005/10/28 16:09:05  carsten
*** empty log message ***

Revision 1.40  2005/10/17 15:10:17  carsten
*** empty log message ***

Revision 1.39  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.38  2005/10/12 14:41:56  carsten
*** empty log message ***

Revision 1.37  2005/10/09 18:54:44  carsten
*** empty log message ***

Revision 1.36  2005/10/06 17:38:36  carsten
*** empty log message ***

Revision 1.35  2005/10/04 18:05:51  carsten
*** empty log message ***

Revision 1.34  2005/10/04 11:07:42  carsten
*** empty log message ***

Revision 1.33  2005/09/30 18:09:40  carsten
*** empty log message ***

Revision 1.32  2005/09/28 13:07:23  carsten
*** empty log message ***

Revision 1.31  2005/09/26 17:18:29  carsten
*** empty log message ***

Revision 1.30  2005/09/26 11:17:31  carsten
*** empty log message ***

Revision 1.29  2005/08/17 10:59:38  carsten
*** empty log message ***

Revision 1.28  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.27  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.26  2005/07/20 14:04:48  carsten
*** empty log message ***

Revision 1.25  2005/07/13 13:36:49  carsten
*** empty log message ***

Revision 1.24  2005/07/01 15:11:28  carsten
*** empty log message ***

Revision 1.23  2005/06/27 16:15:35  carsten
*** empty log message ***

Revision 1.22  2005/06/21 14:53:59  carsten
*** empty log message ***

Revision 1.21  2005/05/31 10:50:04  carsten
*** empty log message ***

Revision 1.20  2005/04/21 16:22:42  carsten
*** empty log message ***

Revision 1.19  2005/03/29 16:57:46  carsten
add inversion tools

Revision 1.18  2005/03/24 14:39:03  carsten
*** empty log message ***

Revision 1.17  2005/03/20 22:51:59  carsten
*** empty log message ***

Revision 1.16  2005/03/10 16:04:38  carsten
*** empty log message ***

Revision 1.15  2005/03/09 15:50:53  carsten
*** empty log message ***

Revision 1.13  2005/02/22 21:51:43  carsten
*** empty log message ***

Revision 1.12  2005/02/18 14:06:19  carsten
*** empty log message ***

Revision 1.11  2005/02/15 15:36:39  carsten
*** empty log message ***

Revision 1.9  2005/02/15 11:24:00  carsten
*** empty log message ***

Revision 1.8  2005/02/14 18:58:54  carsten
*** empty log message ***

Revision 1.7  2005/02/10 14:07:59  carsten
*** empty log message ***

Revision 1.6  2005/02/07 13:33:13  carsten
*** empty log message ***

Revision 1.5  2005/01/13 20:10:42  carsten
*** empty log message ***

Revision 1.4  2005/01/12 20:32:40  carsten
*** empty log message ***

Revision 1.3  2005/01/11 19:28:04  carsten
*** empty log message ***

Revision 1.1  2005/01/06 20:28:30  carsten
*** empty log message ***

*/
