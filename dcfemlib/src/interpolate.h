// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//


#ifndef INTERPOLATE__H
#define INTERPOLATE__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "basemesh.h"

#include <vector>
#include <set>
using namespace std;

// class ANNpointArray;
class ANNkd_tree;

class KDTree{
public:
  KDTree( const BaseMesh & mesh );

  ~KDTree();

  void search( const RealPos & queryPos, int neightN, vector < int > & nn_idx, vector < double > & dists ) const;
protected:
  int dimension_;
  int maxPts_;
  int nPts_;
  double ** dataPts_;
  ANNkd_tree * tree_;
};

MeshEntity * findMeshEntity( const KDTree & tree, const BaseMesh & mesh, const RealPos  & queryPos );
DLLEXPORT vector < MeshEntity * > findMeshEntities( const BaseMesh & mesh, const vector < RealPos > & queryVector, bool verbose );

vector < Node * > findMeshEntityOld( const KDTree & tree, const BaseMesh & mesh, RealPos  & queryPos );
vector < Node * > findMeshEntityMid( const KDTree & tree, const BaseMesh & mesh, RealPos  & queryPos );
DLLEXPORT vector < vector < Node * > > findMeshEntitiesOld( const BaseMesh & mesh, const vector < RealPos > & queryVector, bool verbose = false );

// void interpolate( const BaseMesh & inMesh, const vector < vector < RVector > > & data,
// 		  const vector < vector < Node * > > & vectorSamplingNodes, vector < vector < RVector > > & queryData );

// void interpolate( const BaseMesh & inMesh, const vector < RVector > & data,
// 		  const vector < vector < Node * > > & vectorSamplingNodes, vector < RVector > & queryData );

MeshEntity * isWithin( const RealPos & queryPos, BaseElement & cell, bool verbose );

vector < Node * > isWithin( const RealPos & queryPos, const Edge & edge, bool verbose );
vector < Node * > isWithin( const RealPos & queryPos, const Triangle & tri, bool verbose );
vector < Node * > isWithin( const RealPos & queryPos, const Tetrahedron & tet, bool verbose );

DLLEXPORT void interpolate( const vector < MeshEntity * > & vecEntities, const RVector & data,
			    const vector < RealPos > & queryVector, RVector & queryData );

DLLEXPORT void interpolate( const vector < MeshEntity * > & vecEntities, const vector < RVector > & data,
			    const vector < RealPos > & queryVector, vector < RVector > & queryData );

DLLEXPORT void interpolate( const vector < vector < Node * > > & vectorSamplingNodes, const RVector & data,
			    const vector < RealPos > & queryVector, RVector & queryData );

DLLEXPORT void interpolate( const vector < vector < Node * > > & vectorSamplingNodes, const vector < RVector > & data,
			    const vector < RealPos > & queryVector, vector < RVector > & queryData );

void interpolateValues( const vector < Node * > & vectorSamplingNodes, const vector < RVector > & data,
			const RealPos & queryPos, vector < RVector > & queryData, int ith );

void interpolateValuesEdge( const vector < Node * > & samplingPoints, const vector < RVector > & data,
			    const RealPos & queryPos, vector < RVector > & queryData, int ith );

void interpolateValuesTri( const vector < Node * > & samplingPoints, const vector < RVector > & data,
			   const RealPos & queryPos, vector < RVector > & queryData, int ith );

void interpolateValuesTet( const vector < Node * > & samplingPoints, const vector < RVector > & data,
			   const RealPos & queryPos, vector < RVector > & queryData, int ith );




Triangle * neighbourCellFrom2( Triangle * cell, int id );
bool touchFrom2( const Triangle & tri, const RealPos & pos,int & pFunIdx  );
void untagall();
DLLEXPORT double interpolateFrom2( Triangle * tri, const RealPos & pos, const RVector & data );
DLLEXPORT Triangle * meshFindCellFrom2( BaseMesh & mesh, const RealPos & pos, bool bruteForce );
DLLEXPORT void interpolateFrom2( BaseMesh & mesh, const RVector & data,
                        const BaseMesh & meshPos, RVector & iData, bool bruteForce );

#endif // INTERPOLATE__H

/*
$Log: interpolate.h,v $
Revision 1.10  2008/05/20 08:13:39  thomas
no message

Revision 1.9  2008/05/19 14:52:28  carsten
Add cholmodwrapper, interpolate V.2 in createSurface

Revision 1.8  2005/10/24 09:52:08  carsten
*** empty log message ***

Revision 1.7  2005/10/18 12:48:11  carsten
*** empty log message ***

Revision 1.6  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.5  2005/04/07 11:41:08  carsten
*** empty log message ***

Revision 1.4  2005/03/30 18:37:14  carsten
*** empty log message ***

Revision 1.3  2005/03/15 16:08:41  carsten
*** empty log message ***

Revision 1.2  2005/01/12 21:06:45  carsten
*** empty log message ***

Revision 1.1  2005/01/12 20:32:40  carsten
*** empty log message ***

*/
