// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include "domain3d.h"
#include "domain2d.h"
#include "trianglewrapper.h"

Domain3D::Domain3D( const Domain3D & domain ){
  cout << WHERE_AM_I << endl;
}

Facet * Domain3D::createFacet( const Domain2D & d2, int marker ){
  for ( int i = 0; i < d2.nodeCount(); i ++ ) createVIP( d2.node( i ).pos() );

    Facet * f = 0;

  if ( d2.nodeCount() == 4 ){
     f = createFacet( node( nodeCount()-4 ), node( nodeCount()-3 ),
		 node( nodeCount()-2 ), node( nodeCount()-1 ), marker );
  } else {
    TO_IMPL;
    insertFacet( d2.facet() );
  }

    return f;
}

int Domain3D::load( const string & filename, int fromOne, double snapTol  ){

  fstream file; if ( !openInFile( filename, & file ) ) return 0;

  vector < string > row;
  //** read Header;
  row = getNonEmptyRow( file, '#' );

  int nVerts = toInt( row[ 0 ] );
  int dimension = toInt( row[ 1 ] );
  int nAttributes = toInt( row[ 2 ] );
  int boolBndry = toInt( row[ 3 ] );

  //  cout << nVerts << " " << dimension << " " << nAttributes << " " << boolBndry << endl;

  if ( dimension == 2 ) {
    file.close();
    return hackLoadTrianglePolyFile( filename, fromOne );
  }

  //** read Nodes
  double x = 0.0, y = 0.0, z = 0.0;
  int vertMarker = 0;
  for ( int i = 0; i < nVerts; i++ ){
    row = getNonEmptyRow( file, '#' );
    x = toDouble( row[ 1 ] );
    y = toDouble( row[ 2 ] );
    z = toDouble( row[ 3 ] );

    for ( int j = 0; j < nAttributes; j++ ) {
      //   file >> dummyAttribute;
    }
    if ( boolBndry ) {
      vertMarker = toInt( row[ 3 + nAttributes + 1 ] );
    }
    createNode( RealPos(x, y, z).round( snapTol ), i, vertMarker );
  }

  //** read Facets;
  row = getNonEmptyRow( file, '#' );

  int nfacets = toInt( row[ 0 ] );
  boolBndry = toInt( row[ 1 ] );

  Facet facet;
  Polygon polygon;

  int nPoly = 0, nHoles = 0, nPolynodes = 0, polyNodeId = 0, facetMarker = 0;

  for ( int i = 0; i < nfacets; i++ ){
    row = getNonEmptyRow( file, '#' );

    nPoly = toInt( row[ 0 ] );
    nHoles = toInt( row[ 1 ] );

    if ( boolBndry ) facetMarker = toInt( row[ 2 ] );
    facet.clear();
    facet.setBoundaryMarker( facetMarker );

    for ( int j = 0; j < nPoly; j ++ ){
      row = getNonEmptyRow( file, '#' );
      nPolynodes = toInt( row[ 0 ] );

      polygon.clear();
      for ( int k = 1; k <= nPolynodes; k ++ ){
 	polyNodeId = toInt( row[ k ] );
 	polygon.push_back( & node( polyNodeId - fromOne ) );
      }
      facet.push_back( polygon );
    }

    for ( int k = 0; k < nHoles; k++ ){
      row = getNonEmptyRow( file, '#' );
      facet.createHole( RealPos( toDouble( row[ 1 ] ), toDouble( row[ 2 ] ), toDouble( row[ 3 ] ) ) );
    }

    insertFacet( facet );
  }


  //** read Holes;
  row = getNonEmptyRow( file, '#' );

  nHoles = toInt( row[ 0 ] );
  //  cout << nHoles << endl;

  for ( int i = 0; i < nHoles; i++ ){
    row = getNonEmptyRow( file, '#' );
    x = toDouble( row[ 1 ] );
    y = toDouble( row[ 2 ] );
    z = toDouble( row[ 3 ] );
    insertHole( RealPos( x, y, z ) );
  }
  //** read Regions;
  row = getNonEmptyRow( file, '#' );

  int nRegions = toInt( row[ 0 ] );
  //  cout << nRegions << endl;

  double attribute = 0.0, maxVolume = 0.0;
  for ( int i = 0; i < nRegions; i++ ){
    row = getNonEmptyRow( file, '#' );
    x = toDouble( row[ 1 ] );
    y = toDouble( row[ 2 ] );
    z = toDouble( row[ 3 ] );
    attribute = toDouble( row[ 4 ] );
    maxVolume = toDouble( row[ 5 ] );
    insertRegionMarker( RealPos( x, y, z ), attribute, maxVolume );
  }
  return 1;
}

int Domain3D::save( const string & filename, int fromOne ){
    return saveTetgenPolyFile( filename );
}

int Domain3D::splitFacetWithEdge( Facet & facet, const Polygon & edge ){

    if ( !facet.touch( ( edge[ 0 ]->pos() + edge[ 1 ]->pos() ) / 2.0, true, 1e-4 ) ) return -1;
    if ( facet[ 0 ].touch( ( edge[ 0 ]->pos() + edge[ 1 ]->pos() ) ) > -1) return -1;

    if ( !facet.contains( edge[ 0 ] ) || !facet.contains( edge[ 1 ] ) ) return -1;

    std::cout << "Domain3D::splitFacetWithEdge: " << facet << " " << edge << std::endl;

  Polygon oldPoly, newPoly;
  bool processNew = false, finishNew = false;
  bool processOld = false, finishOld = false;

  for ( size_t i = 0, imax = facet[ 0 ].size() + 1; i < imax; i ++ ){
    if ( i == facet[ 0 ].size() ) i = 0;

    if ( processOld && !finishOld ) {
      //      cout << "Old fill " << facet[ 0 ][ i ]->id() << endl;
      oldPoly.push_back( facet[ 0 ][ i ] );
    }
    if ( processNew && !finishNew ) {
      //      cout << "New fill " << facet[ 0 ][ i ]->id() << endl;
      newPoly.push_back( facet[ 0 ][ i ] );
    }
    if ( facet[ 0 ][ i ] == edge[ 0 ] ){
      if ( oldPoly.size() == 0){
	//	cout << "Start Old " << facet[ 0 ][ i ]->id() << endl;
	oldPoly.push_back( facet[ 0 ][ i ] );
	processOld = true;
      }
      if ( newPoly.size() > 2 ) finishNew = true;
    }
    if ( facet[ 0 ][ i ] == edge[ 1 ] ){
      if ( newPoly.size() == 0){
	//	cout << "Start New " << facet[ 0 ][ i ]->id() << endl;
	newPoly.push_back( facet[ 0 ][ i ] );
	processNew = true;
      }
      if ( oldPoly.size() > 2 ) finishOld = true;
    }
    //cout << finishNew <<"\t" << finishOld << endl;
    if ( finishNew && finishOld ) break;
  }
  facet.clear();
  facet.push_back( oldPoly );
  this->insertpFacet( new Facet( newPoly,
				 facet.leftMarker(),
				 facet.rightMarker(), facet.boundaryMarker() ) );
  return 1;
}

Polygon Domain3D::splitFacetWithLine( Facet & facet, const Line & line ){
    //! find intersection (line-facet) and split this facet, returns: polygon which splits the facet;
    //! intersection nodes will be created in domain.;

    std::list< RealPos > listIntersecPos = facet.intersect( line, 1e-5 );
    std::cout << "Domain3D::splitFacetWithLine " << facet.plane() << std::endl;
    Polygon poly;
    if ( listIntersecPos.size() > 1 ){
        std::cout << "Domain3D::splitFacetWithLine " << listIntersecPos.size() << std::endl;

        for ( list< RealPos >::iterator it = listIntersecPos.begin(); it != listIntersecPos.end(); it ++ ){
            poly.push_back( this->createVIP( (*it), facet.boundaryMarker() ) );
            cout << "Domain3D: " << poly.back()->id() << " " << poly.back()->pos()<< endl;
        }

        vector < Polygon > newEdges;
        Polygon edge;

        for ( size_t i = 0; i < poly.size() -1; i ++ ){
            //      cout << i <<"/" << poly.size()-1 << endl;
            edge.clear();
            edge.push_back( poly[ i ] );
            edge.push_back( poly[ i + 1 ] );
            std::cout << "Check edge << " <<edge[0]->id() << " " << edge[1]->id() << std::endl;
            if ( facet.touch( ( edge[ 0 ]->pos() + edge[ 1 ]->pos() ) / 2.0, true, 1e-4 ) ){
	       newEdges.push_back( edge );
            }	else {
                // 	cout << "Edge not on facet  " << endl;
                // 	cout <<
                //  	cout << facet.plane().distance( (( edge[ 0 ]->pos() + edge[ 1 ]->pos() ) / 2.0 ).round()) << endl;
 	    //  facet[0].switchDirection();
	// 	cout << facet.plane().distance( (( edge[ 0 ]->pos() + edge[ 1 ]->pos() ) / 2.0).round() ) << endl;
            }
        }

        for ( size_t i = 0; i < newEdges.size(); i ++ ){
            cout << "Domain3D: new Edge" << newEdges[i][0]->id() << "\t" << newEdges[i][1]->id() << endl;

            if ( splitFacetWithEdge( facet, newEdges[ i ] ) == -1 ){
	       if ( splitFacetWithEdge( *pFacet( facetCount() -1 ), newEdges[ i ] ) == -1 ){
	       //cout << WHERE_AM_I << " not splited" << endl;
	       }
            }
        }
    }
    return poly;
}

set < Node * > Domain3D::insertLine(const Line & line ){
  set < Node * > newNodes;
  Polygon poly;
  for ( int i = 0, imax = facetCount(); i < imax; i ++ ){
    //    cout << *pFacet( i );
    if ( pFacet( i )->plane().touch( line.center(), 1e-5 ) ){
        poly.clear();
        poly = splitFacetWithLine( *pFacet( i ), line );
        for ( size_t j = 0; j < poly.size(); j ++ ) newNodes.insert( poly[ j ] );
    } else {
     //   std::cout << i << " " << pFacet( i )->plane() << " " << pFacet( i )->plane().distance( line.center() )<< std::endl;
    }
  }
  return newNodes;
}

void Domain3D::insertPlane( const Plane & plane ){
    link( plane );
}

void Domain3D::merge( const Domain3D & subdomain, double tolerance, bool check ){
    if (!check) add( subdomain );
    else link( subdomain, tolerance );
}

void Domain3D::add( const Domain3D & subdomain ){
    for ( int i = 0, imax = subdomain.nodeCount(); i < imax; i ++ )
        subdomain.node( i ).setId( nodeCount() + i );
    for ( int i = 0, imax = subdomain.nodeCount(); i < imax; i ++ )
        createNode( subdomain.node( i ) );

    for ( int i = 0, imax = subdomain.regionCount(); i < imax; i ++ )
        insertRegionMarker( subdomain.region( i ) );
    for ( int i = 0, imax = subdomain.holeCount(); i < imax; i ++ )
        insertHole( subdomain.hole( i ) );
    for ( int i = 0, imax = subdomain.facetCount(); i < imax; i ++ )
        insertFacet( subdomain.facet( i ) );
}

void Domain3D::link( const Plane & plane ){
  Line touchLine;
  map < Facet *, Line > allLines;
  for ( int i = 0, imax = this->facetCount(); i < imax; i ++ ){
    //    cout << "check facet " << i << endl;
//     cout << " " << this->facet( i ).plane() << endl;

    if ( ( touchLine = this->facet( i ).plane().intersect( plane ) ).valid() ){
      //      cout << this->facet( i ).plane() << plane << touchLine << endl;
      allLines[ pFacet( i ) ] = touchLine;
    }
  }
  vector < Polygon > polyVector;
  Polygon poly;
  int count = 0;
  for ( map < Facet *, Line >::iterator it = allLines.begin(); it != allLines.end(); it ++ ){
//     cout << "insertLine " << count << endl; count ++;
//     cout << *(it->first) << (it->second) << endl;
    poly = splitFacetWithLine( *(it->first), (it->second) );
    if ( poly.size() > 0 ) polyVector.push_back( poly );
  }

  if ( polyVector.size() > 2 ){
    Facet *pNewFacet = new Facet( polyVector, 1, 2, 1 );
    this->insertpFacet( pNewFacet );
    //    cout << (*pNewFacet) << endl;
    //    if ( polyVector.size() > 8 )  exit(0);
    Polygon edge;
    vector < Polygon > newEdges;

    for ( size_t i = 0; i < polyVector.size(); i ++){
      //      cout << i << "/" << polyVector.size() << endl;
      if ( polyVector[ i ].size() > 1 ){
	for ( size_t j = 0; j < polyVector[ i ].size() -1; j ++ ){
	  edge.clear();
	  edge.push_back( polyVector[ i ][ j ] );
	  edge.push_back( polyVector[ i ][ j + 1 ] );
	  if ( pNewFacet->touch( ( edge[ 0 ]->pos() + edge[ 1 ]->pos() ) / 2.0 ) ){
	    if ( (*pNewFacet)[ 0 ].touch( ( edge[ 0 ]->pos() + edge[ 1 ]->pos() ) / 2.0 ) < 0){
	      newEdges.push_back( edge );
	    }
	  } else {
	    // 	  cout << WHERE_AM_I << "Edge not on facet  " << endl;
	    // 	  cout << pNewFacet->touch( ( edge[ 0 ]->pos() + edge[ 1 ]->pos() ) / 2.0 ) << endl;
	    // 	  cout << (*pNewFacet) << ( edge[ 0 ]->pos() + edge[ 1 ]->pos() ) / 2.0 << endl;
	    // 	  cout << pNewFacet->plane().distance( (( edge[ 0 ]->pos() + edge[ 1 ]->pos() ) / 2.0 ).round()) << endl;
	    // 	  (*pNewFacet)[0].switchDirection();
	    // 	  cout << pNewFacet->plane().distance( (( edge[ 0 ]->pos() + edge[ 1 ]->pos() ) / 2.0).round() ) << endl;
	  }
	}
      }
    }
//     for ( size_t i = 0; i < newEdges.size(); i ++ ){
//        cout << newEdges[ i ][ 0 ]->id() << "\t" << newEdges[ i ][ 1 ]->id() << endl;
//     }

    vector < Line > splitLines;
    set < Node * > pNodeSet;
    bool isNewLine = true;
    Line line;

    for ( size_t i = 0; i < newEdges.size(); i ++ ){
      pNodeSet.insert( newEdges[ i ][ 0 ] );  pNodeSet.insert( newEdges[ i ][ 1 ] );
      isNewLine = true;
      line.setPositions( newEdges[ i ][ 0 ]->pos(), newEdges[ i ][ 1 ]->pos() );

      for ( size_t j = 0; j < splitLines.size(); j ++ ){
	if ( splitLines[ j ] == line ) {
	  isNewLine = false;
	  break;
	}
      }
      if ( isNewLine ) splitLines.push_back( line );
    }

    //** search single-nodes;
    for ( size_t j = 0; j < (*pNewFacet)[ 0 ].size(); j ++ ) pNodeSet.erase( (*pNewFacet)[ 0 ][ j ] );
//     for ( set < Node * >::iterator it = pNodeSet.begin(); it != pNodeSet.end(); it ++){
//       cout << (*it) << "\t" << (*it)->id() << "\t" << pNewFacet->contains( (*it) )<< endl;
//     }

    for ( size_t j = 0; j < splitLines.size(); j ++ ){
      //      cout << splitLines[ j ] << endl;
      insertLine( splitLines[ j ] );

      for ( set < Node * >::iterator it = pNodeSet.begin(); it != pNodeSet.end(); it ++){
	//cout << "singleNode: " << (*it)->id() << (*it)->pos() << endl;
	if ( splitLines[ j ].touch( (*it)->pos() ) > 0 ){
	  this->insertNode( (*it) );
	  //	  cout << "singleNode: inserted " << (*it)->id() << (*it)->pos() << endl;
	  pNodeSet.erase( (*it) );
	}
      }
    }
  }
}

void Domain3D::link( const Facet & facet, double tolerance ){
    bool touchFacet = false;

    for ( int i = 0; i < this->facetCount(); i ++ ){
        //std::cout << "check, tolerance" << tolerance << std::endl;
        if ( this->facet( i ).touch( facet, tolerance ) ){
            //** found the facet touch one of domains facet

            touchFacet = true;
            //std::cout << "check" << std::endl;
            if ( this->facet( i ) != facet ){
                //** insert all polygons from facet to the new parent
                for ( uint j = 0; j < facet.size(); j ++ ){
                    this->facet( i ).push_back( facet[ j ] );
                }

                //** if the marker differ add the facet as hole to the parent and insert facet as new facet
                if ( this->facet( i ).boundaryMarker() != facet.boundaryMarker() ){
                    this->facet( i ).createHole( facet.center() );
                    touchFacet = false;
                }
            }
        }
    }

    //** if it dont touch insert as free facet
    if ( !touchFacet ) insertpFacet( new Facet( facet ) );
}

void Domain3D::link( const Domain3D & subdomain, double tolerance ){
    Node * tmpNode;
    size_t oldNodeCount = nodeCount();

    //** copy nodes, with duplicate check;
    for ( int i = 0, imax = subdomain.nodeCount(); i < imax; i ++ ){
        tmpNode=createVIP( subdomain.node( i ).pos(), subdomain.node(i).marker(), tolerance );
        subdomain.node( i ).setId( tmpNode->id() );
    }

    //** create Facets;
    for ( int i = 0, imax = subdomain.facetCount(); i < imax; i ++ )
        link( subdomain.facet( i ), tolerance );
    for ( int i = 0, imax = subdomain.regionCount(); i < imax; i ++ )
        insertRegionMarker( subdomain.region( i ) );
    for ( int i = 0, imax = subdomain.holeCount(); i < imax; i ++ )
        insertHole( subdomain.hole( i ) );

  //   Facet *pCommonFacetThis;
//   Facet *pCommonFacetSubdomain;
//   bool facetFound = false;

//   //** find common facet compare each of this.facets with each of subdomains.facets
//   for ( int i = 0, imax = this->facetCount(); i < imax; i ++ ){
//     for ( int j = 0, jmax = subdomain.facetCount(); j < jmax; j ++ ){
//       if ( this->facet( i ).touch( subdomain.facet( j ) ) ){
// 	cout << i << "\t" << j << " linked" << endl;
// 	pCommonFacetThis = this->pFacet( i );
// 	pCommonFacetSubdomain = &subdomain.facet( j );
// 	facetFound = true;
//       }
//     }
//   }

//   add( subdomain );
//   if ( facetFound ){

//     //(*pCommonFacetSubdomain)[ 0 ].switchDirection();
//     pCommonFacetThis->push_back( (*pCommonFacetSubdomain)[ 0 ] );
//     return 1;
//   }
//   cerr << WHERE_AM_I << " Warning cannot link!" << endl;
}

// Node * Domain3D::insertNode( const RealPos & pos, int marker ){
//   Node * node = createNode( pos, nodeCount(), marker );
//   for ( int i = 0, imax = facetCount(); i < imax; i ++ ){
//     //    if ( facet( i ).touch( pos ) ){
//   }
//   return node;
// }

void Domain3D::closeSurface( Node * n0, Node * n1, Node * n2, Node * n3, double zmin, int boundary, int region, double maxArea ){

  Node * n4 = createNode( n0->pos() ); n4->setZ( zmin );
  Node * n5 = createNode( n1->pos() ); n5->setZ( zmin );
  Node * n6 = createNode( n2->pos() ); n6->setZ( zmin );
  Node * n7 = createNode( n3->pos() ); n7->setZ( zmin );

  Polygon front( findAllNodesBetween( &node( n1->id() ), &node( n0->id() ) ) );
  front.push_back( n4 ); front.push_back( n5 );
  insertpFacet( new Facet( front, 1, 1, boundary ) );

  Polygon right( findAllNodesBetween( &node( n2->id() ), &node( n1->id() ) ) );
  right.push_back( n5 ); right.push_back( n6 );
  insertpFacet( new Facet( right, 1, 1, boundary ) );

  Polygon back( findAllNodesBetween( &node( n3->id() ), &node( n2->id() ) ) );
  back.push_back( n6 ); back.push_back( n7 );
  insertpFacet( new Facet( back, 1, 1, boundary ) );

  Polygon left( findAllNodesBetween( &node( n0->id() ), &node( n3->id() ) ) );
  left.push_back( n7 ); left.push_back( n4 );
  insertpFacet( new Facet( left, 1, 1, boundary ) );

  Polygon bottom;
  bottom.push_back( n4 ); bottom.push_back( n7 );
  bottom.push_back( n6 ); bottom.push_back( n5 );
  insertpFacet( new Facet( bottom, 1, 1, boundary ) );
  insertRegionMarker( RealPos( n4->pos() )+ RealPos(0.1, 0.1, 0.1), region, maxArea );
}


void Domain3D::hackClearDomain(){
  // Q&D
  vector< Facet * > tmp = vecpFacets_;
  vecpFacets_.clear();
  for ( int i = 0, imax = tmp.size(); i < imax ; i ++ ){
    if ( tmp[ i ]->size() != 0 ) vecpFacets_.push_back( tmp[ i ] );
  }
}

void Domain3D::closeFacets( Polygon & front, Polygon & back, int left, int right, int marker, bool close ){

  int startface = facetCount();
  for ( int i = 0, imax = front.size() - 1; i < imax; i ++ ){
    vecpFacets_.push_back( new Facet( front[ i ], front[ i + 1 ], back[ i + 1 ], back[ i ],
			 left, right, marker ) );
  }

//  if ( close ){
    vecpFacets_.push_back( new Facet( front[ front.size() - 1 ],
				      front[ 0 ],
				      back[ 0 ],
				      back[ back.size() - 1 ],
				      left, right, marker ) );
  if ( close ){
    Polygon frontRev( front ); switchDirection( frontRev );
    Polygon backRev( back ); //switchDirection( backRev );

    vecpFacets_.push_back( new Facet( frontRev, left, right, marker ) );
    vecpFacets_.push_back( new Facet( backRev, left, right, marker ) );
  }

  if ( marker == 99 ){ // 99 = automatic
    for ( int i = startface, imax = facetCount(); i < imax; i ++ ){
      pFacet( i )->setBoundaryMarkerGivenByNodes( );
    }
  }
}

void Domain3D::insertVIP( Node * node ){
  insertNode( node );
}

Node * Domain3D::nodeAt( const RealPos & pos, double tolerance ){
  for ( int i = 0, imax = nodeCount(); i < imax; i ++ ){
    if ( ( node( i ).pos() - pos ).abs() < tolerance ){
      return &node( i );
    }
  }
  return NULL;
}

Node * Domain3D::createVIP( const RealPos & pos, int marker, double tolerance ){
  Node * node;
  if ( (node = nodeAt( pos, tolerance ) ) == NULL ){
    node = Mesh3D::createNode( pos, nodeCount(), marker );
    //if ( verbose_ )          cout << WHERE_AM_I << node->id() << node->pos() << endl;
    insertNode( node );
  } else{
    node->setMarker( marker );
  }
  return node;
}

void Domain3D::createLineOfVIPs( const RealPos & start, const RealPos & end, int electrodeCount, int marker ){
  if  ( electrodeCount < 2 ) cerr << WHERE_AM_I << "electrodeCount < 2" << endl;
  Line line( start, end );
  double dt = 1.0 / ( electrodeCount - 1 );
  for ( int i = 0; i < electrodeCount; i ++ ){
    //    cout << line.lineAt( dt * i ) << endl;
    createVIP( line.lineAt( dt * i ),  marker );
  }
}

void Domain3D::insertNode( Node * node ){
  bool touchedFacet = false;

  for ( int i = 0, imax = this->facetCount(); i < imax; i ++ ){
   //if ( verbose_ )cout << WHERE_AM_I << &facet( i ) << "\t" << node->id() << "\t" << facet( i )[ 0 ].size() << endl;
    if ( facet( i )[ 0 ].size() > 2){
        //if ( verbose_  ) cout << WHERE_AM_I << node->id() << node->pos() << endl;
      if ( facet( i ).insertNode( node, verbose_) ) {
	touchedFacet = true;
      }
    }
  }
  //** if node touch nothing -- insert free node;
  if ( !touchedFacet ){ insertFreeNode( *node );  }
}

void Domain3D::insertNodeOnFacet( Facet & facet, Node & node ){
  facet.insertFreeNode( &node );
}

void Domain3D::insertNodeOnPolygon( Polygon & poly, Node & node ){
  TO_IMPL
}

void Domain3D::insertFreeNode( Node & node ){
//   Polygon poly;
//   //** doppelt fuer Tetgens Single Vertex Polygon
//   poly.push_back( &node );
//   poly.push_back( &node );
//   vecpFacets_.push_back( new Facet( poly, 0,0,0 ) );
}

Node * Domain3D::createNodeOnFacet( Facet & facet, double x, double y, double z, int marker, double dx, double dy, double dz ){

  if ( dx > 0.0 || dy > 0.0 || dz > 0.0) createNodeOnFacet( facet,  x - dx, y - dy, z - dz, 0 );

  Node * node = createNode( x, y, z, nodeCount(), marker );
  Polygon poly;
  //** doppelt fuer Tetgens Single Vertex Polygon
  poly.push_back( node );
  poly.push_back( node );
  facet.push_back( poly );

  if ( dx > 0.0 || dy > 0.0 || dz > 0.0) createNodeOnFacet( facet,  x + dx, y + dy, z + dz, 0 );
  return node;
}


Node * Domain3D::createFreeNode( double x, double y, double z, int marker, double dx, double dy, double dz ){
  cerr << WHERE_AM_I << "Obsolete, use createFreeNode(..) instead, this method will be removed" << endl;

//   if ( dx > 0.0 || dy > 0.0 || dz > 0.0) createFreeNode( x - dx, y - dy, z - dz, 0 );

//   Node * node = createNode( x, y, z, nodeCount(), marker );

//   Polygon poly;
//   //** doppelt fuer Tetgens Single Vertex Polygon
//   poly.push_back( node );
//   poly.push_back( node );

//   vecpFacets_.push_back( new Facet( poly, 0,0,0 ) );

//   if ( dx > 0.0 || dy > 0.0 || dz > 0.0) insertFreeNode( x + dx, y + dy, z + dz, 0 );

  return NULL;
}


Facet * Domain3D::createSimpleMainDomain( const RealPos & start, const RealPos & end ){
  Facet * surface;

  Polygon front;
  front.push_back( createNode( start.x(), start.y(), start.z(), nodeCount(), MIXED_BC ) );
  front.push_back( createNode( end.x(), start.y(), start.z(), nodeCount(), MIXED_BC ) );
  front.push_back( createNode( end.x(), start.y(), end.z(), nodeCount(), MIXED_BC ) );
  front.push_back( createNode( start.x(), start.y(), end.z(), nodeCount(), MIXED_BC ) );
  Polygon back = extrude( *this, front, 0, end.y() - start.y(), 0 );

  //** create LEFT/RIGHT/BOTTIOM Facet;
  closeFacets( front, back, 1, -1, MIXED_BC, true );

  //** Q&D
    //  (*vecpFacets_[5])[0].switchDirection();

  //** create Surface Facet;
  surface = vecpFacets_[ vecpFacets_.size() - 6 ];
  surface->setBoundaryMarker( HOMOGEN_NEUMANN_BC );

  return surface;
}

Facet * Domain3D::createSandbox( const RealPos & start, const RealPos & end, double region, double dx ){
  Facet * surface;

  Polygon front;
  front.push_back( createNode( start.x(), start.y(), start.z(), nodeCount(), 1 ) );
  front.push_back( createNode( end.x(), start.y(), start.z(), nodeCount(), 1 ) );
  front.push_back( createNode( end.x(), start.y(), end.z(), nodeCount(), 1 ) );
  front.push_back( createNode( start.x(), start.y(), end.z(), nodeCount(), 1 ) );
  Polygon back = extrude( *this, front, 0, end.y() - start.y(), 0 );

  //** create LEFT/RIGHT/BOTTIOM Facet;
  closeFacets( front, back, 1, -1, 1, true );

  //** create Surface Facet;
  surface = vecpFacets_[ vecpFacets_.size() - 6 ];
  surface->setBoundaryMarker( HOMOGEN_NEUMANN_BC );
  insertRegionMarker( RealPos( start.x() + 0.01, start.y() + 0.01, start.z() - 0.01 ), region, dx );

  return surface;
}

void Domain3D::createBoundary( Facet & surface, const RealPos & start, const RealPos & end, double region, double dx ){

  Polygon top;
  top.push_back( createNode( start.x(), start.y(), start.z(), nodeCount(), HOMOGEN_NEUMANN_BC ) );
  top.push_back( createNode( end.x(), start.y(), start.z(), nodeCount(), HOMOGEN_NEUMANN_BC ) );
  top.push_back( createNode( end.x(), end.y(), start.z(), nodeCount(), HOMOGEN_NEUMANN_BC ) );
  top.push_back( createNode( start.x(), end.y(), start.z(), nodeCount(), HOMOGEN_NEUMANN_BC ) );
  Polygon bottom = extrude( *this, top, 0, 0, end.z() - start.z() );

  surface.push_back( top );

  for ( int i = 0, imax = top.size() - 1; i < imax; i ++ ){
  vecpFacets_.push_back( new Facet( top[ i ], top[ i + 1 ], bottom[ i + 1 ], bottom[ i ],
				    static_cast<int>(region), -1, MIXED_BC ) );
  }
  vecpFacets_.push_back( new Facet( top[ top.size() - 1 ], top[ 0 ],
				    bottom[ 0 ], bottom[ top.size() - 1 ],
				    static_cast<int>(region), -1, MIXED_BC ) );

  vecpFacets_.push_back( new Facet( bottom, (int)region, -1, MIXED_BC ) );

  insertRegionMarker( RealPos( start.x() + 0.01, start.y() + 0.01, start.z() - 0.01 ),
		      static_cast<int>(region), dx );
}

int Domain3D::createTopographySurface( const Mesh3D & surfacemesh, double layer1 ){
  //** start TOP;
  for ( int i = 0, imax = surfacemesh.nodeCount(); i < imax; i++ ){
    createNode( surfacemesh.node( i ).x(), surfacemesh.node( i ).y(), surfacemesh.node( i ).z(),
		nodeCount(), surfacemesh.node( i ).marker() );
  }

  for ( int i = 0, imax = surfacemesh.boundaryCount(); i < imax; i++ ){
    createFacet( surfacemesh.boundary( i ).node( 0 ),
		 surfacemesh.boundary( i ).node( 1 ),
		 surfacemesh.boundary( i ).node( 2 ),
		 HOMOGEN_NEUMANN_BC );
  }

  return 0;
}

void Domain3D::addSurface( const Mesh2D & mesh, double z ){
  vector < Node * > nodes;
  for ( int i = 0; i < mesh.nodeCount(); i ++ ){
    nodes.push_back( this->createVIP( RealPos( mesh.node( i ).x(), mesh.node( i ).y(), z ), mesh.node( i ).marker() ) );
  }
  for ( int i = 0; i < mesh.cellCount(); i ++ ){
    if ( mesh.cell( i ).nodeCount() == 3 ){
      this->insertpFacet( new Facet( nodes[ mesh.cell( i ).node( 0 ).id() ],
				     nodes[ mesh.cell( i ).node( 1 ).id() ],
				     nodes[ mesh.cell( i ).node( 2 ).id() ], 1, -1, -1 ) );
    } else {
      TO_IMPL;
    }
  }
}

void Domain3D::merge( const Mesh2D & mesh, int bc ){
  vector < Node * > nodes;
  for ( int i = 0; i < mesh.nodeCount(); i ++ ){
    nodes.push_back( this->createVIP( mesh.node( i ).pos(), mesh.node( i ).marker() ) );
  }
  for ( int i = 0; i < mesh.cellCount(); i ++ ){
    if ( mesh.cell( i ).nodeCount() == 3 ){
      createFacet( *nodes[ mesh.cell( i ).node( 0 ).id() ],
		   *nodes[ mesh.cell( i ).node( 1 ).id() ],
		   *nodes[ mesh.cell( i ).node( 2 ).id() ], bc );
    } else {
      TO_IMPL;
    }
  }
}

Polygon Domain3D::findAllNodesBetween( Node * pNodeStart, Node * pNodeEnd, int marker, double tol ){
  map < double, Node * > nodeMap;

  nodeMap[ 0.0 ] = pNodeStart;
  nodeMap[ pNodeStart->distanceTo( *pNodeEnd ) ] = pNodeEnd;

  Line line( pNodeStart->pos(), pNodeEnd->pos() );

  for ( int i = 0, imax = nodeCount(); i < imax; i ++){
    if ( marker == -1 ){
      if ( line.touch( this->node( i ).pos(), tol ) == 3 ){
	nodeMap[ pNodeStart->distanceTo( this->node( i ) ) ] = &this->node( i );
      }
    } else if ( this->node( i ).marker() == marker ){
      nodeMap[ pNodeStart->distanceTo( this->node( i ) ) ] = &this->node( i );
    }
  }

  Polygon poly;
  for ( map< double, Node * >::iterator it = nodeMap.begin(); it != nodeMap.end(); it ++){
    poly.push_back( it->second );
  }
  return poly;
}

Polygon Domain3D::findAllNodesBetween( const RealPos & start, const RealPos & end ){
  map < double, Node * > nodeMap;

  nodeMap[ 0.0 ] = nodeAt( start );
  nodeMap[ start.distance( end ) ] = nodeAt( end );

  Line line( start, end );
//   cout << nodeAt( start )->id() << endl;
//   cout << nodeAt( end )->id() << endl;
  for ( int i = 0, imax = nodeCount(); i < imax; i ++){
    if ( line.touch( this->node( i ).pos() ) == 3 ){
      nodeMap[ start.distance( this->node( i ).pos() ) ] = &this->node( i );
      //      cout << this->node( i ).id() << endl;
    }
  }

  Polygon poly;
  for ( map< double, Node * >::iterator it = nodeMap.begin(); it != nodeMap.end(); it ++){
    poly.push_back( it->second );
  }
  return poly;
}

Polygon lookingForSubNodes( Mesh2D & source, Mesh3D & target, int edgemarker, Node * startnode, Node * endnode){
  Polygon subnodes;
  vector < Node * > newnodes;
  set< int > subIds;
  map<  double, int > subSort;

  for ( int i = 0, imax = source.boundaryCount(); i < imax; i++ ){
    if ( source.boundary( i ).marker() == edgemarker ){
      subIds.insert( dynamic_cast< Edge &>( source.boundary( i ) ).nodeA().id() );
      subIds.insert( dynamic_cast< Edge &>( source.boundary( i ) ).nodeB().id() );
    }
  }
  subIds.erase( startnode->id() );
  subIds.erase( endnode->id() );

  for ( set< int >::iterator it = subIds.begin(); it !=subIds.end(); it ++){
//     cout << startnode->id() << "\t" << startnode->distance( target.node( *it ) ) << "\t" << (*it) << endl;
//     cout << "\t" << target.node( *it ).x() << "," << target.node( *it ).y() << "," <<  target.node( *it ).z() << endl;
    subSort[ startnode->distance( target.node( *it ) ) ] = (*it);
  }

  for ( map < double, int >::iterator it = subSort.begin(); it != subSort.end(); it ++){
    subnodes.push_back( & target.node( (*it).second ) );
  }

  return subnodes;
}

void Domain3D::switchXY(){
//   double xtmp = 0.0;

//   for ( int i = 0, imax = nodeCount(); i < imax ; i++ ){
//     xtmp = node( i ).x();
//     node( i ).setX( node( i ).y() );
//     node( i ).setY( xtmp );
//   }
//   for ( int i = 0, imax = regions_.size(); i < imax ; i++ ){
//     xtmp = regions_[ i ]->x();
//     regions_[ i ]->setX( regions_[ i ]->y() );
//     regions_[ i ]->setY( xtmp );
//   }
//   for ( int i = 0, imax = vecpHoles_.size(); i < imax ; i++ ){
//     xtmp = vecpHoles_[ i ]->x();
//     vecpHoles_[ i ]->setX( vecpHoles_[ i ]->y() );
//     vecpHoles_[ i ]->setY( xtmp );
//   }
  cerr << WHERE_AM_I << " Obsolete, use rotate(x,y,z) instead." << endl;
}

int Domain3D::checkAllFacets( bool verbose ){
  int valid = 1;
  for ( int i = 0, imax = facetCount(); i < imax; i ++ ){
    if ( !facet( i ).valid() ) {
      valid--;
      if ( verbose ) cerr << WHERE_AM_I << " Warning! Facet " << i << " is NOT valid." << endl;
    }
  }
  return valid;
}

void Domain3D::setPos( const RealPos & pos ){
  if ( !(pos_ == pos) ){
    translate( pos.x() - pos_.x(), pos.y() - pos_.y(), pos.z() - pos_.z() );
  }
}

void Domain3D::insertLayer( const Facet & surface, double depth, int region ){
  // Hack
  Facet *copyface = new Facet();
  Polygon poly;
  for ( int i = 0, imax = surface.size(); i < imax; i ++ ){
    poly.clear();
    for ( int j = 0, jmax = surface[ i ].size(); j < jmax; j ++ ){
      poly.push_back( createNode(surface[ i ][ j ]->x(),
				 surface[ i ][ j ]->y(),
				 surface[ i ][ j ]->z() + depth, nodeCount() ) );
    }
    copyface->push_back( poly );
  }
  copyface->setLeftMarker( region );
  copyface->setRightMarker( region );
  copyface->setBoundaryMarker( region );
  this->insertFacet( *copyface );
}

int Domain3D::loadSTL( const string & filename ){
  fstream file; if( !openInFile( filename, & file ) ) return 0;
  string solid;

  vector < string > row;

  row = getNonEmptyRow( file );
  if ( row[ 0 ] != "solid" ) {
    //    cerr << WHERE_AM_I << " file no vaid ascii-stl-format, try binary" << endl; file.close();
    return loadSTLBinary( filename );
   }

  vector < RealPos > allVerts;
  bool finish = false;
  while ( !finish ){
    row = getNonEmptyRow( file );
    if ( row[0] == "facet" && row[1] == "normal" ){ //** facet normal  0.0  0.0  0.0
      row = getNonEmptyRow( file );	 //** outer loop
	row = getNonEmptyRow( file ); //** vertex x y z;
	allVerts.push_back( RealPos( toDouble( row[ 1 ] ), toDouble( row[ 2 ] ), toDouble( row[ 3 ] ) ) );
	row = getNonEmptyRow( file ); //** vertex x y z
	allVerts.push_back( RealPos( toDouble( row[ 1 ] ), toDouble( row[ 2 ] ), toDouble( row[ 3 ] ) ) );
	row = getNonEmptyRow( file ); //** vertex x y z
	allVerts.push_back( RealPos( toDouble( row[ 1 ] ), toDouble( row[ 2 ] ), toDouble( row[ 3 ] ) ) );
      row = getNonEmptyRow( file );	 //** endloop
      row = getNonEmptyRow( file );	 //** endfacet;
    } else finish = true;
  }

  Node *n1, *n2, *n3;
  if ( allVerts.size() % 3 == 0 && allVerts.size() > 0 ){
    for ( uint i = 0; i < allVerts.size() / 3; i ++ ){
      n1 = createVIP( allVerts[ i * 3 ] );
      n2 = createVIP( allVerts[ i * 3 + 1 ] );
      n3 = createVIP( allVerts[ i * 3 + 2 ] );
      createFacet( *n1, *n2, *n3, 0);
    }
  } else {
    cerr << WHERE_AM_I << " there is something wrong in ascii-stl-format "
	 << allVerts.size() << " " << allVerts.size() % 3 << endl;
    file.close(); return 0;
  }
  file.close();
return 1;
    
}

int Domain3D::loadSTLBinary( const string & filename ){
  FILE * file; file = fopen( filename.c_str(), "r+b" );

  char header[ 80 ];
  fread( &header, 1, 80, file );
  int nFaces = 0;
  fread( &nFaces, 4, 1, file );
  std::cout << "binary stl file with " << nFaces << " Faces" << std::endl;

  vector < RealPos > allVerts;

  float rd[ 48 ];
  char padding[ 2 ];
  for ( int i = 0; i < nFaces; i ++ ){
    fread( &rd, 4, 12, file );
    allVerts.push_back( RealPos( rd[ 3 ],  rd[ 4 ],  rd[ 5 ] ).round( 1e-5 ) );
    allVerts.push_back( RealPos( rd[ 6 ],  rd[ 7 ],  rd[ 8 ] ).round( 1e-5 ) );
    allVerts.push_back( RealPos( rd[ 9 ], rd[ 10 ], rd[ 11 ] ).round( 1e-5 ) );

    fread( &padding, 1, 2, file );
  }

  Node *n1, *n2, *n3;
  if ( allVerts.size() % 3 == 0 && allVerts.size() > 0 ){
    for ( uint i = 0; i < allVerts.size() / 3; i ++ ){
      n1 = createVIP( allVerts[ i * 3 ] );
      n2 = createVIP( allVerts[ i * 3 + 1 ] );
      n3 = createVIP( allVerts[ i * 3 + 2 ] );
      createFacet( *n1, *n2, *n3, 0);
    }
  } else {
    cerr << WHERE_AM_I << " there is something wrong in ascii-stl-format "
	 << allVerts.size() << " " << allVerts.size() % 3 << endl;
    fclose( file );
    return 0;
  }
  fclose( file );
  return 1;
}

int Domain3D::loadWaveFrontObj( const string & filename ){
  fstream file; if ( !openInFile( filename, & file ) ) return 0;

  //** line starts with V x y z ( vertex x y z )
  //** line starts with f vi ... vj  ( face vertex(i) ... vertex(j) )
  //** line starts with g name ( group name of group of faces )

  char buf[1024];
  string line;
  vector< string > valuesPerLine;
  int tokenpos, oldsize = 0;
  Facet facet;
  Polygon polygon;
  while( file.getline( buf, 1024, '\n' ) ){
    line = buf;
    if ( line[ 0 ] != '#' ){
      //** aus vielen spaces mach 1 space
      while( ( tokenpos = line.find("  ") ) != -1 ){
	line.replace( tokenpos, 2, " " );
      }
      //** aus jedem Space mach 1 tab;
      while( ( tokenpos = line.find(" ") ) != -1 ){
	line.replace( tokenpos, 1, "\t" );
      }

      valuesPerLine.clear();
      while ( line.length() > 0 ){
	//** vom Anfang bis zum ende werden die values abgeschnitten und im vector
	//** values geparkt (tokens sind tab space und endl)
	//** gehts bestimmt auch schoener
	if ( ( tokenpos = line.find('\t') ) == -1){
	  if ( ( tokenpos = line.find('\n') ) == -1){
	    tokenpos = line.size();
	  }
	}
	valuesPerLine.push_back( line.substr( 0, tokenpos ).c_str() );
	line.erase( 0, tokenpos + 1 );
      }
      if ( valuesPerLine[ 0 ] == "v" ){
	createNode( atof( valuesPerLine[ 1 ].c_str() ),
		    atof( valuesPerLine[ 2 ].c_str() ),
		    atof( valuesPerLine[ 3 ].c_str() ), nodeCount(), 0 );
      }
      if ( valuesPerLine[ 0 ] == "f" ){
	facet.clear();
	polygon.clear();
	for ( unsigned int k = 1; k < valuesPerLine.size(); k ++ ){
	  polygon.push_back( & node( atoi( valuesPerLine[ k ].c_str() ) - 1 ) );
	}
	facet.push_back( polygon );

	facet.setBoundaryMarker( 1 );
	insertFacet( facet );
      }
      if ( valuesPerLine[ 0 ] == "g" ){

      }
    }
  }

  file.close();
  return 1;
}

int Domain3D::saveVTK( const string & filename, bool verbose ){
  fstream file; openOutFile( filename.substr( 0, filename.rfind( ".vtk" ) ) + ".vtk", & file );

  file << "# vtk DataFile Version 3.0" << endl;
  file << "created by " << WHERE_AM_I << endl;
  file << "ASCII" << endl;
  file << "DATASET POLYDATA" << endl;
  file << "POINTS " << nodeCount() << " double" << endl;

  for ( int i = 0; i < nodeCount(); i ++ ){
    file << node( i ).x() << "\t" << node( i ).y() << "\t" << node( i ).z() << endl;
  }
  int nFacets = vecpFacets_.size();
  int nNodesPoly = 0;
  int size = 0;
  for ( size_t i = 0, imax = nFacets; i < imax; i ++ ){
    nNodesPoly = facet( i )[ 0 ].size();
    size += nNodesPoly;
  }
  size += nFacets;

  file << "POLYGONS "<< nFacets << " " << size << endl;

  for ( size_t i = 0, imax = nFacets; i < imax; i ++ ){

    nNodesPoly = facet( i )[ 0 ].size();
    file << nNodesPoly << "\t";

    for ( size_t k = 0, kmax = nNodesPoly; k < kmax; k++ ){
      file << facet( i )[ 0 ][ k ]->id() << "\t";
    }
    file << endl;
  }

  file.close();
  return 1;
}

int Domain3D::saveSTL( const string & filename, bool verbose ){
  fstream file; openOutFile( filename.substr( 0, filename.rfind( ".stl" ) ) + ".stl", & file );
  file.precision( 14 );

  file << "solid" << endl;

  for ( size_t i = 0, imax = this->facetCount(); i < imax; i ++ ){
    //  for ( size_t i = 1, imax = 2; i < imax; i ++ ){

    Mesh2D mesh( this->facet( i ).triangulate() );

    if ( mesh.cellCount() > 0 ){
      RealPos norm( Plane( mesh.cell( 0 ).node( 0 ).pos(),
			   mesh.cell( 0 ).node( 1 ).pos(),
			   mesh.cell( 0 ).node( 2 ).pos() ).norm() );

      for ( int j = 0; j < mesh.cellCount(); j ++ ){

	file << "facet normal " <<  norm.x() << " " << norm.y() << " " << norm.z() << endl;
	file << "outer loop" << endl;
	for ( int k = 0; k < 3; k ++ ){
	  file << "vertex "
	       << mesh.cell( j ).node( k ).x() << " "
	       << mesh.cell( j ).node( k ).y() << " "
	       << mesh.cell( j ).node( k ).z() << endl;
	}
	file << "endloop" << endl;
	file << "endfacet" << endl;
      }
    }
  }

  file << "endsolid" << endl;
  file.close();
  return 1;
}

int Domain3D::saveSTLOld( const string & filename, bool verbose ){
  fstream file; openOutFile( filename.substr( 0, filename.rfind( ".stl" ) ) + ".stl", & file );
  file.precision( 14 );

  int nFacets = vecpFacets_.size();
  int nPolyNodes = 0;
  int nPoly = 0;
  RealPos norm;

  file << "solid" << endl;

  for ( size_t i = 0, imax = nFacets; i < imax; i ++ ){
    if ( facet( i )[ 0 ].size() > 3 ) triangulateFacetOld( facet( i ), verbose );

    nPoly = facet( i ).size();

    for ( size_t j = 0, jmax = nPoly; j < jmax; j++ ){
      nPolyNodes = facet( i )[ j ].size();
      if ( nPolyNodes == 3 ){
	norm = Plane( facet( i )[ j ][ 0 ]->pos(), facet( i )[ j ][ 1 ]->pos(), facet( i )[ j ][ 2 ]->pos() ).norm();
	file << "facet normal " <<  norm.x() << " " << norm.y() << " " << norm.z() << endl;
	file << "outer loop" << endl;
	for ( int k = 0; k < 3; k ++ ){
	  file << "vertex "
	       << facet( i )[ j ][ k ]->x() << " "
	       << facet( i )[ j ][ k ]->y() << " "
	       << facet( i )[ j ][ k ]->z() << endl;
	}
	file << "endloop" << endl;
	file << "endfacet" << endl;
      } else {
	cerr << WHERE_AM_I << " cannot handle facet with " << nPolyNodes << " vertices" << endl;
      }
    }
  }
  file << "endsolid" << endl;
  file.close();

  return 1;
}

int Domain3D::triangulateFacet( Facet & facet, bool verbose ){
TO_IMPL
return 0;
    
}
int Domain3D::triangulateFacetOld( Facet & facet, double quality, bool verbose ){
  //** funktioniert nur mit faceten mit einem polygon, mit mehreren muessen erst alle Knoten gesammelt werden, doppelte geloescht etc.

  RealPos midPos( averagePosition( this->nodePositions() ) );
  Domain2D facetPoly;
  int nVerts = facet[ 0 ].size();

  for ( int i = 0; i < nVerts; i ++ ){
    //  marker +1, wenn marker = 0 dann setzt triangle das eigenstaendig um
    facetPoly.createNode( facet[ 0 ][ i ]->pos(), facetPoly.nodeCount(), facet[ 0 ][ i ]->id() + 1);
  }
  for ( int i = 0; i < nVerts; i ++ ){
    facetPoly.createEdge( facetPoly.node( i ), facetPoly.node( (i + 1)%nVerts ) );
  }

  //** ich muss das drehen bis z==0
  RealPos planeNormTarget( 0.0, 0.0, 1.0 );
  RealPos planeNorm( facet.norm() );
  RealPos rot( planeNorm.cross( planeNormTarget ) );
  double phix = asin( rot.x() ), phiy = asin( rot.y() ), phiz = asin( rot.z() );

  facetPoly.rotate( phix, 0.0, 0.0 ); facetPoly.rotate( 0.0, phiy, 0.0 ); facetPoly.rotate( 0.0, 0.0, phiz );
  double zOffset = facetPoly.node( 0 ).z();
  //** gedreht


  Mesh2D newmesh( facetPoly.createMesh( quality, verbose ) );
  if ( newmesh.nodeCount() != nVerts && quality == 0.0 ){
    newmesh.save("tritestnew");
    facetPoly.save("tritest");
    cerr << WHERE_AM_I << " Oops. This should not happen." << endl;
    cerr << " facetPoly.nodeCount() " << facetPoly.nodeCount() << " != newmesh.nodeCount() " << newmesh.nodeCount() << endl;
    exit(0);
  }

  newmesh.translate( 0.0, 0.0, zOffset );
  newmesh.rotate( 0.0, 0.0, -phiz );  newmesh.rotate( 0.0, -phiy, 0.0 ); newmesh.rotate( -phix, 0.0, 0.0 );


  vector < Node * > newNodes( newmesh.nodeCount() );
  for ( int i = 0; i < newmesh.nodeCount(); i ++ ){
    newNodes[ i ] = this->createVIP( newmesh.node( i ).pos() );
  }

  facet.clear();
  Polygon poly;
  for ( int i = 0; i < newmesh.cellCount(); i ++ ){
    poly.clear();

    if ( ( planeNorm + newmesh.cell( i ).middlePos() ).distance( midPos )
	 > newmesh.cell( i ).middlePos().distance( midPos ) ){ // ok norm zeigt nach aussen
      for ( int j = 0; j < 3; j ++ ) {
	poly.push_back( newNodes[ newmesh.cell( i ).node( j ).id() ] );
      }
    } else { // else norm zeigt nach innen
      for ( int j = 2; j > -1; j -- ) {
	poly.push_back( newNodes[ newmesh.cell( i ).node( j ).id() ] );
      }
    }

    this->createFacet( poly, facet.boundaryMarker() );
//      facet.push_back( *new Polygon( poly ) );
//     if ( i == 0 ) {
//       facet.push_back( *new Polygon( poly ) );
//     } else {
//       this->createFacet( poly, facet.boundaryMarker() );
//     }
  }
  return 1;
}

int Domain3D::saveOOGL_OFFFile( const string & filename ){
  fstream file; openOutFile( filename.substr( 0, filename.rfind( ".off" ) ) + ".off", & file );

  int nverts = nodeCount();
  int nfacets = vecpFacets_.size();
  int npolygons = 0;

  for ( int i = 0; i < nfacets; i++ ){
    npolygons += vecpFacets_[ i ]->size();
  }

  file << "OFF" << endl;
  file << nverts << "\t" << npolygons << "\t" << 0 << endl;

  for ( int i = 0; i < nverts; i ++ ){
    file << node( i ).x()
	 << "\t" << node( i ).y()
	 << "\t" << node( i ).z() << endl;
  }

  Facet facet;
  Polygon polygon;
  for ( int i = 0; i < nfacets; i++ ){
    facet = (*vecpFacets_[ i ]);
    for ( unsigned  int j = 0; j < facet.size(); j ++ ){
      polygon = facet[ j ];
      file << polygon.size() << "\t" ;
      for ( unsigned int k = 0; k < polygon.size(); k ++ ){
	file << polygon[ k ]->id() << "\t";
      }
      file << endl;
    }
  }
  file.close();
  return 1;
}

int Domain3D::saveGRUMMPBdryFile( const string & filename, bool checkValid ){
  fstream file; openOutFile( filename, & file );

  size_t nverts = nodeCount();
  size_t nfacets = facetCount();
  size_t npolygons = 0;

  for ( size_t i = 0; i < nfacets; i++ ){
    npolygons += facet( i ).size();
  }

  file << nverts << "\t" << npolygons << endl;

  for ( size_t i = 0; i < nverts; i++ ){
    file << node( i ).x()
	 << "\t" << node( i ).y()
	 << "\t" << node( i ).z() << endl;
  }
  size_t nNodePoly = 0;
  for ( size_t i = 0; i < nfacets; i++ ){
    for ( size_t j = 0; j < facet( i ).size(); j ++ ){
      nNodePoly = facet( i )[ j ].size();
      if ( nNodePoly > 2 ){
	file << "polygon ";
	if ( facet( i ).rightMarker() < 0 ) file << "b " << abs( facet( i ).boundaryMarker() );
	else file << "r " << abs( facet( i ).rightMarker() );
	file << " r " << facet( i ).leftMarker() << "\t" << nNodePoly << "\t" ;
	for ( size_t k = 0; k < nNodePoly; k ++ ){
	  file << facet( i )[ j ][ k ]->id() << "\t";
	}
	file << endl;
      }
    }
  }

  file.close();
  return 1;
}

int Domain3D::saveTrianglePolyFile( const string & filename, bool checkValid){
  FUTILE
  fstream file; if ( !openOutFile( filename.substr( 0, filename.rfind( ".poly" ) ) + ".poly", & file ) ) return 0;

  int nverts = nodeCount();

  //  file << "# nverties dimension nattrib boolbndrymarker" << endl;
  file << nverts << "\t2\t0\t1" << endl;

  //file << "#    pointNr. x y z marker" << endl;
  file.setf( ios::scientific, ios::floatfield );
  file.precision( 14 );
  for ( int i = 0; i < nverts; i++ ){
    file << i
	 << "\t" << node( i ).x()
	 << "\t" << node( i ).y()
	 << "\t" << node( i ).marker() << endl;
  }

  //file << "# nfacets boolbndrymarker" << endl;
  size_t nPolyFacet = 0;
  size_t nNodesPoly = 0;

  size_t nEdges = 0;

  for ( size_t i = 0, imax = vecpFacets_.size(); i < imax; i ++ ){
    for ( size_t j = 0, jmax = facet( i ).size(); j < jmax; j++ ) {
      nNodesPoly = facet( i )[ j ].size();
      if ( nNodesPoly > 1 ){
 	if ( facet( i )[ j ][ 0 ]->id() != facet( i )[ j ][ 1 ]->id() ){
	  nEdges += nNodesPoly;
	}
      }
    }
  }

  file << nEdges << "\t1" << endl;

  size_t edgeCount = 0;
  for ( size_t i = 0, imax = vecpFacets_.size(); i < imax; i ++ ){
    for ( size_t j = 0, jmax = facet( i ).size(); j < jmax; j++ ) {
      nNodesPoly = facet( i )[ j ].size();
      if ( nNodesPoly > 1 ) {
	if ( facet( i )[ j ][ 0 ]->id() != facet( i )[ j ][ 1 ]->id() ){
	  for ( size_t k = 0, kmax = nNodesPoly -1; k < kmax; k++ ){
	    file << edgeCount << "\t" << facet( i )[ j ][ k ]->id() << "\t"
		 << facet( i )[ j ][ k + 1 ]->id() << "\t" << facet( i ).boundaryMarker() << endl;
	    edgeCount ++;
	  }
	  //** close the polygon;
	  file << edgeCount << "\t" << facet( i )[ j ][ nNodesPoly - 1 ]->id() << "\t"
	       << facet( i )[ j ][ 0 ]->id() << "\t" << facet( i ).boundaryMarker() << endl;
	  edgeCount ++;
	}
      }
    }
  }
  //file << "# nholes" << endl;
  file << vecpHoles_.size() << endl;
  //file << "#    hole x y z" << endl;
  for ( int i = 0, imax = vecpHoles_.size(); i < imax ; i ++ ){
    file << i << "\t"
	 << vecpHoles_[ i ]->x() << "\t"
	 << vecpHoles_[ i ]->y() << endl;
  }

  //file << "# nregions" << endl;
  file << regions_.size() << endl;
  //file << "#    region x y z attribute maxarea" << endl;
  for ( int i = 0, imax = regions_.size(); i < imax ; i ++ ){
    file << i << "\t"
 	 << regions_[ i ]->x() << "\t"
 	 << regions_[ i ]->y() << "\t"
 	 << regions_[ i ]->attribute() << "\t"
 	 << regions_[ i ]->dx() << endl;
  }
  file.close();

  return 1;
}


int Domain3D::saveTetgenNodeFile( const string & filename ){
  fstream file; openOutFile( filename, & file );
  file << nodeCount() << " 3 0  1" << endl;
  for ( int i = 0; i < nodeCount(); i ++ ){
    file << i << "\t"
	 << node( i ).pos()[ 0 ] << "\t"
	 << node( i ).pos()[ 1 ] << "\t"
	 << node( i ).pos()[ 2 ] << "\t"
	 << node(i).marker() << endl;

  }
  file.close();
  return 1;
}

int Domain3D::saveTetgenPolyFile( const string & filename, bool checkValid ){
  if ( checkValid ) checkAllFacets();

  fstream file; openOutFile( filename.substr( 0, filename.rfind( ".poly" ) ) + ".poly", & file );

  int nverts = nodeCount();
  int nfacets = vecpFacets_.size();

  //  file << "# nverties dimension nattrib boolbndrymarker" << endl;
  file << nverts << "\t3\t0\t1" << endl;

  //file << "#    pointNr. x y z marker" << endl;
  file.setf( ios::scientific, ios::floatfield );
  file.precision( 12 );
  for ( int i = 0; i < nverts; i++ ){
    file << i
	 << "\t" << node( i ).x( )
	 << "\t" << node( i ).y( )
	 << "\t" << node( i ).z( )
	 << "\t" << node( i ).marker() << endl;
  }

  //file << "# nfacets boolbndrymarker" << endl;
  size_t nPolyFacet = 0;
  size_t nNodesPoly = 0;
  file << nfacets << "\t1" << endl;
  for ( size_t i = 0, imax = nfacets;i < imax; i ++ ){

    nPolyFacet = facet( i ).size();
    file << nPolyFacet << "\t" << facet( i ).holeCount() << "\t" << facet( i ).boundaryMarker() << endl;

    for ( size_t j = 0, jmax = nPolyFacet; j < jmax; j++ ) {

      nNodesPoly = facet( i )[ j ].size();
      file << nNodesPoly << " \t";

      for ( size_t k = 0, kmax = nNodesPoly; k < kmax; k++ ){
	file << facet( i )[ j ][ k ]->id() << "\t";
      }
      file << endl;
    }
    for ( int j = 0, jmax = facet( i ).holeCount(); j < jmax; j ++ ){
      file << i << "\t"
	   << facet( i ).hole( j ).x() << "\t"
	   << facet( i ).hole( j ).y() << "\t"
	   << facet( i ).hole( j ).z() << endl;

    }
  }
  //file << "# nholes" << endl;
  file << vecpHoles_.size() << endl;
  //file << "#    hole x y z" << endl;
  for ( int i = 0, imax = vecpHoles_.size(); i < imax ; i ++ ){
    file << i << "\t"
	 << vecpHoles_[ i ]->x() << "\t"
	 << vecpHoles_[ i ]->y() << "\t"
	 << vecpHoles_[ i ]->z() << endl;
  }

  //file << "# nregions" << endl;
  file << regions_.size() << endl;
  //file << "#    region x y z attribute maxarea" << endl;
  for ( int i = 0, imax = regions_.size(); i < imax ; i ++ ){
    file << i << "\t"
 	 << regions_[ i ]->x() << "\t"
 	 << regions_[ i ]->y() << "\t"
       	 << regions_[ i ]->z() << "\t"
 	 << regions_[ i ]->attribute() << "\t"
 	 << regions_[ i ]->dx() << endl;
  }
  file.close();

  return 1;
}

int Domain3D::hackLoadTrianglePolyFile( const string & filename, int fromOne ){
  Domain2D inmesh; inmesh.load( filename );
  convertFromMesh2D( inmesh );

//   BaseMesh * inmesh = new Mesh2D();
//   BaseMesh * outmesh = new Mesh2D();

//   TriangleWrapper triangle( * inmesh, * outmesh);
//   triangle.loadPolyFile( filename );

//   inmesh->showInfos();



//   for ( int i = 0, imax = faceCount(); i < imax; i ++ ){

//   }

//   delete inmesh;
//   delete outmesh;

  return 1;
}

Polygon Domain3D::copyPolygon( const Polygon & poly ){
  Polygon p2;
  for ( size_t i = 0, imax = poly.size(); i < imax; i++ ){
    p2.push_back( this->createNode( poly[ i ]->pos(), nodeCount(), poly[ i ]->marker() ) );
  }
  return p2;
}

void Domain3D::stretchOut( const Polygon & front, double dx, double dy, double dz ){
  Polygon back = this->copyPolygon( front );
  for ( uint i = 0; i < back.size(); i ++ ){
    back[ i ]->setPos(back[ i ]->pos()+ RealPos( dx, dy, dz ) );
  }


  insertpFacet( new Facet( front ) );

  for ( uint i = 0; i < front.size()-1; i ++ ){
    insertpFacet( new Facet( front[ i ], front[ i+1 ], back[ i+1 ], back[ i ] ) );
  }
  insertpFacet( new Facet( front.back(), front.front(), back.front(), back.back()) );

  insertpFacet( new Facet( back ) );
}

// void createZylinder( Domain3D & domain, int segments ){

// }

Domain3D create3DWorld( const RealPos & size, int marker, double area ){
  Domain3D world;

  world.createSimpleMainDomain( RealPos( -size.x() / 2.0, -size.y() / 2.0, 0.0),
				RealPos( size.x() / 2.0, size.y() / 2.0, -size.z() ) );
  world.insertRegionMarker( RealPos( size.x() / 2.0, size.y() / 2.0, 0.0) - size/1000.0, marker, area );

  return world;
}

Domain3D createSphere( const RealPos & pos, int marker, double radius, double area, bool hole ){
  Domain3D sphereIn, sphere;
  sphereIn.loadWaveFrontObj("objects/sph_cube_sm3.obj");

  for ( int i = 0; i < sphereIn.nodeCount(); i ++ ) sphere.createNode( sphereIn.node( i ) );
  for ( int i = 0; i < sphereIn.facetCount(); i ++ ) {
    sphere.insertpFacet( new Facet( &sphere.node( sphereIn.facet( i )[0][0]->id() ),
				    &sphere.node( sphereIn.facet( i )[0][1]->id() ),
				    &sphere.node( sphereIn.facet( i )[0][2]->id() ),
					 1, 2, 1) );
    sphere.insertpFacet( new Facet( &sphere.node( sphereIn.facet( i )[0][2]->id() ),
				    &sphere.node( sphereIn.facet( i )[0][3]->id() ),
				    &sphere.node( sphereIn.facet( i )[0][0]->id() ),
					 1, 2, 1) );
  }
  //** sphere ist nicht rund
  //** rund machen und jeden Konten auf Radius normieren
  RealPos p0( 0.0, 0.0, 0.0), p1;
  for ( int i = 0; i < sphere.nodeCount(); i ++ ) {
    p1 = sphere.node( i ).pos();
    sphere.node( i ).setPos( Line( p0, p1).lineAt(  1.0 / p0.distance( p1 ) ) );
  }
  if ( hole ) {
    sphere.insertHole( RealPos( 0, 0, 0 ) );
  } else {
    sphere.insertRegionMarker( RealPos( 0.0, 0.0, 0.0 ), marker, area);
  }

  sphere.scale( radius, radius, radius );
  sphere.translate( pos );

  return sphere;
}

Domain3D createCube( const RealPos & pos, int marker, double area, bool hole, bool close ){
  Domain3D cube;

  int startFacetCount = cube.facetCount();

  Polygon front;
  front.push_back( cube.createNode( -0.5, -0.5, -0.5, cube.nodeCount(), 2 ) );
  front.push_back( cube.createNode( 0.5, -0.5, -0.5, cube.nodeCount(), 2 ) );
  front.push_back( cube.createNode( 0.5, -0.5, 0.5, cube.nodeCount(), 2 ) );
  front.push_back( cube.createNode( -0.5, -0.5, 0.5, cube.nodeCount(), 2 ) );
  Polygon back = extrude( cube, front, 0, 1, 0 );

  if ( hole ) {
    cube.insertHole( RealPos( 0, 0, 0 ) );
  } else {
    if ( marker != 0) cube.insertRegionMarker( RealPos( 0, 0, 0 ), marker, area ) ;
  }

  cube.closeFacets( front, back, marker, marker, marker, close );

  if ( !hole ){
    cube.facet( startFacetCount)[ 0 ].switchDirection();
    cube.facet( startFacetCount + 1 )[ 0 ].switchDirection();
    cube.facet( startFacetCount + 2 )[ 0 ].switchDirection();
    cube.facet( startFacetCount + 3 )[ 0 ].switchDirection();
    cube.facet( startFacetCount + 4 )[ 0 ].switchDirection();
    cube.facet( startFacetCount + 5 )[ 0 ].switchDirection();
  }

  cube.translate( pos );
  return cube;
}

Domain3D createCylinder( const RealPos & pos, int marker, int nSegments, double area, bool hole, bool close ){
  Domain3D cyl;

  double x = 0, y = 0, dphi = 0, phi = 0;
  dphi = 2 * PI_ / ( (double)nSegments );

  Polygon front;
  for ( int i = 0; i < nSegments; i++ ){
    x = 0.5 * cos( phi );
    y = 0.5 * sin( phi );

    if ( fabs( x ) < TOLERANCE ) x = 0.0;
    if ( fabs( y ) < TOLERANCE ) y = 0.0;

   front.push_back( cyl.createNode( pos.x() + x, pos.y() + y, pos.z() - 0.5, cyl.nodeCount(), 1 ) );

    phi = phi + dphi;
  }

  Polygon back = extrude( cyl, front, 0, 0, 1 );

  cyl.closeFacets( front, back, marker, 0, marker, close );

  if ( hole ){
    cyl.insertHole( RealPos( 0, 0, 0 ) );
  } else {
    if ( marker != 0 ) cyl.insertRegionMarker( pos, marker, area );
  }
  return cyl;
}

Polygon extrude( Domain3D & domain, Polygon & vertsFront, double xoff, double yoff, double zoff ){
  Polygon vertsExtrude;
  for ( unsigned int i = 0, imax = vertsFront.size(); i < imax; i++ ){
    vertsExtrude.push_back( domain.createNode( vertsFront[ i ]->x() + xoff,
					       vertsFront[ i ]->y() + yoff,
					       vertsFront[ i ]->z() + zoff,
					       domain.nodeCount(),
					       vertsFront[ i ]->marker()) );
  }
  return vertsExtrude;
}

void switchDirection( Polygon & polygone ){
  Polygon tmp( polygone );
  polygone.clear();
  for ( int i = 0, imax = tmp.size(); i < imax; i++ ){
    polygone.push_back( tmp[ imax - 1 - i ] );
  }
}

// vector< Facet > closeFacets( Polygon & front, Polygon & back, int left, int right, int marker, bool close){
//   vector < Facet > facets;
//   for ( int i = 0, imax = front.size() - 1; i < imax; i ++ ){
//     facets.push_back( Facet( front[ i ], front[ i + 1 ], back[ i + 1 ], back[ i ], left#, right, marker ) );
//   }

//   if ( close ){
//     facets.push_back( Facet( front[ 0 ], front[ front.size() - 1 ], back[ back.size() - 1 ], back[ 0 ], left, right, marker ) );
//   }

//   Polygon frontRev( front ); switchDirection( frontRev );
//   Polygon backRev( back ); switchDirection( backRev );

//   facets.push_back( Facet( frontRev, left, right, marker ) );
//   facets.push_back( Facet( backRev, left, right, marker ) );

//   return facets;
// }

void createCuboid( Domain3D & domain, int region, int regionleft, double dx, bool hole ){
  int startFacetCount = domain.facetCount();

  Polygon front;
  front.push_back( domain.createNode( -0.5, -0.5, -0.5, domain.nodeCount(), 2 ) );
  front.push_back( domain.createNode( 0.5, -0.5, -0.5, domain.nodeCount(), 2 ) );
  front.push_back( domain.createNode( 0.5, -0.5, 0.5, domain.nodeCount(), 2 ) );
  front.push_back( domain.createNode( -0.5, -0.5, 0.5, domain.nodeCount(), 2 ) );
  Polygon back = extrude( domain, front, 0, 1, 0 );

  if ( hole ) {
      domain.insertHole( RealPos( 0, 0, 0 ) );
  } else {
    if ( region != 0 ){
      domain.insertRegionMarker( RealPos( 0, 0, 0 ), region, dx );
    }
  }

  domain.closeFacets( front, back, regionleft, region, region, true );

  if ( !hole ){
    domain.facet( startFacetCount)[ 0 ].switchDirection();
    domain.facet( startFacetCount + 1 )[ 0 ].switchDirection();
    domain.facet( startFacetCount + 2 )[ 0 ].switchDirection();
    domain.facet( startFacetCount + 3 )[ 0 ].switchDirection();
    domain.facet( startFacetCount + 4 )[ 0 ].switchDirection();
    domain.facet( startFacetCount + 5 )[ 0 ].switchDirection();
  }
}

void createZylinder( Domain3D & domain, int segments, int left, int right, int region, double dx  ){
  RealPos pos( 0.0, 0.0, 0.0 );
  domain.setPos( pos );

  double x = 0, y = 0, dphi = 0, phi = 0;
  dphi = 2 * PI_ / ( (double)segments );

  Polygon front;
  for ( int i = 0; i < segments; i++ ){
    x = 0.5 * cos( phi );
    y = 0.5 * sin( phi );

    if ( fabs( x ) < TOLERANCE ) x = 0.0;
    if ( fabs( y ) < TOLERANCE ) y = 0.0;

   front.push_back( domain.createNode( pos.x() + x, pos.y() + y, pos.z() - 0.5, domain.nodeCount(), 1 ) );

    phi = phi + dphi;
  }

  Polygon back = extrude( domain, front, 0, 0, 1 );

  domain.closeFacets( front, back, left, right, left, true );
  if ( region != 0 ){
    domain.insertRegionMarker( pos, region, dx );
  }
}


/*
$Log: domain3d.cpp,v $
Revision 1.39  2011/01/24 11:13:05  thomas
added -O option into polyCreateCube
changed some example files (3dtank and acucar)

Revision 1.38  2010/07/28 16:54:27  carsten
Fixed small tol problem

Revision 1.37  2010/02/08 17:38:33  carsten
*** empty log message ***

Revision 1.36  2010/01/31 10:49:54  carsten
*** empty log message ***

Revision 1.35  2009/02/03 10:15:00  carsten
*** empty log message ***

Revision 1.34  2009/02/02 10:43:04  carsten
*** empty log message ***

Revision 1.33  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.32  2008/02/28 10:47:33  carsten
*** empty log message ***

Revision 1.31  2008/02/11 11:16:32  carsten
*** empty log message ***

Revision 1.30  2007/11/13 16:12:18  carsten
*** empty log message ***

Revision 1.28  2007/09/18 18:46:22  carsten
*** empty log message ***

Revision 1.27  2007/09/12 21:31:55  carsten
*** empty log message ***

Revision 1.26  2007/09/11 17:28:58  carsten
*** empty log message ***

Revision 1.25  2007/04/04 16:53:54  carsten
*** empty log message ***

Revision 1.24  2007/03/13 17:32:51  carsten
*** empty log message ***

Revision 1.23  2007/02/21 19:44:05  carsten
*** empty log message ***

Revision 1.22  2006/12/04 13:32:09  carsten
*** empty log message ***

Revision 1.21  2006/12/03 18:46:03  carsten
*** empty log message ***

Revision 1.20  2006/07/28 16:24:51  carsten
*** empty log message ***

Revision 1.19  2006/06/06 09:41:49  carsten
*** empty log message ***

Revision 1.18  2006/05/08 20:23:06  carsten
*** empty log message ***

Revision 1.17  2006/05/08 16:48:23  carsten
*** empty log message ***

Revision 1.15  2006/04/17 20:58:03  carsten
*** empty log message ***

Revision 1.14  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.13  2005/08/22 17:28:09  carsten
*** empty log message ***

Revision 1.12  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.11  2005/07/14 15:03:37  carsten
*** empty log message ***

Revision 1.10  2005/07/01 15:11:28  carsten
*** empty log message ***

Revision 1.9  2005/06/21 14:53:59  carsten
*** empty log message ***

Revision 1.8  2005/04/26 10:44:16  carsten
*** empty log message ***

Revision 1.7  2005/03/29 16:57:46  carsten
add inversion tools

Revision 1.6  2005/03/20 22:51:59  carsten
*** empty log message ***

Revision 1.5  2005/03/10 16:04:38  carsten
*** empty log message ***

Revision 1.4  2005/03/08 16:04:45  carsten
*** empty log message ***

Revision 1.3  2005/02/22 21:51:43  carsten
*** empty log message ***

Revision 1.2  2005/02/16 19:36:38  carsten
*** empty log message ***

Revision 1.1  2005/02/07 13:33:13  carsten
*** empty log message ***

*/
