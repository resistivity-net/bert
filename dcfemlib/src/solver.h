// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef SOLVER__H
#define SOLVER__H SOLVER__H

#include "dcfemlib.h"
using namespace DCFEMLib;
using namespace MyVec;

#include "myvec/vector.h"
#include "myvec/matrix.h"

template < class Matrix, class Vector > int solveLU( const Matrix & A_In, Vector & x, const Vector & b_In);

class SolverWrapper{
public:
  SolverWrapper( ){ dummy_ = true; }
  SolverWrapper( const RDirectMatrix & S, bool verbose = false )
    : verbose_( verbose ) {
    dummy_ = false; 
    dim_ = S.size(); 
    nVals_ = S.valueCount();
    tolerance_ = 1e-12;
    dropTol_ = 0.0;
  }
  virtual ~SolverWrapper( ){ }
  virtual int solve( const RVector & rhs, RVector & solution ) = 0;
protected:
  bool dummy_;
  bool verbose_;
  int dim_;
  long nVals_;
  double dropTol_;
  double tolerance_;
  double maxiter_;
};

class PCGWrapper : public SolverWrapper{
public:
  PCGWrapper(){ dummy_ = true; }
  PCGWrapper( RDirectMatrix & S, double epsilon, bool verbose = false )
    : SolverWrapper( S, verbose ) { 
    tolerance_ = epsilon;
    S_ = &S; 
  }

  ~PCGWrapper(){}
  int solve( const RVector & b, RVector & x ){
    int maxiter = b.size() * 10;
    
    if ( x.size() != b.size() ) {
      x.resize( b.size() );
      x.clear();
    }

    int dim = x.size();

    RVector r( b - *S_ * x ); //r *= -1;
    RVector z( dim );
    RVector d( dim );
    RVector w( dim );
    double inner1 = 1.0, inner2 = 1.0; 
    double alpha = 0.0, beta = 0.0, denom = 0.0, error = 0.0;
    
    // % Precondition 
    RVector Mdiag( dim );
    for ( int i = 0, imax = dim; i < imax; i++ ) {
      Mdiag[ i ] = S_->GetVal( i + 1, i + 1 );
      if ( Mdiag[ i ] == 0.0 ) {
	cerr << WHERE_AM_I << "Error! S-diag-element = 0.0 at Value forced to 1.0: " << i << endl; 
	S_->SetVal( i + 1, i + 1, 1.0 );
	Mdiag[ i ] = 1.0;
      }
    }
    for ( int i = 0, imax = dim; i < imax; i++ ) z[ i ] = r[ i ] / Mdiag[ i ]; //  z = M\r; 
    
    //double errorStart = norm( z );
    double errorStart = 1.0;
    
    //  Mdiag.save("Mdiag.vec");
    //  Mdiag.show();
    //  z.show();
    
    inner1 = dot( r, z ); 
    inner2 = 0; 
    d = z;
    //  cout << "Inner1/2: " << inner1 << "\t" << inner2 << endl;
    
    //** start conjugate gradient iteration
      for ( int k = 1, kmax = maxiter; k < kmax; k++ ){
	if ( k > 1 ){
	  beta = inner1/inner2;
	  d = z + beta * d;
	}
	w = *S_ * d; 
	denom = dot(d, w);
	if ( denom <= 0 ){
	  x = d / norm( d ); //% Direction of negative/zero curvature
	  cerr << WHERE_AM_I << " Exit zero/negative curvature detected " << endl;
	  //      break;
	} else {
	  alpha = inner1 / denom;
	  x += alpha * d;
	  r -= alpha * w;
	}
	for ( int i = 0, imax = dim; i < imax; i++ ) z[ i ] = r[ i ] / Mdiag[ i ]; //  z = M\r; 
	//cout << beta << "\t" << inner1 << "\t" << inner2 << "\t" << z.norm() << endl;
	error = norm( z );
	if ( error < ( errorStart * tolerance_ ) ) break; 
	if ( verbose_ ) cout << "\r[" << k << "\t" << error << "]"; //% Exit if Hp=-g solved within tolerance
	//     if ( error < tol ) break; 
	//     if ( verbose_ ) cout << "\r[" << k << "\t" << z.norm() << "]"; //% Exit if Hp=-g solved within tolerance
	
	inner2 = inner1;
	inner1 = dot(r, z);
      }
      if ( verbose_ ) cout << endl;
      return 1;
  }

protected:
  RDirectMatrix * S_;

};

template< class Vector > void copyVec( Vector & toFill, const Vector & in, double val ){
  for ( int i = 0, imax = in.size(); i < imax; i ++ ){
    toFill[ i ] = in[ i ];
  }
  toFill[ in.size() ] = val;
}

// void solveCG( RDirectMatrix & S, RVector & x, const RVector & b,
// 	     double epsilon, int maxiter, bool verbose ); 
//void solvePCG( RDirectMatrix & S, RVector & x, const RVector & b, double epsilon, int maxiter, bool verbose ); 

template< class Matrix , class Vector >
int solveGaussDirect( Matrix & A_in, Vector & x, const Vector & b_in){
  FUTILE
  return solveLU( A_in, x, b_in );
  //cout << WHERE_AM_I << "variablen tauchen" << endl;;
  // return 1; solving ok
  // return -1; not enough linear independend vectors

  //cout << A_in << endl;
  //  cout << x << endl;
  //  cout << b_in << endl;

  size_t dim = x.size();
  size_t nRows = A_in.dim1();
  size_t nColumns = A_in.dim2();

  Matrix A( dim, dim );
  Vector b( dim );
  bool linDependend = false;
  if ( A_in.dim1() > dim ){ // if more rows than columns
    Vector test( dim + 1 ), test1( dim + 1 );
    
    int startRow = 0;
    for ( size_t i = 0; i < nRows; i ++ ){
      if ( !isZero( A_in[ i ] ) ){
	A[ 0 ] = A_in[ i ]; b[ 0 ] = b_in[ i ];
	startRow = i;
	break;
      }
    }
    //** fill A with linear independend and noZero vectors of A_in and b_in;
    for ( size_t i = startRow + 1, row = 1; row < dim; i ++ ){
      if ( i == A_in.dim1() ) return -1; // not enough linear independend vectors

      if ( !isZero( A_in[ i ] ) ){
	copyVec( test1, A_in[ i ], b_in[ i ] );
	linDependend = false;
	
	//** test the current row with all allready filled on linearly independence;
	for ( int j = row - 1; j > -1; j -- ){
	  copyVec( test, A[ j ], b[ j ] );
	  if ( linearlyDependend( test, test1 ) ){
	    linDependend = true; 
	    break;
	  } 
	}
	if ( !linDependend ) {
	  A[ row ] = A_in[ i ]; 
	  b[ row ] = b_in[ i ];
	  row ++;
	}
      }
    }
  } else {
    A = A_in;
    b = b_in;
  }
  
  //  cout << A << endl;
  //  cout << b << endl;

  Matrix R( dim, dim );
  double tmp = 0.0;  

  for ( int p = 0, pmax = dim; p < pmax; p++ ){
    //** Pivotisierung
    if ( fabs( A[ p ][ p ] ) < 1e-15 ){
      //** Zeilentausch
      for ( int i = p + 1, imax = dim; i < imax; i++ ){	
	if ( fabs( A[ i ][ p ] ) > 1e-15 ){
	  for ( int j = p, jmax = dim; j < jmax; j++ ){
	    tmp = A[ i ][ j ];
	    A[ i ][ j ] = A[ p ][ j ];
	    A[ p ][ j ] = tmp;
	  }	
	  tmp = b[ i ];
	  b[ i ] = b[ p ];
	  b[ p ] = tmp;
	  i = imax;
	}
      }
    }
    cout << A <<  endl;
    for ( int i = p + 1, imax = dim; i < imax; i++ ){	
      R[ i ][ p ] = A[ i ][ p ] / A[ p ][ p ]; 
      for ( int j = p, jmax = dim; j < jmax; j++ ){	
        A[ i ][ j ] -= R[ i ][ p ] * A[ p ][ j ];
      }
      b[ i ] -= R[ i ][ p ] * b[ p ];
      //  cout << b[i] << endl;
    }	
  }
  
  //** Backward substitution;
  for ( int i = dim - 1, imin = -1; i > imin; i -- ){
    tmp = 0.0;
    for ( int j = dim - 1, jmin = i; j > jmin; j -- ){
      tmp += x[ j ] * A[ i ][ j ];
    }
    x[ i ] = ( b[ i ] - tmp ) / A[ i ][ i ];
  }

//   if ( sum( A * x -b ) > 1e-10 ){
//     cerr << "sum( A * x -b ) " << sum( A * x -b ) << endl; 
//     cout << A_in << b_in << x << endl;
//     cout << A << b << x << endl;
//     exit(0);
//   }
  return 1;
}

/*! Schwarz, H.R.: Methode der Finiten Elemente. Teubner Studienbuecher Mathematik 1991. S.210 */
template< class Matrix , class Vector >
int solveCholeskyDirect( Matrix & A, Vector & x, const Vector & b ){
  cout << "!!!!!!!! Auf jeden FALL erst testen. !!!!!!!!!!!!!!" << endl;
//  cout << WHERE_AM_I << "variablen tauschen" << endl;
  Matrix L( b.size(), b.size() );
  x = 0.0;

  //** Decomposition
  for ( int p = 0, pmax = b.size(); p < pmax; p ++ ){
    L[ p ][ p ] = sqrt( A[ p ][ p ] );
    for ( int i = p + 1, imax = b.size(); i < imax; i ++ ){
      L[ i ][ p ] = A[ i ][ p ] / L[ p ][ p ];
      for ( int k = p + 1, kmax = i; k < kmax; k ++ ){
	cout << p << "\t" << i << "\t" << k << endl;
	A[ i ][ k ] = A[ i ][ k ] - L[ i ][ p ] * L[ k ][ p ];
      }
    }
  }
//   cout << A << endl;
//   cout << L << endl;

  //** Forward substitution;
  Vector c( b.size() );
  double s = 0.0;
  for ( int k = 0, kmax = b.size(); k < kmax; k ++ ){
    s = b[ k ];
    for ( int i = 0, imax = k - 1; i < imax; i ++ ){
      s = s - L[ k ][ i ] * c[ i ];
    }
    c[ k ] = s/L[ k ][ k ];
  }


  //cout << c << endl;

  //** Backward substitution
  for ( int k = b.size() - 1, kmin = -1; k > kmin; k -- ){
    s = c[ k ];
    for ( int i = k, imax = b.size(); i < imax; i ++ ){
      s = s + L[ i ][ k ] * x[ i ];
    }
    x[ k ] = -s / L[ k ][ k ];
  }

  return 1;
}

template < class Matrix, class Vector, class Preconditioner >
int t_solveCG( const Matrix & A, Vector & x, const Vector & b, const Preconditioner & P, 
	       int & maxIter, double & tol, bool verbose ){
// % Initializations source Matlab optimazation toolbox  H*p= -g;
// g = b;
// H = S;
// p = x;

//  S.scale();
//  S.save("test.matrix");

  int dim = b.size();
  if ( x.size() != dim ) x.resize( dim ); 
  x.clear();
  

  Vector r( b ); //r *= -1;
  Vector z( x );
  Vector d( x );
  Vector w( x );
  double inner1 = 1.0, inner2 = 1.0; 
  double alpha = 0.0, beta = 0.0, denom = 0.0, error = 0.0;

  // % Precondition 
  // RVector Mdiag( dim );
//   for ( int i = 0, imax = dim; i < imax; i++ ) {
//     Mdiag[ i ] = S.GetVal( i + 1, i + 1 );
//     if ( Mdiag[ i ] == 0.0 ) cerr << WHERE_AM_I << "Error! S-diag-element = 0.0 at " << i << endl; 
//   }
//   for ( int i = 0, imax = dim; i < imax; i++ ) z[ i ] = r[ i ] / Mdiag[ i ]; //  z = M\r; 
  z = P.solve( r );
  
  //  Mdiag.save("Mdiag.vec");
  //  Mdiag.show();
  //  z.show();

  inner1 = r * z; 
  inner2 = 0; 
  d = z;
  //  cout << "Inner1/2: " << inner1 << "\t" << inner2 << endl;

  //** start conjugate gradient iteration
  for ( int k = 1, kmax = maxIter; k < kmax; k++){
    if ( k > 1 ){
      beta = inner1/inner2;
      d = z + beta * d;
    }
    w = A * d; 
    denom = d * w;
    if ( denom <= 0 ){
      x = d * ( 1 / norm( d ) ); //% Direction of negative/zero curvature
      cerr << WHERE_AM_I << " Exit zero/negative curvature detected " << endl;
      break;
    } else {
      alpha = inner1 / denom;
      x += alpha * d;
      r -= alpha * w;
    }
    z = P.solve( r );
    //for ( int i = 0, imax = dim; i < imax; i++ ) z[ i ] = r[ i ] / Mdiag[ i ]; //  z = M\r; 
    //cout << beta << "\t" << inner1 << "\t" << inner2 << "\t" << z.norm() << endl;
    error = norm( z );
    if ( error < tol ) break; 
    if ( verbose ) cout << "\r[" << k << "\t" << norm( z ) << "]"; //% Exit if Hp=-g solved within tolerance
//     if ( error < tol ) break; 
//     if ( verbose ) cout << "\r[" << k << "\t" << z.norm() << "]"; //% Exit if Hp=-g solved within tolerance
    
    inner2 = inner1;
    inner1 = r * z;
  }
  if ( verbose ) cout << endl;

  return 1;
}

template < class Matrix, class ConstrMatrix, class Vector > 
int solveCGLSCDW( const Matrix & S, const ConstrMatrix & C, const Vector & dWeight, 
		  const Vector & b, Vector & x, const Vector & wc, 
		  double lambda, const Vector & deltaX, bool verbose = false){

  int nData = b.size();
  int nModel = x.size();

  //  if ( nData != S.size() ) cerr << "Data-dimension differ SensM" << nData << " / " << S.size() << endl;
  //  if ( nModel != S[ 0 ].size() ) cerr << "Modell-dimension differ SensM" << nModel << " / " << S[ 0 ].size() << endl;
  // if ( nModel != C.size() ) cerr << "Modell-dimension differ ContraintsM (critcal)" << nModel << " / " << C.size() << endl;

  Vector cdx( transMult( C, wc * wc * (C * deltaX) * lambda ) );
  Vector z( ( b - S * x ) * dWeight);
  Vector p( transMult( S, z * dWeight ) - cdx - transMult( C, wc * wc * ( C * x ) * lambda ) );
  Vector r( transMult( S, b * dWeight * dWeight ) - cdx );

  double accuracy = 1e-8 * dot( r, r );
  r = p;  

  double normR2 = dot( r, r ), normR2old = 0.0;
  double alpha = 0.0, beta = 0.0;

  int count = 0;
  int maxcount = 200;
  Vector q( nData );
  Vector wcp( C.size() );

  while( count < maxcount && normR2 > accuracy ){

    count ++;
    q = S * p * dWeight;
    wcp = wc * (C * p);
    alpha = normR2 / ( dot( q, q ) + lambda * dot( wcp, wcp ) );
    x += p * alpha;
    if( (count % 10) == -1 ) { // TOM
      z = b - S*x; // z exakt durch extra Mult.
      z *= dWeight;
    } else {
      z -= q * alpha;
    }
    r = transMult( S, z * dWeight ) - transMult( C, wc * wc * (C * x) ) * lambda - cdx;

    normR2old = normR2;
    normR2 = dot( r, r );
    beta = normR2 / normR2old; 
    p = r + p * beta;
    if ( verbose )  cout << "\r[ " << count << "/" << normR2 << "]\t";
  }
  if ( verbose ) cout << endl;
  return 1;
}

template < class Matrix, class SparseMatrix, class Vector > 
int solveCGLSCD( const Matrix & S, const SparseMatrix & C, const Vector & dWeight, 
		 const Vector & b, Vector & x, double lambda, const Vector & deltaX, bool verbose = false);

template < class Matrix, class SparseMatrix, class Vector > 
int solveCGLSC( const Matrix & A, const SparseMatrix & C, const Vector & b, Vector & x, double lambda, bool verbose){
  Vector dWeight( b.size(), 1.0 );
  return solveCGLSCD( A, C, dWeight, b, x, lambda, verbose);
}

template < class Matrix, class SparseMatrix, class Vector > 
int solveCGLSCD( const Matrix & S, const SparseMatrix & C, const Vector & dWeight, 
		 const Vector & b, Vector & x, double lambda, bool verbose){
  Vector deltaX( x.size() );
  return solveCGLSCD( S, C, dWeight, b, x, lambda, deltaX, verbose);
}

template < class Matrix, class SparseMatrix, class Vector > 
int solveCGLSCD( const Matrix & S, const SparseMatrix & C, const Vector & dWeight, 
		 const Vector & b, Vector & x, double lambda, const Vector & deltaX, bool verbose ){

  int nData = b.size();
  int nModel = x.size();

  //  if ( nData != S.size() ) cerr << "Data-dimension differ SensM" << nData << " / " << S.size() << endl;
  //  if ( nModel != S[ 0 ].size() ) cerr << "Modell-dimension differ SensM" << nModel << " / " << S[ 0 ].size() << endl;
  if ( nModel != C.size() ) cerr << "Modell-dimension differ ContraintsM (critcal)" << nModel << " / " << C.size() << endl;

  Vector cdx( C * deltaX * lambda );
  Vector z( ( b - S * x ) * dWeight);
  Vector p( transMult( S, z * dWeight ) - cdx - (C * x) * lambda );
  Vector r( transMult( S, b * dWeight * dWeight ) - cdx );

  double accuracy = 1e-8 * dot( r, r );
  r = p;  

  double normR2 = dot( r, r ), normR2old = 0.0;

  double alpha = 0.0, beta = 0.0;

  int count = 0;
  int maxcount = 200;
  Vector q( nData );

  while( count < maxcount && normR2 > accuracy ){

    count ++;
    q = S * p * dWeight;

    alpha = normR2 / ( dot( q, q ) + lambda * dot( p, C * p ) );
    x += p * alpha;

    if( (count % 10) == -1 ) { // TOM
      z = b - S*x; // z exakt durch extra Mult.
      z *= dWeight;
    } else {     z -= q * alpha;
    }
    r = transMult( S, z * dWeight ) - C * x * lambda - cdx;

    normR2old = normR2;
    normR2 = dot( r, r );
    beta = normR2 / normR2old; 
    p = r + p * beta;
    if ( verbose )  cout << "\r[ " << count << "/" << normR2 << "]\t";
  }
  if ( verbose ) cout << endl;
  return 1;
}

template < class Matrix, class SparseMatrix, class Vector > 
int solveCGLSCCD( const Matrix & S, const SparseMatrix & C, const Vector & dWeight, 
		 const Vector & b, Vector & x, double lambda, bool verbose = false){
  //solveCGLSCD( sensitivityMatrix, constraintMatrix, dWeight, deltaData, deltaModel, lambda, true );  

  int nData = b.size();
  int nModel = x.size();

//   if ( nData != S.size() ) cerr << "Data-dimension differ SensM" << nData << " / " << S.size() << endl;
//   if ( nModel != S[ 0 ].size() ) cerr << "Modell-dimension differ" << nModel << " / " << S[ 0 ].size() << endl;
  if ( nModel != C.size() ) cerr << "Modell-dimension differ ContraintsM (critcal)" << nModel << " / " << C.size() << endl;

  Vector cdx( C * (C * x) *lambda);
  x.clear();
  
  Vector z( b - S * x );
  z*=dWeight;
  Vector p( transMult( S, z * dWeight ) - cdx );
  Vector r( transMult( S, b * dWeight ) );
  r = p;
  Vector q( nData );
  double normR2 = dot( r, r ), normR2old = 0.0;

  double accuracy = 1e-8 * normR2;
  double alpha = 0.0, beta = 0.0;

  int count = 0;
  int maxcount = 200;
  vector< double > residuum; // TOM
  while( count < maxcount && normR2 > accuracy ){
    count ++;
    q = S * p;
    q *= dWeight;

    alpha = normR2 / ( dot( q, q ) + lambda * dot( C * p, C * p ) );
    x += p * alpha;
    if( (count % 30) == -1 ) { // TOM
      z = b - S * x; // z exakt durch extra Mult.
      z *= dWeight;
    } else {
      z -= q * alpha;
    }
    r = transMult( S, z * dWeight ) - C * (C * x) * lambda - cdx;

    normR2old = normR2;
    normR2 = dot( r, r );
    beta = normR2 / normR2old; 
    p = r + p * beta;
    residuum.push_back( normR2 ); //TOM
    if ( verbose )  cout << "\r[ " << count << "/" << normR2 << "]        ";
  }
  if ( verbose ) cout << endl;
  MyVec::save( residuum, "cglsccd.log" ); //TOM
  return 1;
}

template < class Matrix, class Vector > int solveLU( const Matrix & A_In, Vector & x, const Vector & b_In){
  //** from TETGEN 

  Matrix lu(A_In);
  Vector b(b_In);
  int N = 0, n = b.size();

  int ps[ n ];

  double scales[ n ];
  double pivot, biggest, mult, tempf;
  int pivotindex = 0;
  int i, j, k;

  for (i = N; i < n + N; i++) {                             // For each row.
    // Find the largest element in each row for row equilibration
    biggest = 0.0;
    for (j = N; j < n + N; j++)
      if (biggest < (tempf = fabs(lu[i][j])))
        biggest  = tempf;
    if (biggest != 0.0)
      scales[i] = 1.0 / biggest;
    else {
      scales[i] = 0.0;
      cerr << WHERE_AM_I << " Zero row: singular matrix" << endl;
      return false;                            // Zero row: singular matrix.
    }
    ps[i] = i;                                 // Initialize pivot sequence.
  }

  for (k = N; k < n + N - 1; k++) {                      // For each column.
    // Find the largest element in each column to pivot around.
    biggest = 0.0;
    for (i = k; i < n + N; i++) {
      if (biggest < (tempf = fabs(lu[ps[i]][k]) * scales[ps[i]])) {
        biggest = tempf;
        pivotindex = i;
      }
    }
    if (biggest == 0.0) {
      cerr << WHERE_AM_I << " Zero column: singular matrix" << endl;
      return false;                         // Zero column: singular matrix.
    }
    if (pivotindex != k) {                         // Update pivot sequence.
      j = ps[k];
      ps[k] = ps[pivotindex];
      ps[pivotindex] = j;
      //      *d = -(*d);                          // ...and change the parity of d.
    }

    // Pivot, eliminating an extra variable  each time
    pivot = lu[ps[k]][k];
    for (i = k + 1; i < n + N; i++) {
      lu[ps[i]][k] = mult = lu[ps[i]][k] / pivot;
      if (mult != 0.0) {
        for (j = k + 1; j < n + N; j++)
          lu[ps[i]][j] -= mult * lu[ps[k]][j];
      }
    }
  }

  double dot;

  // Vector reduction using U triangular matrix.
  for (i = N; i < n + N; i++) {
    dot = 0.0;
    for (j = N; j < i + N; j++)
      dot += lu[ps[i]][j] * x[j];
    x[i] = b[ps[i]] - dot;
  }

  // Back substitution, in L triangular matrix.
  for (i = n + N - 1; i >= N; i--) {
    dot = 0.0;
    for (j = i + 1; j < n + N; j++)
      dot += lu[ps[i]][j] * x[j];
    x[i] = (x[i] - dot) / lu[ps[i]][i];
  }
  
  if ( rms( (const Vector)( A_In * x - b_In) ) > 1e-9 ){
    cerr << "rms( A * x -b ) " << rms( (const Vector)(A_In * x - b_In) ) << endl; 
    cout << A_In * x - b_In << endl;
    cout << A_In << b_In << x << endl;
    return -1;
  }
  return 1;
}

template < class Matrix > double detLU( const Matrix & A ){
  //** from TETGEN 

  Matrix lu( A );
  cout << lu << endl;
  int N = 0, n = lu.dim1();

  int ps[ n ];
  double scales[ n ];
  double pivot, biggest, mult, tempf;
  int pivotindex = 0;
  int i, j, k;

  for ( i = N; i < n + N; i++ ) {                             // For each row.
    // Find the largest element in each row for row equilibration
    biggest = 0.0;
    for ( j = N; j < n + N; j++ )
      if ( biggest < ( tempf = fabs( lu[ i ][ j ] ) ) ) biggest  = tempf;
    if ( biggest != 0.0 ){
      scales[ i ] = 1.0 / biggest;
    } else {
      cerr << WHERE_AM_I << " row " << i << "only contains = 0.0 " << endl;
      cerr << lu << endl;
      scales[ i ] = 0.0;
      return 0.0;                            // Zero row: singular matrix.
    }
    ps[ i ] = i;                                 // Initialize pivot sequence.
  }

  for ( k = N; k < n + N - 1; k++ ) {                      // For each column.
    // Find the largest element in each column to pivot around.
    biggest = 0.0;
    for ( i = k; i < n + N; i++ ) {
      if ( biggest < (tempf = fabs( lu[ ps[ i ] ][ k ] ) * scales[ ps[ i ] ] ) ) {
        biggest = tempf;
      	pivotindex = i;
      }
    }
    if ( biggest == 0.0 ) {
      return false;                         // Zero column: singular matrix.
    }
    if ( pivotindex != k ) {                         // Update pivot sequence.
      j = ps[ k ];
      ps[ k ] = ps[ pivotindex ];
      ps[ pivotindex ] = j;
      //      *d = -(*d);                          // ...and change the parity of d.
    }

    // Pivot, eliminating an extra variable  each time
    pivot = lu[ ps[ k ] ][ k ];
    for (i = k + 1; i < n + N; i++) {
      lu[ps[i]][k] = mult = lu[ps[i]][k] / pivot;
      if (mult != 0.0) {
        for (j = k + 1; j < n + N; j++)
          lu[ ps[ i ] ][ j ] -= mult * lu[ps[k]][j];
      }
    }
  }

  double det = lu[ ps[ 0 ] ][ 0 ];
  for ( int i = 1; i < n; i++ ){
    det *= lu[ ps[ i ] ][ i ];
  }

  return det;
}

template < class Matrix > double detLU_test( const Matrix & A ){

  cout << A << endl;

  //int gaussp( int n, double **a, double *b, double *x, int *p ) {

  int n = A.row();
  
  Matrix LU( A );
  int p[ n ];

  double pivot, pmax, ptemp, zmult, sum;
  int    i, j, k, m, pi, pk, itemp;

  for ( int i = 0; i < n; i++ ) p[ i ] = i;

  for ( int i = 0; i < n - 1; i++ ) {
    m = i;
    pmax = 0.0;
    // suche max|a[j][i]| unterhalb 
    for ( int j = i; j < n; j++ ) {
      ptemp = fabs( LU[ p[ j ] ][ i ] ); // der Hauptdiagonale in Spalte i 
      if ( ptemp > pmax ) {
	pmax = ptemp;
	m = j;                 // Zeilenindex f�r neue Pivotzeile
      }
    }
    if ( pmax <= TOLERANCE ) {          // Matrix nahezu singul�r
      return 0.0;
    }
    
    if ( m != i ) {               // Zeile i mit Zeile m vertauschen 
      itemp = p[ i ];
      p[ i ] = p[ m ];
      p[ m ] = itemp;
    }

    /*  Eliminationsschritte f�r verbleibende Zeilen k = i+1,...,n    */
    
    pi = p[ i ];
    pivot = LU[ pi ][ i ];

    cout << i << endl;
    cout << LU << endl; 
    for ( int k = i + 1; k < n; k++ ) {
      pk = p[ k ];
      cout << k << " " << pk  << " " << i << " " << p[ k ] << endl;
      zmult = LU[ pk ][ i ] / pivot;

      //  Neue Elemente in den Spalten j der Zeile k 
      for ( int j = i + 1; j < n; j++ ) {
	cout << j << " " << pk << " " << pi << endl;
	LU[ pk ][ j ] = LU[ pk ][ j ] - zmult * LU[ pi ][ j ];

      }
      
      //      b[pk] = b[pk] - zmult * b[ pi ];
    }
  }
 
 cout << LU << endl; 
 double det = LU[ p[ 0 ] ][ 0 ];
 for ( int i = 1; i < n; i++ ) {
   det *= LU[ p[ i ] ][ i ];
 }


/*  R�ckw�rtseinsetzen  */

 
//  x[n] = b[p[n]] / a[p[n]][n];
//    for (i = n - 1; i >= 1; i--) {

//         pi = p[i];
//         sum = b[pi];

//         for (j = i + 1; j <= n; j++)
//             sum = sum - a[pi][j]*x[j];

//         x[i] = sum/a[pi][i];
//     }

//     return(0);


}
#endif // SOLVER__H


/*
$Log: solver.h,v $
Revision 1.27  2011/05/10 16:36:58  thomas
1.2.4 release with updated configure

Revision 1.26  2007/08/30 18:00:40  thomas
*** empty log message ***

Revision 1.25  2006/09/01 18:34:09  carsten
*** empty log message ***

Revision 1.24  2006/08/27 21:52:31  carsten
*** empty log message ***

Revision 1.23  2006/08/22 14:48:46  thomas
robustdata, constraint 10

Revision 1.22  2006/08/02 18:24:19  carsten
*** empty log message ***

Revision 1.21  2006/07/27 15:49:50  carsten
*** empty log message ***

Revision 1.20  2006/07/27 14:52:08  carsten
*** empty log message ***

Revision 1.19  2006/07/16 20:00:03  carsten
*** empty log message ***

Revision 1.18  2006/05/29 19:42:04  carsten
*** empty log message ***

Revision 1.17  2006/05/08 16:25:59  carsten
*** empty log message ***

Revision 1.16  2006/02/14 11:09:18  carsten
*** empty log message ***

Revision 1.15  2006/02/07 15:17:22  thomas
new constraint formulation - first steps

Revision 1.14  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.13  2005/06/22 17:36:59  carsten
*** empty log message ***

Revision 1.12  2005/06/02 15:44:10  carsten
*** empty log message ***

Revision 1.11  2005/02/22 21:51:43  carsten
*** empty log message ***

Revision 1.10  2005/02/15 14:38:03  carsten
*** empty log message ***

Revision 1.9  2005/02/15 11:52:43  carsten
*** empty log message ***

Revision 1.7  2005/02/15 11:26:49  carsten
*** empty log message ***

Revision 1.6  2005/02/15 11:24:28  carsten
*** empty log message ***

Revision 1.4  2005/02/10 14:07:59  carsten
*** empty log message ***

Revision 1.3  2005/01/13 20:10:42  carsten
*** empty log message ***

Revision 1.2  2005/01/11 19:12:26  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
