// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef STLMATRIX__H
#define STLMATRIX__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include <valarray>
#include <iostream>

using namespace std;

namespace MyVec{

struct linSparseMatElement{
  int row;
  int col;
  float val;
};

class  LinSparseVector : public vector < linSparseMatElement >{
public:
  LinSparseVector( int row, int col ) : rows_( row ), cols_( col ){
  }

  int rows() const { return rows_; }
  int cols() const { return cols_; }
  int columns() const { return cols_; }
  void setRows( int rows ){ rows_ = rows; }
  void setCols( int cols ){ cols_ = cols; }

  void setVal( int r, int c, float val ){ 
    el_.row=r; el_.col=c; el_.val=val; this->push_back( el_ );
  }

  double memSize() const { return size() * sizeof( linSparseMatElement ); }
  DLLEXPORT int save( const string & fname );
  DLLEXPORT int save( const string & fname, IOFormat format );
  DLLEXPORT int load( const string & fname );

protected:
  linSparseMatElement el_;
  int rows_;
  int cols_;
};

typedef LinSparseVector SMatrix;

template< class Vector > Vector operator * ( const SMatrix & A, const Vector & b);
template< class Vector > Vector transMult( const SMatrix & A, const Vector & b);

template< class Vector > Vector operator * ( const SMatrix & A, const Vector & b){
  Vector tmp( A.rows(), 0.0 );
  SMatrix::const_iterator temp = A.begin();
  while( temp != A.end()) {

  if ( (*temp).row > tmp.size() || (*temp).col > b.size()  ){
	cout << (*temp).row << "/"  << tmp.size() << " " << (*temp).col <<"/"<<b.size() << endl; 
  }

    tmp[ (*temp).row ] += (*temp).val * b[ (*temp).col ];
    ++temp;
  }
  return tmp;
}

template< class Vector > Vector transMult( const SMatrix & A, const Vector & b){
  Vector tmp( A.columns(), 0.0 );
  SMatrix::const_iterator temp = A.begin();
  while( temp != A.end()) {
    tmp[ (*temp).col ] += (*temp).val * b[ (*temp).row ];

//if ( isnan( tmp[ (*temp).col  ] ) ){
//	cout << "NANA" << tmp[ (*temp).col ] << endl;
//	cout << (*temp).col << " " << (*temp).row << " " << (*temp).val  << " " << b[(*temp).row]<< endl; 
//	}

    ++temp;
  }
  return tmp;
}

typedef valarray< double > STLVector; 

ostream & operator << ( ostream & str, const STLVector & a );

//! Simple 2Dim Matrix based on std::valarray.
/*! Simple 2Dim Matrix based on std::valarray, found at Bjarne Stroustrup. "Die C++ Programmiersprache" */
class DLLEXPORT STLMatrix {
public:
  /*!Constructs a empty matrix with the dimension d1 x d2.*/
  STLMatrix( size_t d1 = 1, size_t d2 = 1);
  /*!Constructs a STLMatrix that is a copy of STLMatrix a.*/
  STLMatrix( const STLMatrix & a );

  /*!Default destructor. Destructs this STLMatrix and frees unused memory.*/
  ~STLMatrix(){ delete mat_; }

  /*!Assigns a shallow copy of STLMatrix a and returns a reference to this STLMatrix.*/
  STLMatrix & operator = ( const STLMatrix & a );

  STLMatrix & operator *= ( double val );

  STLMatrix & operator /= ( double val );

  STLMatrix & operator += ( const STLMatrix & a );

  /*!Returns the size of this STLMatrix. This size is dim1 * dim2 and so the number of elements.*/
  size_t size() const { return dim1_ * dim2_; }
  /*!Returns the size of dimension 1 of this STLMatrix.*/
  size_t dim1() const { return dim1_; }
  size_t row() const { return dim1_; }
  /*!Returns the size of dimension 2 of this STLMatrix.*/
  size_t dim2() const { return dim2_; }
  size_t col() const { return dim2_; }

  /*!*/
  valarray< double > row( size_t row ){ return (*mat_)[ row ]; }
  /*!*/
  valarray< double > row( size_t row ) const { return (*mat_)[ row ]; }

  /*!*/
  valarray< double > column( size_t col ) const;

  void setColumn( size_t col, const valarray< double > & vec );
  void setRow( size_t row, const valarray< double > & vec );

  /*!FORTRAN-stil indexoperator*/
  double operator () ( size_t i, size_t j ){ return (*mat_)[ i ][ j ]; }
  /*!FORTRAN-stil indexoperator*/
  double operator () ( size_t i, size_t j ) const { return (*mat_)[ i ][ j ]; }

  /*!C-stil indexoperator.*/
  valarray< double > & operator [] ( size_t i ) { return (*mat_)[ i ]; }  
  /*!*/
  valarray< double > operator [] ( size_t i ) const { return (*mat_)[ i ]; }  
  
  valarray < valarray < double > > & matrix() { return *mat_; }
  valarray < valarray < double > > matrix() const { return *mat_; }
  
  void resize( int dim1, int dim2 );

  void clean();

private:
  valarray < valarray < double > > *mat_;
  size_t dim1_, dim2_;
};

ostream & operator << ( ostream & str, const STLMatrix & a );

  //STLMatrix operator *( const STLMatrix & A, double val );
  //double mul( const valarray< double > & v1, const valarray< double > & v2 );
DLLEXPORT STLMatrix operator + ( const STLMatrix & A, const STLMatrix & B );
DLLEXPORT STLMatrix operator * ( const STLMatrix & A, const STLMatrix & B );
DLLEXPORT STLMatrix operator * ( const STLMatrix & A, double val );
DLLEXPORT STLMatrix operator * ( double val, const STLMatrix & A );

valarray< double > operator * ( const STLMatrix & A, const valarray< double > & b );
  //valarray< class Vector > operator * ( const STLMatrix & A, const Vector & b );

} // namespace MyVec
#endif // STLMATRIX__H

/*
$Log: stlmatrix.h,v $
Revision 1.14  2006/09/22 10:42:18  thomas
DLLEXPORT added

Revision 1.13  2006/09/21 17:08:40  carsten
*** empty log message ***

Revision 1.12  2006/08/02 18:24:19  carsten
*** empty log message ***

Revision 1.11  2006/07/28 16:24:51  carsten
*** empty log message ***

Revision 1.10  2006/07/27 14:52:08  carsten
*** empty log message ***

Revision 1.9  2006/07/19 19:20:18  carsten
*** empty log message ***

Revision 1.8  2006/05/29 19:42:04  carsten
*** empty log message ***

Revision 1.7  2006/02/14 11:09:18  carsten
*** empty log message ***

Revision 1.6  2006/02/06 17:53:54  carsten
*** empty log message ***

Revision 1.4  2005/10/07 17:19:49  carsten
*** empty log message ***

Revision 1.3  2005/10/06 17:38:36  carsten
*** empty log message ***

Revision 1.2  2005/01/13 21:12:15  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
