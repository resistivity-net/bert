// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "spline.h"

vector < RealPos > createSpline( const vector < RealPos > & input, int nSegments, bool close ){

  vector < double > inX( input.size() ), inY( input.size() );
  for ( uint i = 0; i < input.size(); i ++ ){
    inX[ i ] = input[ i ][ 0 ];
    inY[ i ] = input[ i ][ 1 ];
  }

  vector < CubicFunct > X,Y;
  if ( close ){
    X = calcNaturalCubicClosed( inX );
    Y = calcNaturalCubicClosed( inY );
  } else {
    X = calcNaturalCubic( inX );
    Y = calcNaturalCubic( inY );
  }

  vector < RealPos > output;
  output.push_back( RealPos( X[ 0 ].val( 0 ), Y[ 0 ].val( 0 ) ) );

  for (uint i = 0; i < X.size(); i++) {
    for (int j = 1; j <= nSegments; j++ ) {
      double u = j / (double)nSegments;
      output.push_back( RealPos( X[ i ].val( u ), Y[ i ].val( u ) ) );
    }
  }
  return output;
}

vector < RealPos > createSplineLocalDX( const vector < RealPos > & input, double localDX, bool close ){

  vector < double > inX( input.size() ), inY( input.size() );
  for ( uint i = 0; i < input.size(); i ++ ){
    inX[ i ] = input[ i ][ 0 ];
    inY[ i ] = input[ i ][ 1 ];
  }

  vector < CubicFunct > X,Y;
  if ( close ){
    X = calcNaturalCubicClosed( inX );
    Y = calcNaturalCubicClosed( inY );
  } else {
    X = calcNaturalCubic( inX );
    Y = calcNaturalCubic( inY );
  }

  vector < RealPos > output;
  output.push_back( RealPos( X[ 0 ].val( 0 ), Y[ 0 ].val( 0 ) ) );

  for (uint i = 0; i < X.size(); i++) {
    output.push_back( RealPos( X[ i ].val( localDX ), Y[ i ].val( localDX ) ) );
    output.push_back( RealPos( X[ i ].val( 1-localDX ), Y[ i ].val( 1-localDX ) ) );
    output.push_back( RealPos( X[ i ].val( 1 ), Y[ i ].val( 1 ) ) );
  }
  return output;
}

/*! FROM: http://www.cse.unsw.edu.au/~lambert/splines/
  calculates the closed natural cubic spline that interpolates
  x[0], x[1], ... x[n]
  The first segment is returned as
  C[0].a + C[0].b*u + C[0].c*u^2 + C[0].d*u^3 0<=u <1
  the other segments are in C[1], C[2], ...  C[n] */

vector < CubicFunct > calcNaturalCubicClosed( const vector < double > & x ){
  int n = x.size()-1;
  
  double w[ n + 1 ];
  double v[ n + 1 ];
  double y[ n + 1 ];
  double D[ n + 1 ];
  double z = 0.0, F = 0.0, G = 0.0, H = 0.0;
/* We solve the equation
   [4 1      1] [D[0]]   [3(x[1] - x[n])  ]
   |1 4 1     | |D[1]|   |3(x[2] - x[0])  |
   |  1 4 1   | | .  | = |      .         |
   |    ..... | | .  |   |      .         |
   |     1 4 1| | .  |   |3(x[n] - x[n-2])|
   [1      1 4] [D[n]]   [3(x[0] - x[n-1])]
   
   by decomposing the matrix into upper triangular and lower matrices
   and then back sustitution.  See Spath "Spline Algorithms for Curves
   and Surfaces" pp 19--21. The D[i] are the derivatives at the knots.
*/

 w[ 0 ] = v[ 0 ] =0;
 w[ 1 ] = v[ 1 ] = z = 1.0 / 4.0;
 y[ 0 ] = z * 3.0 * ( x[ 1 ] - x[ n ] );
 H = 4.0;
 F = 3.0 * ( x[ 0 ] - x[ n - 1 ] );
 G = 1.0;
 for ( int i = 1; i < n; i++) {
   v[ i + 1 ] = z = 1.0 / ( 4.0 - v[ i ] );
   w[ i + 1 ] = -z * w[ i ];
   y[ i ] = z * ( 3.0 * ( x[ i + 1 ] - x[ i - 1 ] ) - y[ i - 1 ] );
   H = H - G * w[ i ];
   F = F - G * y[ i - 1 ];
   G = -v[ i ] * G;
 }
 H = H - ( G + 1.0 ) * ( v[ n ] + w[ n ] );
 y[ n ] = F - ( G + 1 ) * y[ n - 1 ];
 

 D[ n ] = y[ n ] / H;
 D[ n - 1 ] = y[ n - 1 ] - ( v[ n ] + w[ n ] ) * D[ n ]; /* This equation is WRONG! in my copy of Spath */
 for ( int i = n - 2; i >= 0; i-- ) {
   D[ i ] = y[ i ] - v[ i + 1 ] * D[ i + 1 ] - w[ i + 1 ] * D[ n ];
 }


 vector < CubicFunct > C;
 for ( int i = 0; i < n; i++) {
   C.push_back( CubicFunct( x[ i ], D[ i ], 3.0 * ( x[ i + 1 ] - x[ i ] ) - 2.0 * D[ i ] - D[ i + 1 ],
		       2.0 * ( x[ i ] - x[ i + 1 ] ) + D[ i ] + D[ i + 1 ] ) );
 }
 C.push_back( CubicFunct( x[ n ], D[ n ], 3.0 * ( x[ 0 ] - x[ n ] ) - 2.0 * D[ n ] - D[ 0 ],
 		     2.0 *( x[ n ] - x[ 0 ] ) + D[ n ] + D[ 0 ] ) );
 return C;
}

vector < CubicFunct > calcNaturalCubic( const vector < double > & x ){
  int n = x.size()-1;
  
  /* We solve the equation
     [2 1       ] [D[0]]   [3(x[1] - x[0])  ]
     |1 4 1     | |D[1]|   |3(x[2] - x[0])  |
     |  1 4 1   | | .  | = |      .         |
     |    ..... | | .  |   |      .         |
     |     1 4 1| | .  |   |3(x[n] - x[n-2])|
     [       1 2] [D[n]]   [3(x[n] - x[n-1])]
     
     by using row operations to convert the matrix to upper triangular
     and then basic sustitution.  The D[i] are the derivatives at the inots.
  */
  
  double gamma[ n + 1 ];
  gamma[ 0 ] = 0.5;
  for ( int i = 1; i < n; i++) {
    gamma[ i ] = 1.0 / ( 4 - gamma[ i - 1 ] );
  }
  gamma[ n ] = 1.0 / ( 2 - gamma[ n - 1 ] );
  
  double delta[ n + 1 ];

  delta[ 0 ] = 3.0 * ( x[ 1 ] - x[ 0 ] ) * gamma[ 0 ];
  for ( int i = 1; i < n; i++ ){
    delta[ i ] = ( 3.0 * ( x[ i + 1 ] - x[ i - 1 ] ) - delta[ i - 1 ] ) * gamma[ i ];
  }
  delta[ n ] = ( 3.0 * ( x[ n ] - x[ n - 1 ] ) - delta[ n - 1 ] ) * gamma[ n ];
  
  double D[ n + 1 ];
  D[ n ] = delta[ n ];
  for ( int i = n-1; i >= 0; i--) {
    D[ i ] = delta[ i ] - gamma[ i ] * D[ i + 1 ];
  }

  /* now compute the coefficients of the cubics */
  vector < CubicFunct > C;
  for ( int i = 0; i < n; i++ ){
    C.push_back( CubicFunct( x[ i ], D[i], 3 * (x[ i + 1 ] - x[ i ] ) - 2 * D[ i ] - D[ i + 1 ],
			2 * ( x[ i ] - x[ i + 1 ] ) + D[ i ] + D[ i + 1 ] ) );
  }
  return C;
}

/*
$Log: spline.cpp,v $
Revision 1.3  2009/07/09 22:31:04  carsten
some small fixes and simple 1.check-script for examples

Revision 1.2  2005/09/12 18:50:50  carsten
*** empty log message ***

Revision 1.1  2005/09/08 19:08:48  carsten
*** empty log message ***

 */
