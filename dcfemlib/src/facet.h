// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef FACET__H
#define FACET__H FACET__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "polygon.h"
#include "line.h"
#include "plane.h"

#include <list>
using namespace std;

namespace MyMesh{

class Mesh2D;

class Facet : public vector< Polygon >{
public:
  Facet( ){
    leftmarker_ = 0;
    rightmarker_ = 0;
    boundarymarker_ = 0;
  }
  //*! left == inner region, right = outer region; right = -1 than this facet is an boundary facet;
  Facet( const Polygon & polygon, int left = 0, int right = 0, int boundary = 0)
    : leftmarker_( left ), rightmarker_( right ), boundarymarker_( boundary ) {
    push_back( polygon );
//     for ( size_t i = 0, imax = polygon.size(); i < imax; i++ ) cout << polygon[i]->id() << "\t";
//     cout << endl;
  }

  Facet( const Facet & facet ) : vector< Polygon >( facet ) {
    //    cerr << WHERE_AM_I << " Warnung schwache kopie !" << endl;
    leftmarker_ = facet.leftMarker();
    rightmarker_ = facet.rightMarker();
    boundarymarker_ = facet.boundaryMarker();
    vecpHoles_ = facet.holes();
  }
  Facet( vector < Polygon > & polygonVector, int left = 0, int right = 0, int boundary = 0);
  DLLEXPORT Facet( Node * v1, Node * v2, Node * v3, Node * v4, int left = 0, int right  = 0 , int marker = 0);
  DLLEXPORT Facet( Node * v1, Node * v2, Node * v3, int left = 0, int right = 0, int marker = 0);

  virtual ~Facet(){}

  virtual void clear(){
    vector< Polygon >::clear();
    vecpHoles_.clear();
  }

  /*! */
  bool operator == ( const Facet & facet );
  /*! */
  bool operator != ( const Facet & facet );
  /*! */
  bool compare( const Facet & facet, double tol = TOLERANCE );

  /*! */
  void insert( const Line & line );
  
  /*!Check if this Facet is valid. It have to be coplanar and more than 2 Nodes, except 2 identical nodes for the single free Node.*/
  bool valid( double tol = TOLERANCE ) const;
  RealPos norm( ) const { return plane().norm(); }

  list< RealPos > intersect( const Plane & plane, double tol = TOLERANCE );

  /*!Returns a list of RealPos which represents the intersection of the line and the facet, respectively the intersection of this line and each edge of facet.
   If the line break trough the Facet the size of the returning list of intersection Points is one. Else the count of Intersection points have to be even.*/
  list< RealPos > intersect( const Line & line, double tol = TOLERANCE );

  /*!Check if this facet is coplanar that means every node of this facet have to touch the plane of this facet with a tolerance of tol (default = 1e-12).
    The method returns 1 if facet is coplanar, else it returns 0.*/
  DLLEXPORT bool isCoplanar( double tol = TOLERANCE, bool verbose = true ) const ;

  /*!Returns the plane of this Facet. Each facet have to be coplanar.*/
  Plane plane() const;

  inline int leftMarker( ) const { return leftmarker_; }
  inline int rightMarker( ) const { return rightmarker_; }
  inline int boundaryMarker( ) const { return boundarymarker_; }

  inline void setLeftMarker( int left ) { leftmarker_ = left; }
  inline void setRightMarker( int right ) { rightmarker_ = right; }
  inline void setBoundaryMarker( int boundary ) { boundarymarker_ = boundary; }
  void setBoundaryMarkerGivenByNodes();

    RealPos center() const {
        size_t nodeCountPoly = (*this)[0].size();
        RealPos center( 0.0, 0.0, 0.0 );
        for ( size_t i = 0; i < nodeCountPoly;  i ++ ){
            center += (*this)[0][i]->pos();
        }
        return center / (double)nodeCountPoly;
    }
    
  Polygon tangent( const Facet & face );
  RealPos findNodeVarZ( double x, double y );
  void switchDirection();
  bool isLeft();

  void insertFreeNode( Node * node, bool verbose = false );
  bool insertNode( Node * node, bool verbose = false );
  void snap( Node * node , bool verbose = false);

  /*! Returns true if the Facet already contains the Node, else it returns false.*/
  bool contains( Node * node );

  bool touch( const Facet & facet, double tol = TOLERANCE);
  /*! Returns true if pos touch this Facet with a numerical tolerance of tol. ( default = 1e-12 ). At the moment this method works only if this Facet is convex. */
  bool touch( const RealPos & pos, bool withBoundary = true, double tol = TOLERANCE);

  void createHole( const RegionMarker & hole ) { vecpHoles_.push_back( new RegionMarker( hole ) ); }
  void createHole( const RealPos & hole ) { vecpHoles_.push_back( new RegionMarker( hole ) ); }

  int holeCount() const { return vecpHoles_.size(); }
  RegionMarker & hole( int i ) const { return * vecpHoles_[ i ]; }
  vector < RegionMarker * > & holes() { return vecpHoles_ ; }
  vector < RegionMarker * > holes() const { return vecpHoles_ ; }

    template < class Mat > void transformHoles( const Mat & mat ){
       for ( size_t i = 0; i < vecpHoles_.size(); i ++ ) vecpHoles_[ i ]->pos().transform( mat );
    }

  Mesh2D triangulate( double quality = 0.0 );

protected:
  set <Node *> collectAllNodes() const;
  vector< RegionMarker * > vecpHoles_ ;

  int leftmarker_;
  int rightmarker_;
  int boundarymarker_;
 
};

ostream & operator << ( ostream & str, Facet & facet );

} //namespace MyMesh

#endif // FACET__H
/*
$Log: facet.h,v $
Revision 1.11  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.10  2008/02/28 11:52:51  carsten
*** empty log message ***

Revision 1.9  2008/02/28 10:47:33  carsten
*** empty log message ***

Revision 1.8  2007/10/04 07:21:59  thomas
BERT + polytools codeblocks (dllexport)

Revision 1.7  2007/09/18 18:46:22  carsten
*** empty log message ***

Revision 1.6  2006/07/28 16:24:51  carsten
*** empty log message ***

Revision 1.5  2006/04/17 20:58:03  carsten
*** empty log message ***

Revision 1.4  2005/08/22 17:28:09  carsten
*** empty log message ***

Revision 1.3  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.2  2005/03/08 16:04:45  carsten
*** empty log message ***

Revision 1.1  2005/02/07 13:33:13  carsten
*** empty log message ***

*/
