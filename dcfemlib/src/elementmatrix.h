// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef ELEMENTMATRIX__H
#define ELEMENTMATRIX__H

#include "myvec/matrix.h"
using namespace MyMesh;

const double Triangle6_S1[ 6 ][ 6 ] = {
  {  3.0,  1.0, 0.0, -4.0,  0.0,  0.0 },
  {  1.0,  3.0, 0.0, -4.0,  0.0,  0.0 },
  {  0.0,  0.0, 0.0,  0.0,  0.0,  0.0 },
  { -4.0, -4.0, 0.0,  8.0,  0.0,  0.0 },
  {  0.0,  0.0, 0.0,  0.0,  8.0, -8.0 },
  {  0.0,  0.0, 0.0,  0.0, -8.0,  8.0 }
};
const double Triangle6_S2[ 6 ][ 6 ] = {
  {  6.0,  1.0,  1.0, -4.0,  0.0, -4.0 },
  {  1.0,  0.0, -1.0, -4.0,  4.0,  0.0 },
  {  1.0, -1.0,  0.0,  0.0,  4.0, -4.0 },
  { -4.0, -4.0,  0.0,  8.0, -8.0,  8.0 },
  {  0.0,  4.0,  4.0, -8.0,  8.0, -8.0 },
  { -4.0,  0.0, -4.0,  8.0, -8.0,  8.0 }
};
const double Triangle6_S3[ 6 ][ 6 ] = {
  {  3.0,  0.0,  1.0,  0.0,  0.0, -4.0 },
  {  0.0,  0.0,  0.0,  0.0,  0.0,  0.0 },
  {  1.0,  0.0,  3.0,  0.0,  0.0, -4.0 },
  {  0.0,  0.0,  0.0,  8.0, -8.0,  0.0 },
  {  0.0,  0.0,  0.0, -8.0,  8.0,  0.0 },
  { -4.0,  0.0, -4.0,  0.0,  0.0,  8.0 }
};
const double Triangle6_S4[ 6 ][ 6 ] = {
  {  6.0, -1.0, -1.0,  0.0, -4.0,  0.0 },
  { -1.0,  6.0, -1.0,  0.0,  0.0, -4.0 },
  { -1.0, -1.0,  6.0, -4.0,  0.0,  0.0 },
  {  0.0,  0.0, -4.0, 32.0, 16.0, 16.0 },
  { -4.0,  0.0,  0.0, 16.0, 32.0, 16.0 },
  {  0.0, -4.0,  0.0, 16.0, 16.0, 32.0 }
};
const double Edge3_Me[ 3 ][ 3 ] = { // Me = edge.length / 30.0 * Edge3_Me
  {  4.0,  2.0, -1.0 }, 
  {  2.0, 16.0,  2.0 }, 
  { -1.0,  2.0,  4.0 }
};
const double Edge3_Se[ 3 ][ 3 ] = { // Se = 1 / ( 3.0 * edge.length ) * Edge3_Se
  {  7.0, -8.0,  1.0 }, 
  { -8.0, 16.0, -8.0 }, 
  {  1.0, -8.0,  7.0 }
};

namespace MyVec{
class ElementMatrix : public BasisMatrix< double >{
  //template< class T > class ElementMatrix : public BasisMatrix< T >{
  
public:
  ElementMatrix( int n = 0 ){
    size_ = n;
    mat = new double *[ size_ ]; assert( mat );
    for ( int i = 0; i < size_; i++ ){
      mat[ i ] = new double[ size_ ]; assert( mat[ i ] );
      vec_i.push_back( 0 );
    }

    Tri6_u_xi2 = new STLMatrix( 6, 6 );
    Tri6_u_eta2 = new STLMatrix( 6, 6 );
    Tri6_u_xi_u_eta = new STLMatrix( 6, 6 );
    Tri6_u2 = new STLMatrix( 6, 6 );

    Tet4_u_xi2 = new STLMatrix( 4, 4 );
    Tet4_u_eta2 = new STLMatrix( 4, 4 );
    Tet4_u_zeta2 = new STLMatrix( 4, 4 );
    Tet4_u_xi_u_eta = new STLMatrix( 4, 4 );
    Tet4_u_xi_u_zeta = new STLMatrix( 4, 4 );
    Tet4_u_eta_u_zeta = new STLMatrix( 4, 4 );
    Tet4_u2= new STLMatrix( 4, 4 );

    Tet10_u_xi2 = new STLMatrix( 10, 10 );
    Tet10_u_eta2 = new STLMatrix( 10, 10 );
    Tet10_u_zeta2 = new STLMatrix( 10, 10 );
    Tet10_u_xi_u_eta = new STLMatrix( 10, 10 );
    Tet10_u_xi_u_zeta = new STLMatrix( 10, 10 );
    Tet10_u_eta_u_zeta = new STLMatrix( 10, 10 );
    Tet10_u2= new STLMatrix( 10, 10 );

    buildBaseMatrixes();
  }
  
  ~ElementMatrix(){
    freeMem();
    delete Tri6_u_xi2;
    delete Tri6_u_eta2;
    delete Tri6_u_xi_u_eta;
    delete Tri6_u2;

    delete Tet4_u_xi2;
    delete Tet4_u_eta2;
    delete Tet4_u_zeta2;
    delete Tet4_u_xi_u_eta;
    delete Tet4_u_xi_u_zeta;
    delete Tet4_u_eta_u_zeta;
    delete Tet4_u2;

    delete Tet10_u_xi2;
    delete Tet10_u_eta2;
    delete Tet10_u_zeta2;
    delete Tet10_u_xi_u_eta;
    delete Tet10_u_xi_u_zeta;
    delete Tet10_u_eta_u_zeta;
    delete Tet10_u2;

    vec_i.clear();
  }
  void freeMem(){
    for ( int i = 0; i < size_; i++ ) delete [] mat[ i ];
    delete [] mat;
  }

  ElementMatrix( const ElementMatrix & A ) : BasisMatrix< double >( A ), vec_i( A.vec_i ) { // = Kopierkonstruktor
    size_ = A.size();
    mat = new double *[ size_ ]; assert( mat );
    for ( int i = 0; i < size_; i++ ){
      mat[ i ] = new double[ size_ ]; assert( mat[ i ] );
      vec_i.push_back( 0 );
    }

    for ( int i = 0; i < size_; i++ ) memcpy( mat[i], A.mat[i], size_ * sizeof(double) );
  }

  ElementMatrix & operator = ( const ElementMatrix & A ){ // = Operator
    //    cout << "EM::=OP\t" << endl;
    if ( this != & A ){
      if (size_ != A.size() ){
	freeMem();
	size_ = A.size();
	mat = new double *[ size_ ]; assert( mat );
	for ( int i = 0; i < size_; i++ ){
	  mat[ i ] = new double[ size_ ]; assert( mat[ i ] );
	}
      }	
      
      for ( int i = 0; i < size_; i++ ) memcpy( mat[i], A.mat[i], size_ * sizeof(double) );
      vec_i = A.vec_i;
    }
    return *this;
  }

  ElementMatrix & operator += (const ElementMatrix & A){
    //    cout << "EM::+=EM\t" << endl;
    assert( size_ == A.size() );
    for ( int i = 0; i < size_; i++ ) 
      for ( int j = 0; j < size_; j++ ) 
	mat[ i ][ j ] += A.mat[ i ][ j ];
    return *this;
  }
  
  ElementMatrix & operator *= ( double a ){
    //    cout << "EM::*=a\t" << endl;
    for ( int i = 0; i < size_; i++ )
      for ( int j = 0; j < size_; j++ ) mat[ i ][ j ] *= a;
    return * this;
  }

  void clear(){ for ( int i = 0; i < size_; i++ ) memset( mat[ i ], '\0', size_*sizeof(double) ); }
  inline int size() const { return size_; }
  inline int size() { return size_; }

  void show(){
    for ( int i = 0; i < size_; i++ ){
      for ( int j = 0; j < size_; j++ ){
	cout << mat[ i ][ j ] << "\t";
      }
      cout << vec_i[ i ] << endl;
    }
  }
  
  void resize( int size ){
    //    cout << "resize>\t" << size_<< "\t>" << size<< endl;
    /* delete old stuff */
    freeMem();

    /* allocate new mem */
    size_ = size;
    mat = new double *[ size_ ]; assert( mat );
    //    vec_i.reserve( size_ );
    for ( int i = 0; i < size_; i++ ){
      mat[ i ] = new double[ size_ ]; assert( mat[i] );
      vec_i.push_back(0);
    }
    clear();
  }

  //  inline int i( int i ){ assert (i>0 && i<=size_); return vec_i[ i ]; }
  inline int i( int i ) const { return vec_i[ i ]; }

  inline double at( int i, int j ) const {
    //    assert ( i > -1 && i < size_ ); assert (j > -1 && j < size_ ); 
    return mat[ i ][ j ]; 
  }

//   inline double at( int i, int j) const {
//     assert (i>0 && i<=size_); assert (j>0 && j<=size_); return(mat[i-1][j-1]); }

  inline void setVal( int i, int j, double val ){
    assert (i > -1 && i < size_); assert (j > -1 && j < size_) ; 
    mat[ i ][ j ] = val; 
  }

  inline void setJonI( int i, int j ){ vec_i[ i ] = j; }

ElementMatrix & ux2uy2uz2( BaseElement & cell ){
  // Elementsteifigkeitsmatrix (schwartz)
 
  switch( cell.rtti() ){
  case MYMESH_TRIANGLE_RTTI:
  return this->ux2uy2_dxdy( cell );
    break;
  case MYMESH_TRIANGLE6_RTTI:
    return this->ux2uy2_dxdy( cell );
    break;
  }

  int nodeCount = cell.nodeCount();
  if ( size_ != nodeCount ) resize( nodeCount );
  for ( int i = 0; i < nodeCount; i++ ) vec_i[ i ] = cell.node( i ).id();

  double J = cell.jacobianDeterminant( );
  if ( J < 0 ) {
    cerr << WHERE_AM_I << "JacobianDeterminant < 0 (" << J << ")" 
	 <<  " for cell " << cell.id() << endl;
    J = fabs(J);
  }
  
  double x_xi = cell.partDerivationRealToUnity( 'x', 1 );
  double x_eta = cell.partDerivationRealToUnity( 'x', 2 );
  double x_zeta = cell.partDerivationRealToUnity( 'x', 3 );
  
  double y_xi = cell.partDerivationRealToUnity( 'y', 1 );
  double y_eta = cell.partDerivationRealToUnity( 'y', 2 );
  double y_zeta = cell.partDerivationRealToUnity( 'y', 3 );
  
  double z_xi = cell.partDerivationRealToUnity( 'z', 1 );
  double z_eta = cell.partDerivationRealToUnity( 'z', 2 );
  double z_zeta = cell.partDerivationRealToUnity( 'z', 3 );
  
  double xi_x = 1.0 / J * det2( y_eta, z_eta, y_zeta, z_zeta );
  double xi_y = -1.0 / J * det2( x_eta, z_eta, x_zeta, z_zeta );
  double xi_z = 1.0 / J * det2( x_eta, y_eta, x_zeta, y_zeta );
  
  double eta_x = -1.0 / J * det2( y_xi, z_xi, y_zeta, z_zeta );
  double eta_y = 1.0 / J * det2( x_xi, z_xi, x_zeta, z_zeta );
  double eta_z = -1.0 / J * det2( x_xi, y_xi, x_zeta, y_zeta );
  
  double zeta_x = 1.0 / J * det2( y_xi, z_xi, y_eta, z_eta );
  double zeta_y = -1.0 / J * det2( x_xi, z_xi, x_eta, z_eta );
  double zeta_z = 1.0 / J * det2( x_xi, y_xi, x_eta, y_eta );
  
  double a = J * ( xi_x * xi_x + xi_y * xi_y + xi_z * xi_z );
  double b = J * ( eta_x * eta_x + eta_y * eta_y + eta_z * eta_z );
  double c = J * ( zeta_x * zeta_x + zeta_y * zeta_y + zeta_z * zeta_z );
  
  double d = J * ( xi_x * eta_x + xi_y * eta_y + xi_z * eta_z );
  double e = J * ( xi_x * zeta_x + xi_y * zeta_y + xi_z * zeta_z );
  double f = J * ( eta_x * zeta_x + eta_y * zeta_y + eta_z * zeta_z );
  
  STLMatrix compound( 1, 1 );
  //  cout << J << endl;
  //  cout << xi_x * zeta_x << "\t" << xi_y * zeta_y <<"\t"<< xi_z * zeta_z << endl;
  //cout << a << "\t"<<  b << "\t"<<  c << "\t"<<  d << "\t"<<  e << "\t"<<  f << endl;

  double val = 0.0;
  switch( cell.rtti() ){
  case MYMESH_TETRAHEDRON_RTTI:{
    //     compound = a * (*Tet4_u_xi2) + b * (*Tet4_u_eta2) + c * (*Tet4_u_zeta2)
    //       + (2*d) * (*Tet4_u_xi_u_eta) 
    //       + (2*e) * (*Tet4_u_xi_u_zeta) 
    //       + (2*f) * (*Tet4_u_eta_u_zeta);
    
    a = a * 1. / 6.;
    double u_xi2[ 4 ][ 4 ] = {
      {  a, -a, 0.0, 0.0},
      { -a,  a, 0.0, 0.0},
      {  0.0,  0.0, 0.0, 0.0},
      {  0.0,  0.0, 0.0, 0.0}
    };
    b = b * 1. / 6.;
    double u_eta2[ 4 ][ 4 ] = {
      {  b, 0.0, -b, 0.0},
      {  0.0, 0.0,  0.0, 0.0},
      { -b, 0.0,  b, 0.0},
      {  0.0, 0.0,  0.0, 0.0}
    };
    c = c * 1. / 6.;
    double u_zeta2[ 4 ][ 4 ] = {
      {  c, 0.0, 0.0, -c},
      {  0.0, 0.0, 0.0,  0.0},
      {  0.0, 0.0, 0.0,  0.0},
      { -c, 0.0, 0.0,  c}
    };
    d = d * 1. / 6.;
    double u_xi__u_eta[ 4 ][ 4 ] = {
      { 2.0 * d, -d, -d, 0.0},
      {    -d,  0.0,  d, 0.0},
      {    -d,  d,  0.0, 0.0},
      {     0.0,  0.0,  0.0, 0.0}
    };
    e = e * 1. / 6.;
    double u_xi__u_zeta[ 4 ][ 4 ] = {
      { 2.0 * e, -e, 0.0, -e},
      {    -e,  0.0, 0.0,  e},
      {     0.0,  0.0, 0.0,  0.0},
      {    -e,  e, 0.0,  0.0}
    };
    f = f * 1. / 6.;
    double u_eta__u_zeta[ 4 ][ 4 ] = {
      { 2.0 * f, 0.0, -f, -f},
      {     0.0, 0.0,  0.0,  0.0},
      {    -f, 0.0,  0.0,  f},
      {    -f, 0.0,  f,  0.0}
    };
  
    for ( int i = 0; i < nodeCount; i++ ){
      for ( int j = 0; j < nodeCount; j++ ){
	val = u_xi2[ i ][ j ] 
	  + u_eta2[ i ][ j ] 
	  + u_zeta2[ i ][ j ] 
	  + u_xi__u_eta[ i ][ j ] 
	  + u_xi__u_zeta[ i ][ j ] 
	  + u_eta__u_zeta[ i ][ j ];
	if ( fabs( val ) > 1e-15 ) mat[ i ][ j ] = val;
	else mat[ i ][ j ] = 0.0;
      }
    }
  }
    //    this->show();
    //    exit(0);
  break;
  case MYMESH_TETRAHEDRON10_RTTI: {
    //    d = 0;
    //e = 0; 
    //f = 0; 
    compound = a * (*Tet10_u_xi2) + b * (*Tet10_u_eta2) + c * (*Tet10_u_zeta2)
      + (2.0*d) * (*Tet10_u_xi_u_eta) 
      + (2.0*e) * (*Tet10_u_xi_u_zeta) 
      + (2.0*f) * (*Tet10_u_eta_u_zeta);
    //    cout << compound << endl;
    for ( int i = 0; i < nodeCount; i++ ){
      for ( int j = 0; j < nodeCount; j++ ){
	//       mat[ i ][ j ] = u_xi2[ i ][ j ] 
	// 	+ u_eta2[ i ][ j ] 
	// 	+ u_zeta2[ i ][ j ] 
	// 	+ u_xi__u_eta[ i ][ j ] 
	// 	+ u_xi__u_zeta[ i ][ j ] 
	// 	+ u_eta__u_zeta[ i ][ j ];
	mat[ i ][ j ] = compound[ i ][ j ];
      }
    }
    break;
  }
  default: cerr << WHERE_AM_I << " celltype not spezified: " << cell.rtti() << endl; 
  }

  return * this;
}

ElementMatrix & ux2uy2_dxdy( BaseElement & cell ){
  // Elementsteifigkeitsmatrix (schwartz)
  int nodeCount = cell.nodeCount();
  if ( size_ != nodeCount ) resize( nodeCount );
  for ( int i = 0; i < nodeCount; i++ ) vec_i[ i ] = cell.node(i).id();

  double x1 = 0.0 , x2 = 0.0, x3 = 0.0, y1 = 0.0, y2 = 0.0, y3 = 0.0;
  
  switch( cell.rtti() ){
  case MYMESH_TRIANGLE_RTTI:
  case MYMESH_TRIANGLE6_RTTI:
    x1 = cell.node(0).x();
    x2 = cell.node(1).x();
    x3 = cell.node(2).x();
    y1 = cell.node(0).y();
    y2 = cell.node(1).y();
    y3 = cell.node(2).y(); 
    break;
  case MYMESH_QUADRANGLE_RTTI: 
    x1 = cell.node(0).x();
    x2 = cell.node(1).x();
    x3 = cell.node(3).x();  // links rum,  nachrechnen schwarz S.86 f.
    y1 = cell.node(0).y();
    y2 = cell.node(1).y();
    y3 = cell.node(3).y(); // links rum,  nachrechnen schwarz S.86 f.
    break;
  default: cerr << WHERE_AM_I << "celltype not spezified: " << cell.rtti() << endl; break;
  }
  
  double J = cell.jacobianDeterminant();
  if ( J < 0 ) cerr << WHERE_AM_I << "JacobianDeterminant < 0 (" << J << ")" << endl;

  double a, b, c;
  a = ( (x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1) ) / J;
  b = - ( (x3 - x1) * (x2 - x1) + (y3 - y1) * (y2 - y1) ) / J;
  c = ( (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1) ) / J;
  
  //        cout << "J: " << J << "\ta: " << a << "\tb: " << b << "\tc: " << c << endl;
  switch( cell.rtti() ){
  //  mat = aS_a + bS_b + cS_c;
  case MYMESH_TRIANGLE_RTTI:
    mat[0][0] = a *  0.5 + b        + c *  0.5;
    mat[1][0] = a * -0.5 + b * -0.5           ;
    mat[2][0] =            b * -0.5 + c * -0.5;
    mat[0][1] = a * -0.5 + b * -0.5           ;
    mat[1][1] = a *  0.5                      ;
    mat[2][1] =            b *  0.5           ;
    mat[0][2] =            b * -0.5 + c * -0.5;
    mat[1][2] =            b *  0.5           ;
    mat[2][2] =                       c *  0.5; 
    return * this;
    break;
  case MYMESH_TRIANGLE6_RTTI:
    for ( int i = 0, imax = 6; i < imax; i ++ ){
      for ( int j = 0, jmax = 6; j < jmax; j ++ ){
	mat[ i ][ j ] = 
	  Triangle6_S1[ i ][ j ] * a / 6.0 + 
	  Triangle6_S2[ i ][ j ] * b / 6.0 + 
	  Triangle6_S3[ i ][ j ] * c / 6.0;
      }
    }
    break;
  case MYMESH_QUADRANGLE_RTTI:
    mat[0][0] =  2./6. * a + 0.5 * b + 2./6. * c;
    mat[1][0] = -2./6. * a           + 1./6. * c;
    mat[2][0] = -1./6. * a - 0.5 * b - 1./6. * c;
    mat[3][0] =  1./6. * a           - 2./6. * c;
    mat[0][1] = -2./6. * a           + 1./6. * c;
    mat[1][1] =  2./6. * a - 0.5 * b + 2./6. * c;
    mat[2][1] =  1./6. * a           - 2./6. * c;
    mat[3][1] = -1./6. * a + 0.5 * b - 1./6. * c;
    mat[0][2] = -1./6. * a - 0.5 * b - 1./6. * c;
    mat[1][2] =  1./6. * a           - 2./6. * c;
    mat[2][2] =  2./6. * a + 0.5 * b + 2./6. * c;
    mat[3][2] = -2./6. * a           + 1./6. * c;
    mat[0][3] =  1./6. * a           - 2./6. * c;
    mat[1][3] = -1./6. * a + 0.5 * b - 1./6. * c;
    mat[2][3] = -2./6. * a           + 1./6. * c;
    mat[3][3] =  2./6. * a - 0.5 * b + 2./6. * c;
    break;
  default : cerr << WHERE_AM_I << "celltype not spezified " << cell.rtti() << endl; break; 
  }
  return * this;
}

ElementMatrix & u2( BaseElement & boundary ){
  // Masselement matrix
  int nodeCount = boundary.nodeCount();
  if ( size_ != nodeCount ) resize( nodeCount );
  for ( int i = 0; i < nodeCount; i++ ) vec_i[ i ] = boundary.node( i ).id();
  
  double J = boundary.jacobianDeterminant(), l = 0.0;

 if ( J < 0 ) {
    cerr <<  WHERE_AM_I << " Jacobi-Determinant < 0 " << J << "  " << boundary.id() << endl;
 }
  
  STLMatrix compound( 1, 1 );

  switch( boundary.rtti() ){

  case MYMESH_EDGE_RTTI:

    l = dynamic_cast<Edge & >( boundary ).length();
    mat[0][0] = l / 3.0;
    mat[1][0] = l / 6.0;
    mat[0][1] = l / 6.0;
    mat[1][1] = l / 3.0;
    break;

  case MYMESH_EDGE3_RTTI:

    // hack: im schwartz ist die edge 1-2-3 bezeichnet bei mir 1-3-2 die Me muss amngepass werden
    vec_i[ 1 ] = boundary.node( 2 ).id();
    vec_i[ 2 ] = boundary.node( 1 ).id();
    l = dynamic_cast<Edge & >( boundary ).length();
    for ( int i = 0, imax = 3; i < imax; i ++ ){
      for ( int j = 0, jmax = 3; j < jmax; j ++ ){
	mat[ i ][ j ] = Edge3_Me[ i ][ j ] * l / 30.0;
      }
    }
    break;

  case MYMESH_TRIANGLE_RTTI:
  case MYMESH_TRIANGLEFACE_RTTI:
    J = J / 24.0;
    mat[0][0] = J * 2.0;
    mat[1][0] = J;
    mat[2][0] = J;
    mat[0][1] = J;
    mat[1][1] = J * 2.0;
    mat[2][1] = J;
    mat[0][2] = J;
    mat[1][2] = J;
    mat[2][2] = J * 2.0;
    break;
  case MYMESH_TRIANGLE6_RTTI:
  case MYMESH_TRIANGLE6FACE_RTTI:

    compound = (*Tri6_u2) * J;

    for ( int i = 0; i < nodeCount; i++ ){
      for ( int j = 0; j < nodeCount; j++ ){
	mat[ i ][ j ] = compound[ i ][ j ];
      }
    }
    break;
  case MYMESH_QUADRANGLE_RTTI:
    J = J / 36.0;
    mat[0][0] = J * 4.0;
    mat[1][0] = J * 2.0;
    mat[2][0] = J;
    mat[3][0] = J * 2.0;
    mat[0][1] = J * 2.0;
    mat[1][1] = J * 4.0;
    mat[2][1] = J * 2.0;
    mat[3][1] = J;
    mat[0][2] = J;
    mat[1][2] = J * 2.0;
    mat[2][2] = J * 4.0;
    mat[3][2] = J * 2.0;
    mat[0][3] = J * 2.0;
    mat[1][3] = J;
    mat[2][3] = J * 2.0;
    mat[3][3] = J * 4.0;
    break;
  default: cerr << WHERE_AM_I << "boundarytype not spezified " << boundary.rtti() << endl; 
  }
  return * this;
}

ElementMatrix & u( BaseElement & cell, const RVector & f ){
  int nodeCount = cell.nodeCount();
  if ( size_ != nodeCount ) resize( nodeCount );
  for ( int i = 0; i < nodeCount; i++ ) vec_i[ i ] = cell.node( i ).id();

  double J = cell.jacobianDeterminant();
  
  switch( cell.rtti() ){
  case MYMESH_TRIANGLE_RTTI: 
    mat[ 0 ][ 0 ] = 1.0 / 6.0 * J * f[ vec_i[ 0 ] ];
    mat[ 1 ][ 0 ] = 1.0 / 6.0 * J * f[ vec_i[ 1 ] ];
    mat[ 2 ][ 0 ] = 1.0 / 6.0 * J * f[ vec_i[ 2 ] ];
    break;
  case MYMESH_TRIANGLE6_RTTI:
    mat[ 0 ][ 0 ] = 0.0;
    mat[ 1 ][ 0 ] = 0.0;
    mat[ 2 ][ 0 ] = 0.0;
    mat[ 3 ][ 0 ] = 1.0 / 6.0 * J * f[ vec_i[ 3 ] ];
    mat[ 4 ][ 0 ] = 1.0 / 6.0 * J * f[ vec_i[ 4 ] ];
    mat[ 5 ][ 0 ] = 1.0 / 6.0 * J * f[ vec_i[ 5 ] ];
    break;
  default: cerr << WHERE_AM_I << " celltype not spezified " << endl; 
  }
  
  return * this;
}

  void buildBaseMatrixes(){
    //    cout << WHERE_AM_I << " start." << endl;
//     *Tri3_u_xi2 = transpose( TriangleLinearA() ) 
//       * createMatrix( 3, intLinTriangle_u_xi2() ) * TriangleLinearA();
//     *Tri3_u_eta2 = transpose( TriangleLinearA() ) 
//       * createMatrix( 3, intLinTriangle_u_eta2() ) * TriangleLinearA();
//     *Tri3_u_xi_u_eta = transpose( TriangleLinearA() ) 
//       * createMatrix( 3, intLinTriangle_u_xi_u_eta() ) * TriangleLinearA();
//     *Tri3_u2 = transpose( TriangleLinearA() ) 
//       * createMatrix( 3, intLinTriangle_u2() ) * TriangleLinearA();

    *Tri6_u_xi2 = transpose( TriangleQuadA() ) 
      * createMatrix( 6, intQuadTriangle_u_xi2() ) * TriangleQuadA();
    *Tri6_u_eta2 = transpose( TriangleQuadA() ) 
      * createMatrix( 6, intQuadTriangle_u_eta2() ) * TriangleQuadA();
    *Tri6_u_xi_u_eta = transpose( TriangleQuadA() ) 
      * createMatrix( 6, intQuadTriangle_u_xi_u_eta() ) * TriangleQuadA();
    *Tri6_u2 = transpose( TriangleQuadA() ) 
      * createMatrix( 6, intQuadTriangle_u2() ) * TriangleQuadA();
    
    *Tet4_u_xi2 = transpose( TetrahedronLinearA() ) 
      * createMatrix( 4, intLinTetrahedron_u_xi2() ) * TetrahedronLinearA();
    *Tet4_u_eta2 = transpose( TetrahedronLinearA() ) 
      * createMatrix( 4, intLinTetrahedron_u_eta2() ) * TetrahedronLinearA();
    *Tet4_u_zeta2 = transpose( TetrahedronLinearA() ) 
      * createMatrix( 4, intLinTetrahedron_u_zeta2() ) * TetrahedronLinearA();
    *Tet4_u_xi_u_eta = transpose( TetrahedronLinearA() ) 
      * createMatrix( 4, intLinTetrahedron_u_xi_u_eta() ) * TetrahedronLinearA();
    *Tet4_u_xi_u_zeta = transpose( TetrahedronLinearA() ) 
      * createMatrix( 4, intLinTetrahedron_u_xi_u_zeta() ) * TetrahedronLinearA();
    *Tet4_u_eta_u_zeta = transpose( TetrahedronLinearA() ) 
      * createMatrix( 4, intLinTetrahedron_u_eta_u_zeta() ) * TetrahedronLinearA();
    
    *Tet10_u_xi2 = transpose( TetrahedronQuadA() ) 
      * createMatrix( 10, intQuadTetrahedron_u_xi2() ) * TetrahedronQuadA();
    *Tet10_u_eta2 = transpose( TetrahedronQuadA() ) 
      * createMatrix( 10, intQuadTetrahedron_u_eta2() ) * TetrahedronQuadA();
    *Tet10_u_zeta2 = transpose( TetrahedronQuadA() ) 
      * createMatrix( 10, intQuadTetrahedron_u_zeta2() ) * TetrahedronQuadA();
    *Tet10_u_xi_u_eta = transpose( TetrahedronQuadA() ) 
      * createMatrix( 10, intQuadTetrahedron_u_xi_u_eta() ) * TetrahedronQuadA();
    *Tet10_u_xi_u_zeta = transpose( TetrahedronQuadA() ) 
      * createMatrix( 10, intQuadTetrahedron_u_xi_u_zeta() ) * TetrahedronQuadA();
    *Tet10_u_eta_u_zeta = transpose( TetrahedronQuadA() ) 
      * createMatrix( 10, intQuadTetrahedron_u_eta_u_zeta() ) * TetrahedronQuadA();
  }

protected:
  double** mat;
  int size_;
  std::vector<int> vec_i;
  
  STLMatrix *Tri3_u_xi2;
  STLMatrix *Tri3_u_eta2;
  STLMatrix *Tri3_u_xi_u_eta;
  STLMatrix *Tri3_u2;

  STLMatrix *Tri6_u_xi2;
  STLMatrix *Tri6_u_eta2;
  STLMatrix *Tri6_u_xi_u_eta;
  STLMatrix *Tri6_u2;

  STLMatrix *Tet4_u_xi2;
  STLMatrix *Tet4_u_eta2;
  STLMatrix *Tet4_u_zeta2;
  STLMatrix *Tet4_u_xi_u_eta;
  STLMatrix *Tet4_u_xi_u_zeta;
  STLMatrix *Tet4_u_eta_u_zeta;
  STLMatrix *Tet4_u2;

  STLMatrix *Tet10_u_xi2;
  STLMatrix *Tet10_u_eta2;
  STLMatrix *Tet10_u_zeta2;
  STLMatrix *Tet10_u_xi_u_eta;
  STLMatrix *Tet10_u_xi_u_zeta;
  STLMatrix *Tet10_u_eta_u_zeta;
  STLMatrix *Tet10_u2;
  
};

inline ElementMatrix operator + ( const ElementMatrix & A , const ElementMatrix & B ){
  //  cout << "EM::+EM\t";   
  assert( A.size() == B.size() );
  ElementMatrix tmp( A );
  return tmp += B;
} 

inline ElementMatrix operator * ( double a, const ElementMatrix & B ){
  //  cout << "EM::*(double,EM)\t";   
  ElementMatrix tmp( B );
  return(tmp *= a);
} 

inline ElementMatrix operator * (const ElementMatrix & B, double a){
  //  cout << "EM::*(EM, double)\t";   
  ElementMatrix tmp( B );
  return( tmp *= a) ;
} 
}  //namespace MyVec

#endif //ELEMENTMATRIX__H

/*
$Log: elementmatrix.h,v $
Revision 1.12  2007/08/01 12:30:06  carsten
*** empty log message ***

Revision 1.11  2007/03/07 16:38:14  carsten
*** empty log message ***

Revision 1.10  2006/11/03 13:25:40  carsten
*** empty log message ***

Revision 1.9  2006/10/26 18:28:00  carsten
*** empty log message ***

Revision 1.8  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.7  2005/10/14 15:31:37  carsten
*** empty log message ***

Revision 1.6  2005/10/12 14:41:56  carsten
*** empty log message ***

Revision 1.5  2005/09/28 17:43:20  carsten
*** empty log message ***

Revision 1.4  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.3  2005/07/13 17:05:21  carsten
*** empty log message ***

Revision 1.2  2005/03/29 16:57:46  carsten
add inversion tools

Revision 1.1  2005/01/06 20:28:30  carsten
*** empty log message ***


*/
