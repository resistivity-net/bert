// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef ERRORESTIMATOR__H
#define ERRORESTIMATOR__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "myvec/vector.h"
#include "stlmatrix.h"
using namespace MyVec;


#include "elements.h"
#include "basemesh.h"
using namespace MyMesh;

// class Quadrature{
// public:

// private:
//   STLVector weights_;
//   STLMatrix *samplePointsNorm_;
// };

class ErrorEstimator{
public: 
  ErrorEstimator( BaseMesh * mesh, int order = 5 );
  ~ErrorEstimator( );

  /*! */
  double normL1( const RVector & u ){ return norml1( normLpVec( u, 1 ) ); }
  double normL2( const RVector & u ){ return norml2( normLpVec( u, 2 ) ); }
  double normH1semi( const vector< RealPos > & uGrad ){ return norml2( normH1semiVec( uGrad ) ); }

  RVector normH1semiVec( const vector< RealPos > & uGrad );
  RVector normLpVec( const RVector & u, int p );

  void setQuadrature( int n ){ nQuadrature_ = n; }
  int quadrature(){ return nQuadrature_; }

  //  STLMatrix N(){ return * samplePointsNorm_; }

  vector< RealPos > quadPoints( );
  int quadVals( const RVector & u, RVector & uQuad, vector < RealPos > & uGradQuad );

  RVector quadVals( const RVector & u );

  vector< RealPos > quadValsGrad( const RVector & uh );
  vector< RealPos > valsGrad( const RVector & u );

protected:
  void init_( int ElementRTTI );

  int order_;
  BaseMesh * mesh_;
  int nQuadrature_;
  STLVector weights_;
  STLMatrix zetaMat_;
  //  STLMatrix *samplePointsNorm_;
};

#endif //ERRORESTIMATOR__H

/*
$Log: errorestimator.h,v $
Revision 1.4  2005/10/12 14:41:56  carsten
*** empty log message ***

Revision 1.3  2005/10/06 17:38:36  carsten
*** empty log message ***

Revision 1.2  2005/10/04 18:05:51  carsten
*** empty log message ***

Revision 1.1  2005/10/04 11:10:25  carsten
*** empty log message ***

*/
