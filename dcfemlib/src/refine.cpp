// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "refine.h"
#include <cassert>
using namespace std;

SparcepNodeMatrix::SparcepNodeMatrix( size_t size ) : size_( size ){
  mat = new Node* *[ size_ ]; assert( mat );
  for( size_t i = 0; i < size_; i++ ){
    mat[ i ] = new Node*[ 1 ]; assert( mat[ i ] );
    mat[ i ][ 0 ] = 0;
  }
}
SparcepNodeMatrix::~SparcepNodeMatrix( ) {
  for ( size_t i = 0; i < size_; i++ ){
    delete [] mat[ i ];
  }
  delete [] mat;
}

void SparcepNodeMatrix::setVal( size_t i, size_t j, Node * node ){
  
  //     cout << size_ << ": " << i << " " << j << " " << node->id() << endl;
  //     for ( size_t tmp = 0; tmp < size_; tmp ++ ){
  //       cout << tmp << ": "<< mat[tmp][0]<< " " <<(size_t)mat[tmp][0] << ", ";
  //     } cout << endl;
  
  size_t row = i, column = j;
  if ( column < row ) row = j, column = i;
  
  size_t valuesPerRow = (size_t)mat[ row ][ 0 ];
  
  for ( size_t h = 1; h < valuesPerRow * 2; h += 2 ){
    if ( (size_t)mat[ row ][ h ] == column ){
      mat[ row ][ h + 1 ] = node;
    }
  }
  
  Node * *tmp = new Node*[ valuesPerRow * 2 + 1 ];
  
  memset( tmp, '0', ( 2 * valuesPerRow + 1 )*sizeof( Node * ) );
  memcpy( tmp, mat[ row ], ( 2 * valuesPerRow + 1 )*sizeof( Node * ) );
  
  delete [] mat[ row ];
  mat[ row ] = new Node*[ ( valuesPerRow * 2 ) + 3 ];
  memcpy( mat[ row ], tmp, ( 2 * valuesPerRow + 1 )*sizeof( Node * ) );
  
  mat[ row ][ 0 ] = ( Node* )(((size_t)mat[ row ][ 0 ]) + 1);
  mat[ row ][ 2 * valuesPerRow + 1 ] = ( Node* )column;
  mat[ row ][ 2 * valuesPerRow + 2 ] = node;
  delete [] tmp;
}

Node * SparcepNodeMatrix::val( size_t i, size_t j ){
  size_t row = i, col = j;
  if ( col < row ) row = j, col = i;
  
  size_t valuesPerRow = (size_t)mat[ row ][ 0 ];
  
  //    cout << mat[ row ][ 0 ] << "\t" << (size_t)mat[ row ][ 0 ]<< endl;
  for ( size_t h = 1; h < valuesPerRow * 2; h += 2 ){
    //      cout  << "row: " << row <<" col:" << col << " h:" << h << "\t" << valuesPerRow << endl;
    
    if ( (size_t)mat[ row ][ h ] == col ) return mat[ row ][ h + 1 ];
  }
  return NULL;
}

void SparcepNodeMatrix::save( const string & fname){
  fstream file; openOutFile( fname, & file );
  for ( size_t row = 0; row < size_; row ++){
    size_t valuesPerRow = (size_t)mat[ row ][ 0 ];
    for ( size_t h = 1; h < valuesPerRow * 2; h += 2 ){
      
      file << row << "\t" << (size_t)mat[ row ][ h ] << "\t" << mat[ row ][ h + 1 ]->id() << endl;
    }
  } 
  file.close();
}

int markerT( Node * n0, Node * n1 ){
  if ( n0->marker() == -99 && n1->marker() == -99 ) return -1;
  if ( n0->marker() == -99 ) return n1->marker();
  if ( n1->marker() == -99 ) return n0->marker();
  if ( n0->marker() == n1->marker() ) return n1->marker();
  else return 0;
}

RVector estimateZienkiewiczZhuError( const BaseMesh & mesh, const RVector & u ){
  RVector eta( mesh.cellCount() );
  vector< double > cellAreas;
  vector< double > gradCellUx;
  vector< double > gradCellUy;
  vector< double > gradCellUxPerNode( mesh.nodeCount(), 0.0 );
  vector< double > gradCellUyPerNode( mesh.nodeCount(), 0.0 );
  vector< double > sumCellAreasPerNode( mesh.nodeCount(), 0.0 );

  double J = 0.0;
  double dx1 = 0.0, dx2 = 0.0, dx3 = 0.0, dy1 = 0.0, dy2 = 0.0, dy3 = 0.0;
  double x1 = 0.0, x2 = 0.0, x3 = 0.0, y1 = 0.0, y2 = 0.0, y3 = 0.0;
  double u1 = 0.0, u2 = 0.0, u3 = 0.0;
  for ( int i = 0; i < mesh.cellCount(); i++ ){
    J = mesh.cell( i ).jacobianDeterminant();
    cellAreas.push_back( J / 2.0 );
    for ( int j = 0; j < mesh.cell( i ).nodeCount(); j++ ){
      sumCellAreasPerNode[ mesh.cell( i ).node( j ).id() ] += cellAreas[ i ];
    }
    x1 = mesh.cell( i ).node( 0 ).x();
    x2 = mesh.cell( i ).node( 1 ).x();
    x3 = mesh.cell( i ).node( 2 ).x();

    y1 = mesh.cell( i ).node( 0 ).y();
    y2 = mesh.cell( i ).node( 1 ).y();
    y3 = mesh.cell( i ).node( 2 ).y();

    dx1 = ( y2 - y3 ) / J;
    dx2 = - ( y1 - y3 ) / J;
    dx3 = ( y1 - y2 ) / J;
    dy1 = - ( x2 - x3 ) / J;
    dy2 = ( x1 - x3 ) / J;
    dy3 = - ( x1 - x2 ) / J;

    u1 = u[ mesh.cell( i ).node( 0 ).id() ];
    u2 = u[ mesh.cell( i ).node( 1 ).id() ];
    u3 = u[ mesh.cell( i ).node( 2 ).id() ];

    gradCellUx.push_back( dx1 * u1 + dx2 * u2 + dx3 * u3 );
    gradCellUy.push_back( dy1 * u1 + dy2 * u2 + dy3 * u3 );
    //    cout << dx1 * u1 + dx2 * u2 + dx3 * u3 << "\t" << dy1 * u1 + dy2 * u2 + dy3 * u3 << endl;

    for ( int j = 0; j < mesh.cell( i ).nodeCount(); j++ ){
      gradCellUxPerNode[ mesh.cell( i ).node( j ).id() ] += cellAreas[ i ] * gradCellUx[ i ];
      gradCellUyPerNode[ mesh.cell( i ).node( j ).id() ] += cellAreas[ i ] * gradCellUy[ i ];
    }
  }

  for ( int i = 0; i < mesh.nodeCount(); i++ ){
    gradCellUxPerNode[ i ] /= sumCellAreasPerNode[ i ];
    gradCellUyPerNode[ i ] /= sumCellAreasPerNode[ i ];
  }

  double gn1 = 0.0, gn2 = 0.0, gn3 = 0.0;
  double gr = 0.0;

  //** hack
  RVector S0( 3 );
  RVector S1( 3 );
  RVector S2( 3 );
  RVector gn( 3 );
  RVector s( 3 );
  RVector tmp( 3 );
  S0[ 0 ] = 2;  S0[ 1 ] = 1; S0[ 2 ] = 1;
  S1[ 0 ] = 1;  S1[ 1 ] = 2; S1[ 2 ] = 1;
  S2[ 0 ] = 1;  S2[ 1 ] = 1; S2[ 2 ] = 2;
  s[ 0 ] = 2;  s[ 1 ] = 2; s[ 2 ] = 2;

  for ( int i = 0; i < mesh.cellCount(); i++ ){
    gn[ 0 ] = gradCellUxPerNode[ mesh.cell( i ).node( 0 ).id() ];
    gn[ 1 ] = gradCellUxPerNode[ mesh.cell( i ).node( 1 ).id() ];
    gn[ 2 ] = gradCellUxPerNode[ mesh.cell( i ).node( 2 ).id() ];
    gr  = gradCellUx[ i ];

    tmp[ 0 ] = scalar( gn, S0 ) / 12.;
    tmp[ 1 ] = scalar( gn, S1 ) / 12.;
    tmp[ 2 ] = scalar( gn, S2 ) / 12.;
    tmp -= (gr / 3.0 * s);

    eta[ i ] = cellAreas[ i ] * ( ( gr * gr) + scalar( tmp, gn ) );
    
    gn[ 0 ] = gradCellUyPerNode[ mesh.cell( i ).node( 0 ).id() ];
    gn[ 1 ] = gradCellUyPerNode[ mesh.cell( i ).node( 1 ).id() ];
    gn[ 2 ] = gradCellUyPerNode[ mesh.cell( i ).node( 2 ).id() ];
    gr  = gradCellUy[ i ];

    tmp[ 0 ] = scalar( gn, S0 ) / 12.;
    tmp[ 1 ] = scalar( gn, S1 ) / 12.;
    tmp[ 2 ] = scalar( gn, S2 ) / 12.;
    tmp -= (gr / 3.0 * s);

    eta[ i ] += cellAreas[ i ] * ( ( gr * gr ) + scalar(tmp, gn) );
  }

  for ( int i = 0; i < mesh.cellCount(); i++ ) eta[ i ] = sqrt ( eta[ i ] );

  return eta;
}

/*
$Log: refine.cpp,v $
Revision 1.3  2005/10/17 15:10:17  carsten
*** empty log message ***

Revision 1.2  2005/10/14 15:31:37  carsten
*** empty log message ***

Revision 1.1  2005/03/10 16:04:38  carsten
*** empty log message ***

*/
